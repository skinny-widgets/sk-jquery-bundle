(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.window = global.window || {}));
}(this, (function (exports) { 'use strict';

  function _classCallCheck(a, n) {
    if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function");
  }

  function _typeof(o) {
    "@babel/helpers - typeof";

    return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, _typeof(o);
  }

  function toPrimitive(t, r) {
    if ("object" != _typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != _typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }

  function toPropertyKey(t) {
    var i = toPrimitive(t, "string");
    return "symbol" == _typeof(i) ? i : i + "";
  }

  function _defineProperties(e, r) {
    for (var t = 0; t < r.length; t++) {
      var o = r[t];
      o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, toPropertyKey(o.key), o);
    }
  }
  function _createClass(e, r, t) {
    return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", {
      writable: !1
    }), e;
  }

  function _assertThisInitialized(e) {
    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    return e;
  }

  function _possibleConstructorReturn(t, e) {
    if (e && ("object" == _typeof(e) || "function" == typeof e)) return e;
    if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined");
    return _assertThisInitialized(t);
  }

  function _getPrototypeOf(t) {
    return _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function (t) {
      return t.__proto__ || Object.getPrototypeOf(t);
    }, _getPrototypeOf(t);
  }

  function _setPrototypeOf(t, e) {
    return _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function (t, e) {
      return t.__proto__ = e, t;
    }, _setPrototypeOf(t, e);
  }

  function _inherits(t, e) {
    if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
    t.prototype = Object.create(e && e.prototype, {
      constructor: {
        value: t,
        writable: !0,
        configurable: !0
      }
    }), Object.defineProperty(t, "prototype", {
      writable: !1
    }), e && _setPrototypeOf(t, e);
  }

  function _isNativeFunction(t) {
    try {
      return -1 !== Function.toString.call(t).indexOf("[native code]");
    } catch (n) {
      return "function" == typeof t;
    }
  }

  function _isNativeReflectConstruct$17() {
    try {
      var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    } catch (t) {}
    return (_isNativeReflectConstruct$17 = function _isNativeReflectConstruct() {
      return !!t;
    })();
  }

  function _construct(t, e, r) {
    if (_isNativeReflectConstruct$17()) return Reflect.construct.apply(null, arguments);
    var o = [null];
    o.push.apply(o, e);
    var p = new (t.bind.apply(t, o))();
    return r && _setPrototypeOf(p, r.prototype), p;
  }

  function _wrapNativeSuper(t) {
    var r = "function" == typeof Map ? new Map() : void 0;
    return _wrapNativeSuper = function _wrapNativeSuper(t) {
      if (null === t || !_isNativeFunction(t)) return t;
      if ("function" != typeof t) throw new TypeError("Super expression must either be null or a function");
      if (void 0 !== r) {
        if (r.has(t)) return r.get(t);
        r.set(t, Wrapper);
      }
      function Wrapper() {
        return _construct(t, arguments, _getPrototypeOf(this).constructor);
      }
      return Wrapper.prototype = Object.create(t.prototype, {
        constructor: {
          value: Wrapper,
          enumerable: !1,
          writable: !0,
          configurable: !0
        }
      }), _setPrototypeOf(Wrapper, t);
    }, _wrapNativeSuper(t);
  }

  function _newArrowCheck(n, r) {
    if (n !== r) throw new TypeError("Cannot instantiate an arrow function");
  }

  var HgTemplateEngine = /*#__PURE__*/function () {
    function HgTemplateEngine(handlebars) {
      _classCallCheck(this, HgTemplateEngine);
      this.handlebars = handlebars;
    }
    return _createClass(HgTemplateEngine, [{
      key: "renderString",
      value: function renderString(tplString, data) {
        var templateFunc = this.handlebars.compile(tplString);
        var result = templateFunc(data);
        return result;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          var wrapper = document.createElement('div');
          wrapper.appendChild(tpl);
          var templateString = wrapper.outerHTML.toString();
          var rendered = this.renderString(templateString, data);
          var wrapper2 = document.createElement('div');
          wrapper2.insertAdjacentHTML('beforeend', rendered);
          return wrapper2.firstElementChild.innerHTML;
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);
  }();

  var BaseTemplateEngine = /*#__PURE__*/function () {
    function BaseTemplateEngine() {
      _classCallCheck(this, BaseTemplateEngine);
    }
    return _createClass(BaseTemplateEngine, [{
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = document.createElement('div');
        wrapper.appendChild(el);
        var template = wrapper.outerHTML.toString();
        for (var _i = 0, _Object$keys = Object.keys(map); _i < _Object$keys.length; _i++) {
          var key = _Object$keys[_i];
          template = template.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }
        var wrapper2 = document.createElement('div');
        wrapper2.insertAdjacentHTML('beforeend', template);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "renderString",
      value: function renderString(tplString, data) {
        var resultString = (' ' + tplString).slice(1);
        for (var _i2 = 0, _Object$keys2 = Object.keys(data); _i2 < _Object$keys2.length; _i2++) {
          var key = _Object$keys2[_i2];
          resultString = resultString.replace(new RegExp('{{ ' + key + ' }}', 'g'), data[key]);
        }
        return resultString;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          return this.renderMustacheVars(tpl, data);
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);
  }();

  function _createForOfIteratorHelper$k(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$l(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$l(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$l(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$l(r, a) : void 0; } }
  function _arrayLikeToArray$l(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  var ResLoader = /*#__PURE__*/function () {
    function ResLoader() {
      _classCallCheck(this, ResLoader);
    }
    return _createClass(ResLoader, null, [{
      key: "dynamicImportSupported",
      value: function dynamicImportSupported() {
        try {
          new Function('import("")');
          return true;
        } catch (err) {
          return false;
        }
      }
    }, {
      key: "isInIE",
      value: function isInIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        return false;
      }
    }, {
      key: "loadJson",
      value: function loadJson(url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this);
          fetch(url).then(function (response) {
            var _this2 = this;
            response.json().then(function (json) {
              _newArrowCheck(this, _this2);
              resolve(json);
            }.bind(this));
          });
        }.bind(this));
      }
    }, {
      key: "loadClass",
      value: function loadClass(className, path, callback) {
        var _this3 = this;
        var nowindow = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
        var dynImportOff = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
        var loaded;
        var setToWin = function setToWin() {
          _newArrowCheck(this, _this3);
          if (!nowindow) {
            loaded.then(function (m) {
              if (m[className]) {
                window[className] = m[className];
                return m;
              }
            });
          }
        }.bind(this);
        var applyCallback = function applyCallback() {
          _newArrowCheck(this, _this3);
          if (callback) {
            loaded.then(callback);
          }
        }.bind(this);
        if (!dynImportOff && ResLoader.dynamicImportSupported() && !ResLoader.isInIE()) {
          loaded = require("".concat(path));
          setToWin();
          applyCallback();
          return loaded;
        } else {
          loaded = new Promise(function (resolve, reject) {
            var script = document.createElement('script');
            script.onload = resolve;
            script.onerror = reject;
            script.async = true;
            script.src = path;
            document.body.appendChild(script);
          }.bind(this));
          setToWin();
          applyCallback();
          return loaded;
        }
      }
    }, {
      key: "deepCopy",
      value: function deepCopy(from, to, level) {
        var maxLevel = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 256;
        if (from == null || _typeof(from) != "object") return from;
        if (from.constructor != Object && from.constructor != Array) return from;
        if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function || from.constructor == String || from.constructor == Number || from.constructor == Boolean) return new from.constructor(from);
        to = to || new from.constructor();
        if (!level) level = 0;
        level++;
        if (level <= maxLevel) {
          for (var name in from) {
            to[name] = ResLoader.deepCopy(from[name], null);
          }
        }
        return to;
      }
    }, {
      key: "instIndex",
      value: function instIndex(className, path, factory) {
        return factory ? className + '::' + path + '::' + factory.toString().replace(/(\r\n|\n|\r| |\t)/gm, "") : className + '::' + path;
      }
    }, {
      key: "promiseStore",
      value: function promiseStore() {
        var resLoader = window.ResLoader ? window.ResLoader : ResLoader;
        if (!resLoader._loadPromises) {
          resLoader._loadPromises = new Map();
        }
        return resLoader._loadPromises;
      }
    }, {
      key: "instanceStore",
      value: function instanceStore() {
        var resLoader = window.ResLoader ? window.ResLoader : ResLoader;
        if (!resLoader._loadInstances) {
          resLoader._loadInstances = new Map();
        }
        return resLoader._loadInstances;
      }
    }, {
      key: "cacheClassDefs",
      value: function cacheClassDefs() {
        var _iterator = _createForOfIteratorHelper$k(arguments),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var def = _step.value;
            if (def.name && !window[def.name]) {
              window[def.name] = def;
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }

      /**
       * does class and instance loading.
       * If class with the same name and path was previously loaded it's previous instance will be returned 
       * by default and until forceNewInstance arg is set. 
       * 
       * className - class name string
       * path - loading path
       * factory - external method to create and setup new instance
       * forceNewInstance - will do new instance with provided argument factory or class constructor. 
       * keepState - link state props from prev instance
       * deepClone - recursive copy from prev instance
       * promiseStore - use external Map for loading promises cache
       * instanceStore - use external Map for class instance cache
       * cloneImpl - use external implementation for deepClone operation 
       * dynImmportOff - turn of dynamic import use (e.g. for transpilled code)
       * sync return instance without promise, class def must be preloaded and assigned to window
       * */
    }, {
      key: "dynLoad",
      value: function dynLoad(className, path, factory, forceNewInstance, keepState, deepClone, promiseStore, instanceStore, cloneImpl, dynImportOff, sync, stDef) {
        var _this4 = this;
        var index = ResLoader.instIndex(className, path, factory);
        var promises;
        var instances;
        var doNewInstance = function doNewInstance(model) {
          var instance;
          if (factory) {
            instance = factory(model.constructor);
          } else {
            instance = new model.constructor();
          }
          if (keepState) {
            if (deepClone) {
              if (cloneImpl && typeof cloneImpl === 'function') {
                instance = cloneImpl(model);
              } else {
                ResLoader.deepCopy(model, instance);
              }
            } else {
              Object.assign(instance, model);
            }
          }
          return instance;
        };
        if (!promiseStore) {
          promises = ResLoader.promiseStore();
        } else {
          promises = promiseStore;
        }
        if (!instanceStore) {
          instances = ResLoader.instanceStore();
        } else {
          instances = instanceStore;
        }
        if (instances.get(index)) {
          if (forceNewInstance) {
            var model = instances.get(index);
            var instance = doNewInstance(model);
            return sync ? instance : Promise.resolve(instance);
          } else {
            return sync ? instances.get(index) : Promise.resolve(instances.get(index));
          }
        } else {
          if (sync) {
            if (stDef && !window[className]) {
              window[className] = stDef;
            }
            var def = Function('return ' + className)();
            var _instance;
            if (factory) {
              _instance = factory(def);
            } else {
              _instance = new def();
            }
            instances.set(index, _instance);
            promises.set(index, Promise.resolve(_instance));
            return _instance;
          } else {
            if (!promises.get(index)) {
              promises.set(index, new Promise(function (resolve, reject) {
                var _this5 = this;
                _newArrowCheck(this, _this4);
                try {
                  if (stDef && !window[className]) {
                    window[className] = stDef;
                  }
                  var _def = Function('return ' + className)();
                  var _instance2;
                  if (factory) {
                    _instance2 = factory(_def);
                  } else {
                    _instance2 = new _def();
                  }
                  instances.set(index, _instance2);
                  resolve(_instance2);
                } catch (_unused) {
                  if (path.endsWith(".json")) {
                    promises.set(index, ResLoader.loadJson(path));
                    promises.get(index).then(function (m) {
                      _newArrowCheck(this, _this5);
                      resolve(m);
                    }.bind(this));
                  } else {
                    promises.set(index, ResLoader.loadClass(className, path, null, false, dynImportOff));
                    promises.get(index).then(function (m) {
                      _newArrowCheck(this, _this5);
                      var def = typeof m[className] === 'function' ? m[className] : window[className];
                      if (def) {
                        var _instance3;
                        if (factory) {
                          _instance3 = factory(def);
                        } else {
                          _instance3 = new def();
                        }
                        instances.set(index, _instance3);
                        resolve(_instance3);
                      } else {
                        reject("expected constructor for ".concat(className, " not found"));
                      }
                    }.bind(this));
                  }
                }
              }.bind(this)));
            }
            if (forceNewInstance) {
              return new Promise(function (resolve, reject) {
                var _this6 = this;
                _newArrowCheck(this, _this4);
                var onLoaded = promises.get(index);
                onLoaded.then(function (model) {
                  _newArrowCheck(this, _this6);
                  var instance = doNewInstance(model);
                  if (!instances.get(index)) {
                    instances.set(index, instance);
                  }
                  return resolve(instance);
                }.bind(this));
              }.bind(this));
            } else {
              return promises.get(index);
            }
          }
        }
      }
    }]);
  }();

  var RD_SHARED_ATTRS = {
    'rd-cache': 'cacheTemplates',
    'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender',
    'rd-tpl-fmt': 'tplFmt',
    'basepath': 'basePath',
    'theme': 'theme'
  };
  var RD_OWN_ATTRS = {
    'rd-own': 'rdOwn',
    'rd-cache': 'cacheTemplates',
    'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender',
    'rd-tpl-fmt': 'tplFmt'
  };
  var Renderer = /*#__PURE__*/function () {
    //templates = {};

    function Renderer() {
      _classCallCheck(this, Renderer);
      this.templates = {};
      this.templateLoad = new Map();
      this.translations = {};
      this.cacheTemplates = true;
      this.allowGlobalTemplates = true;
      this.variableRender = true;
      if (window['$'] !== undefined || window['jQuery'] !== undefined) {
        this.jquery = $ || jQuery.noConflict();
      }
    }
    return _createClass(Renderer, [{
      key: "confFromEl",
      value: function confFromEl(el) {
        for (var _i = 0, _Object$keys = Object.keys(RD_SHARED_ATTRS); _i < _Object$keys.length; _i++) {
          var attrName = _Object$keys[_i];
          var value = el.getAttribute(attrName);
          if (value !== null && value !== undefined) {
            try {
              this[RD_SHARED_ATTRS[attrName]] = JSON.parse(value);
            } catch (e) {
              this[RD_SHARED_ATTRS[attrName]] = value;
            }
          }
        }
      }
    }, {
      key: "templateEngine",
      get: function get() {
        if (!this._templateEngine) {
          if (this.variableRender === 'handlebars') {
            if (this.Handlebars !== undefined) {
              this._templateEngine = new HgTemplateEngine(this.Handlebars);
            } else {
              if (window && window['Handlebars']) {
                this._templateEngine = new HgTemplateEngine(window['Handlebars']);
              } else {
                console.error('no handlebars found in window or property');
              }
            }
          } else {
            if (typeof window[this.variableRender] === 'function') {
              window[this.variableRender].call(this);
            } else {
              this._templateEngine = new BaseTemplateEngine();
            }
          }
        }
        return this._templateEngine;
      }
    }, {
      key: "queryTemplate",
      value: function queryTemplate(sl) {
        if (this.templates[sl] !== null && this.templates[sl] !== undefined) {
          return this.templates[sl];
        } else {
          var template = document.querySelector(sl);
          if (template !== null && template !== undefined) {
            this.templates[sl] = template;
          } else {
            console.warn("Template with selector ".concat(sl, " not found"));
          }
          return this.templates[sl];
        }
      }
    }, {
      key: "loadTemplate",
      value: function loadTemplate(path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this);
          fetch(path).then(function (response) {
            var _this2 = this;
            response.text().then(function (text) {
              _newArrowCheck(this, _this2);
              resolve(text);
            }.bind(this));
          });
        }.bind(this));
      }
    }, {
      key: "findTemplateEl",
      value: function findTemplateEl(id, hostEl) {
        var el = null;
        if (this.cacheTemplates) {
          if (hostEl) {
            el = hostEl.querySelector('#' + id);
            if (!el) {
              el = hostEl.querySelector(id);
            }
          }
          if (this.allowGlobalTemplates) {
            if (!el) {
              el = document.getElementById(id);
              if (el && el.parentElement && el.parentElement.tagName !== 'DIV' && el.parentElement.tagName !== 'BODY' && el.parentElement.tagName !== 'HEAD' && el.parentElement.tagName !== 'HTML') {
                // skip internal customElements overrides as they must not be shared
                el = null;
              }
              if (!el) {
                el = document.querySelector(id);
              }
            }
          }
        }
        return el;
      }
    }, {
      key: "genElPathString",
      value: function genElPathString(el) {
        var path = el.nodeName.split("-").join('__');
        var parent = el.parentNode;
        while (parent && parent.tagName) {
          path = parent.nodeName.split("-").join('__') + '_' + path;
          parent = parent.parentNode;
        }
        return path;
      }
    }, {
      key: "suggestId",
      value: function suggestId(id) {
        if (document.querySelector('#' + id) !== null) {
          var nums = id.match(/.*(\d+)/);
          if (nums !== null) {
            var num = nums[1];
            var name = id.replace(num, '');
            id = this.suggestId(name + (++num).toString());
          } else {
            id = this.suggestId(id + '2');
          }
        }
        return id;
      }
    }, {
      key: "genElId",
      value: function genElId(el) {
        var id = el.getAttribute('id');
        if (!id) {
          var domPath = this.genElPathString(el);
          id = this.suggestId(domPath);
        }
        return id;
      }
    }, {
      key: "mountTemplate",
      value: function mountTemplate(path, id, hostEl, preRenData) {
        var tplHash = path;
        if (hostEl && hostEl.constructor) {
          // different classes must not share the same template
          tplHash = hostEl.constructor.name + ":" + tplHash;
        }
        if (preRenData && !preRenData.constructor) {
          // pre-rendered values must not overwrite
          tplHash = tplHash + JSON.stringify(preRenData);
        }
        if (!this.templateLoad.has(tplHash)) {
          this.templateLoad.set(tplHash, new Promise(function (resolve, reject) {
            var _this3 = this;
            var el = this.findTemplateEl(id, hostEl);
            if (el !== null) {
              var elInstance = document.importNode(el, true);
              // can be mustache or native template inlined and overriden
              if (this.variableRender && this.templateEngine && preRenData) {
                elInstance.innerHTML = this.templateEngine.render(elInstance.innerHTML, preRenData);
              }
              resolve(elInstance);
            } else {
              this.loadTemplate(path).then(function (body) {
                _newArrowCheck(this, _this3);
                var template = this.findTemplateEl(id, hostEl);
                if (!template) {
                  if (this.tplFmt === 'handlebars') {
                    template = this.createEl('script');
                    template.setAttribute('type', 'text/x-handlebars-template');
                  } else {
                    template = this.createEl('template');
                  }
                }
                template.setAttribute('id', id);
                template.innerHTML = body;
                if (this.cacheTemplates) {
                  el = this.findTemplateEl(id, hostEl);
                  if (this.allowGlobalTemplates) {
                    if (template.hasAttribute('id') && !document.getElementById(template.getAttribute('id'))) {
                      document.body.appendChild(template);
                    }
                  } else {
                    hostEl.appendChild(template);
                  }
                  el = this.findTemplateEl(id, hostEl);
                }
                if (this.variableRender && this.templateEngine && preRenData) {
                  var contents = this.templateEngine.render(body, preRenData);
                  var templateCopy = document.importNode(template, true);
                  templateCopy.innerHTML = contents;
                  resolve(templateCopy);
                } else {
                  resolve(el);
                }
              }.bind(this));
            }
          }.bind(this)));
        }
        return this.templateLoad.get(tplHash) || Promise.reject('Template load error', tplHash);
      }
    }, {
      key: "prepareTemplate",
      value: function prepareTemplate(tpl) {
        if (this.tplFmt && this.tplFmt === 'handlebars') {
          var el = this.jquery(tpl.innerHTML);
          var html = '';
          if (el.length > 0) {
            html = this.jquery().prop ? this.jquery(tpl.innerHTML).prop('outerHTML') : this.jquery(tpl.innerHTML)[0].outerHTML;
          }
          var wrapper = this.createEl('div');
          this.jquery(wrapper).append(html);
          return wrapper.firstElementChild;
        } else {
          if (tpl.length) {
            return document.importNode(tpl[0].content, true);
          } else {
            return document.importNode(tpl.content, true);
          }
        }
      }
    }, {
      key: "renderTemplate",
      value: function renderTemplate(sl, dataMap) {
        var template = this.queryTemplate(sl);
        var fragment = document.importNode(template.content, true);
        for (var _i2 = 0, _Object$keys2 = Object.keys(dataMap); _i2 < _Object$keys2.length; _i2++) {
          var _sl = _Object$keys2[_i2];
          var entry = dataMap[_sl];
          var el = fragment.querySelector(_sl);
          if (el !== null) {
            if (entry.type === 'attr') {
              el.setAttribute(entry.attr, entry.value);
            } else {
              el.textContent = entry.value;
            }
          } else {
            console.warn("Element with selector ".concat(_sl, " not found in DOM"));
          }
        }
        return fragment;
      }
    }, {
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = this.createEl('div');
        if (typeof el === 'string') {
          wrapper.insertAdjacentHTML('beforeend', el);
        } else {
          wrapper.appendChild(el);
        }
        var template = wrapper.outerHTML.toString(); // :TODO detect if document-fragment or element is passed

        var rendered = this.replaceVars(template, map);
        var wrapper2 = this.createEl('div');
        wrapper2.insertAdjacentHTML('beforeend', rendered);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "replaceVars",
      value: function replaceVars(target, map) {
        for (var _i3 = 0, _Object$keys3 = Object.keys(map); _i3 < _Object$keys3.length; _i3++) {
          var key = _Object$keys3[_i3];
          target = target.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }
        return target;
      }
    }, {
      key: "createEl",
      value: function createEl(tagName, options) {
        return document.createElement(tagName, options);
      }
    }, {
      key: "renderWithTr",
      value: function renderWithTr(trJson, targetEl) {
        var _this4 = this;
        var fakeRoot = this.createEl('div');
        fakeRoot.appendChild(targetEl);
        fakeRoot.querySelectorAll('[tr]').forEach(function (el) {
          _newArrowCheck(this, _this4);
          var tr = el.getAttribute('tr');
          if (tr === 'contents') {
            var trValue = trJson[el.innerHTML];
            if (trValue !== undefined) {
              el.innerHTML = trValue;
            }
          } else if (tr.startsWith('attr')) {
            var trInfo = tr.split(':');
            var _trValue = trJson[trInfo[2]];
            if (_trValue !== undefined) {
              el.setAttribute(trInfo[1], _trValue);
            }
          }
        }.bind(this));
        return fakeRoot.firstChild.cloneNode(true);
      }
    }, {
      key: "localizeEl",
      value: function localizeEl(targetEl, locale, trUrl, trName) {
        var _this5 = this;
        var self = this;
        var promiseStore = ResLoader.promiseStore();
        ResLoader.dynLoad(trName, trUrl);
        return new Promise(function (resolve, reject) {
          var _this6 = this;
          _newArrowCheck(this, _this5);
          var index = ResLoader.instIndex(trName, trUrl);
          promiseStore.get(index).then(function (json) {
            _newArrowCheck(this, _this6);
            self.translations[locale] = Object.assign(self.translations[locale] ? self.translations[locale] : {}, json);
            resolve(self.renderWithTr(self.translations[locale], targetEl));
          }.bind(this));
        }.bind(this));
      }
    }, {
      key: "whenElRendered",
      value: function whenElRendered(el, callback) {
        return Renderer.callWhenRendered(el, callback);
      }
    }], [{
      key: "hasRenderAttrs",
      value: function hasRenderAttrs(el) {
        for (var _i4 = 0, _Object$keys4 = Object.keys(RD_SHARED_ATTRS); _i4 < _Object$keys4.length; _i4++) {
          var attrName = _Object$keys4[_i4];
          var value = el.getAttribute(attrName);
          if (value !== null && value !== undefined) {
            return true;
          }
        }
        return false;
      }
    }, {
      key: "hasOwnAttrs",
      value: function hasOwnAttrs(el) {
        for (var _i5 = 0, _Object$keys5 = Object.keys(RD_OWN_ATTRS); _i5 < _Object$keys5.length; _i5++) {
          var attrName = _Object$keys5[_i5];
          var value = el.getAttribute(attrName);
          if (value !== null && value !== undefined) {
            return true;
          }
        }
        return false;
      }
    }, {
      key: "configureForElement",
      value: function configureForElement(el) {
        if (!el.renderer) {
          el.renderer = new Renderer();
          el.renderer.confFromEl(el);
        } else {
          // if one of renderer options specified element needs own extended renderer
          if (Renderer.hasOwnAttrs(el)) {
            var rendererCopy = el.renderer;
            el.renderer = new Renderer();
            for (var _i6 = 0, _Object$keys6 = Object.keys(RD_SHARED_ATTRS); _i6 < _Object$keys6.length; _i6++) {
              var attrName = _Object$keys6[_i6];
              if (rendererCopy[RD_SHARED_ATTRS[attrName]] !== null && rendererCopy[RD_SHARED_ATTRS[attrName]] !== undefined) {
                el.renderer[RD_SHARED_ATTRS[attrName]] = rendererCopy[RD_SHARED_ATTRS[attrName]];
              }
            }
            el.renderer.confFromEl(el);
          }
        }
      }
    }, {
      key: "callOnceOn",
      value: function callOnceOn(el, eventName, callback) {
        var _this7 = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this7);
          if (el.impl || el._implPromise) {
            if (!el.impl || el.impl && !el.implRenderTimest) {
              el.addEventListener(eventName, function skElementWhenEvt(event) {
                if (event.target === el) {
                  el.removeEventListener(eventName, skElementWhenEvt);
                  resolve(callback ? callback(event) : null);
                }
              });
            } else {
              resolve(callback ? callback() : null);
            }
          } else {
            resolve(callback ? callback() : null);
          }
        }.bind(this));
      }
    }, {
      key: "callWhenRendered",
      value: function callWhenRendered(el, callback) {
        return Renderer.callOnceOn(el, 'skrender', callback);
      }
    }]);
  }();

  function _createForOfIteratorHelper$j(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$k(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$k(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$k(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$k(r, a) : void 0; } }
  function _arrayLikeToArray$k(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  var LOG_LEVEL_DEFAULT = 'info,error,warn';
  var ConsoleLogger = /*#__PURE__*/function () {
    function ConsoleLogger() {
      _classCallCheck(this, ConsoleLogger);
    }
    return _createClass(ConsoleLogger, [{
      key: "levels",
      get: function get() {
        return this._levels;
      },
      set: function set(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
      }
    }, {
      key: "logLevel",
      get: function get() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this._logLevel = level;
        this._levels = level.split(",");
      }
    }, {
      key: "info",
      value: function info() {
        if (this.levels.indexOf('info') > -1) {
          if (typeof console !== 'undefined') {
            console.log.apply(console, arguments);
          }
        }
      }
    }, {
      key: "debug",
      value: function debug() {
        if (this.levels.indexOf('debug') > -1) {
          if (typeof console !== 'undefined') {
            console.debug.apply(console, arguments);
          }
        }
      }
    }, {
      key: "error",
      value: function error() {
        if (this.levels.indexOf('error') > -1) {
          if (typeof console !== 'undefined') {
            console.error.apply(console, arguments);
          }
        }
      }
    }, {
      key: "warn",
      value: function warn() {
        if (this.levels.indexOf('warn') > -1) {
          if (typeof console !== 'undefined') {
            console.warn.apply(console, arguments);
          }
        }
      }
    }], [{
      key: "instance",
      value: function instance() {
        if (!ConsoleLogger._instance) {
          ConsoleLogger._instance = new ConsoleLogger();
          ConsoleLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return ConsoleLogger._instance;
      }
    }, {
      key: "info",
      value: function info() {
        ConsoleLogger.instance().info(arguments);
      }
    }, {
      key: "debug",
      value: function debug() {
        ConsoleLogger.instance().debug(arguments);
      }
    }, {
      key: "error",
      value: function error() {
        ConsoleLogger.instance().error(arguments);
      }
    }, {
      key: "warn",
      value: function warn() {
        ConsoleLogger.instance().warn(arguments);
      }
    }]);
  }();
  var MemoryLogger = /*#__PURE__*/function () {
    function MemoryLogger() {
      _classCallCheck(this, MemoryLogger);
    }
    return _createClass(MemoryLogger, [{
      key: "logs",
      get: function get() {
        if (!this._logs) {
          this._logs = [];
        }
        return this._logs;
      },
      set: function set(logs) {
        this._logs = logs;
      }
    }, {
      key: "levels",
      get: function get() {
        return this._levels;
      },
      set: function set(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
      }
    }, {
      key: "logLevel",
      get: function get() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this._logLevel = level;
        this._levels = level.split(",");
      }
    }, {
      key: "info",
      value: function info() {
        if (this.levels.indexOf('info') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }, {
      key: "debug",
      value: function debug() {
        if (this.levels.indexOf('debug') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }, {
      key: "error",
      value: function error() {
        if (this.levels.indexOf('error') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }, {
      key: "warn",
      value: function warn() {
        if (this.levels.indexOf('warn') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }], [{
      key: "instance",
      value: function instance() {
        if (!MemoryLogger._instance) {
          MemoryLogger._instance = new MemoryLogger();
          MemoryLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return MemoryLogger._instance;
      }
    }, {
      key: "info",
      value: function info() {
        MemoryLogger.instance().info(arguments);
      }
    }, {
      key: "debug",
      value: function debug() {
        MemoryLogger.instance().debug(arguments);
      }
    }, {
      key: "error",
      value: function error() {
        MemoryLogger.instance().error(arguments);
      }
    }, {
      key: "warn",
      value: function warn() {
        MemoryLogger.instance().warn(arguments);
      }
    }]);
  }();
  var SESSION_LOGS_ATTR = "skLogs";
  var SessionLogger = /*#__PURE__*/function () {
    function SessionLogger() {
      _classCallCheck(this, SessionLogger);
    }
    return _createClass(SessionLogger, [{
      key: "logs",
      get: function get() {
        if (!this._logs) {
          try {
            var prevLogs = sessionStorage.get(SESSION_LOGS_ATTR) || "[]";
            if (prevLogs) {
              var logsParsed = JSON.parse(prevLogs);
              if (Array.isArray(logsParsed) && logsParsed.length > -1) {
                this._logs = logsParsed;
              } else {
                this._logs = [];
              }
            } else {
              this._logs = [];
            }
          } catch (_unused) {
            this._logs = [];
          }
        }
        return this._logs;
      },
      set: function set(logs) {
        this._logs = logs;
      }
    }, {
      key: "levels",
      get: function get() {
        return this._levels;
      },
      set: function set(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
      }
    }, {
      key: "logLevel",
      get: function get() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this._logLevel = level;
        this._levels = level.split(",");
      }
    }, {
      key: "writeLog",
      value: function writeLog() {
        var message = [];
        var _iterator = _createForOfIteratorHelper$j(arguments),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var arg = _step.value;
            if (typeof arg === "string" || typeof arg === "number") {
              message.push(arg.toString());
            } else if (_typeof(arg) === "object") {
              var subArg = {};
              var _iterator2 = _createForOfIteratorHelper$j(Object.getOwnPropertyNames(arg)),
                _step2;
              try {
                for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                  var subArgName = _step2.value;
                  if (["caller", "callee", "arguments", "length"].indexOf(subArgName) < 0) {
                    if (typeof arg[subArgName] === "string" || typeof arg[subArgName] === "number") {
                      subArg[subArgName] = arg[subArgName];
                    }
                  }
                }
              } catch (err) {
                _iterator2.e(err);
              } finally {
                _iterator2.f();
              }
              try {
                if (Object.getOwnPropertyNames(subArg).length > -1) {
                  message.push(JSON.stringify(subArg));
                }
              } catch (_unused2) {}
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        if (message.length > -1) {
          this.logs.push({
            time: Date.now(),
            message: message
          });
          sessionStorage.setItem(SESSION_LOGS_ATTR, JSON.stringify(this.logs));
        }
      }
    }, {
      key: "info",
      value: function info() {
        if (this.levels.indexOf('info') > -1) {
          this.writeLog(arguments);
        }
      }
    }, {
      key: "debug",
      value: function debug() {
        if (this.levels.indexOf('debug') > -1) {
          this.writeLog(arguments);
        }
      }
    }, {
      key: "error",
      value: function error() {
        if (this.levels.indexOf('error') > -1) {
          this.writeLog(arguments);
        }
      }
    }, {
      key: "warn",
      value: function warn() {
        if (this.levels.indexOf('warn') > -1) {
          this.writeLog(arguments);
        }
      }
    }], [{
      key: "instance",
      value: function instance() {
        if (!SessionLogger._instance) {
          SessionLogger._instance = new SessionLogger();
          SessionLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return SessionLogger._instance;
      }
    }, {
      key: "info",
      value: function info() {
        SessionLogger.instance().info(arguments);
      }
    }, {
      key: "debug",
      value: function debug() {
        SessionLogger.instance().debug(arguments);
      }
    }, {
      key: "error",
      value: function error() {
        SessionLogger.instance().error(arguments);
      }
    }, {
      key: "warn",
      value: function warn() {
        SessionLogger.instance().warn(arguments);
      }
    }]);
  }();

  function _arrayLikeToArray$j(r, a) {
    (null == a || a > r.length) && (a = r.length);
    for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e];
    return n;
  }

  function _arrayWithoutHoles(r) {
    if (Array.isArray(r)) return _arrayLikeToArray$j(r);
  }

  function _iterableToArray(r) {
    if ("undefined" != typeof Symbol && null != r[Symbol.iterator] || null != r["@@iterator"]) return Array.from(r);
  }

  function _unsupportedIterableToArray$j(r, a) {
    if (r) {
      if ("string" == typeof r) return _arrayLikeToArray$j(r, a);
      var t = {}.toString.call(r).slice(8, -1);
      return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$j(r, a) : void 0;
    }
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _toConsumableArray(r) {
    return _arrayWithoutHoles(r) || _iterableToArray(r) || _unsupportedIterableToArray$j(r) || _nonIterableSpread();
  }

  function asyncGeneratorStep(n, t, e, r, o, a, c) {
    try {
      var i = n[a](c),
        u = i.value;
    } catch (n) {
      return void e(n);
    }
    i.done ? t(u) : Promise.resolve(u).then(r, o);
  }
  function _asyncToGenerator(n) {
    return function () {
      var t = this,
        e = arguments;
      return new Promise(function (r, o) {
        var a = n.apply(t, e);
        function _next(n) {
          asyncGeneratorStep(a, r, o, _next, _throw, "next", n);
        }
        function _throw(n) {
          asyncGeneratorStep(a, r, o, _next, _throw, "throw", n);
        }
        _next(void 0);
      });
    };
  }

  function _arrayWithHoles(r) {
    if (Array.isArray(r)) return r;
  }

  function _iterableToArrayLimit(r, l) {
    var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (null != t) {
      var e,
        n,
        i,
        u,
        a = [],
        f = !0,
        o = !1;
      try {
        if (i = (t = t.call(r)).next, 0 === l) {
          if (Object(t) !== t) return;
          f = !1;
        } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0);
      } catch (r) {
        o = !0, n = r;
      } finally {
        try {
          if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return;
        } finally {
          if (o) throw n;
        }
      }
      return a;
    }
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _slicedToArray(r, e) {
    return _arrayWithHoles(r) || _iterableToArrayLimit(r, e) || _unsupportedIterableToArray$j(r, e) || _nonIterableRest();
  }

  function createCommonjsModule(fn) {
    var module = { exports: {} };
  	return fn(module, module.exports), module.exports;
  }

  var _typeof_1 = createCommonjsModule(function (module) {
    function _typeof(o) {
      "@babel/helpers - typeof";

      return module.exports = _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
        return typeof o;
      } : function (o) {
        return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
      }, module.exports.__esModule = true, module.exports["default"] = module.exports, _typeof(o);
    }
    module.exports = _typeof, module.exports.__esModule = true, module.exports["default"] = module.exports;
  });

  var regeneratorRuntime$1 = createCommonjsModule(function (module) {
    var _typeof = _typeof_1["default"];
    function _regeneratorRuntime() {

      /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
      module.exports = _regeneratorRuntime = function _regeneratorRuntime() {
        return e;
      }, module.exports.__esModule = true, module.exports["default"] = module.exports;
      var t,
        e = {},
        r = Object.prototype,
        n = r.hasOwnProperty,
        o = Object.defineProperty || function (t, e, r) {
          t[e] = r.value;
        },
        i = "function" == typeof Symbol ? Symbol : {},
        a = i.iterator || "@@iterator",
        c = i.asyncIterator || "@@asyncIterator",
        u = i.toStringTag || "@@toStringTag";
      function define(t, e, r) {
        return Object.defineProperty(t, e, {
          value: r,
          enumerable: !0,
          configurable: !0,
          writable: !0
        }), t[e];
      }
      try {
        define({}, "");
      } catch (t) {
        define = function define(t, e, r) {
          return t[e] = r;
        };
      }
      function wrap(t, e, r, n) {
        var i = e && e.prototype instanceof Generator ? e : Generator,
          a = Object.create(i.prototype),
          c = new Context(n || []);
        return o(a, "_invoke", {
          value: makeInvokeMethod(t, r, c)
        }), a;
      }
      function tryCatch(t, e, r) {
        try {
          return {
            type: "normal",
            arg: t.call(e, r)
          };
        } catch (t) {
          return {
            type: "throw",
            arg: t
          };
        }
      }
      e.wrap = wrap;
      var h = "suspendedStart",
        l = "suspendedYield",
        f = "executing",
        s = "completed",
        y = {};
      function Generator() {}
      function GeneratorFunction() {}
      function GeneratorFunctionPrototype() {}
      var p = {};
      define(p, a, function () {
        return this;
      });
      var d = Object.getPrototypeOf,
        v = d && d(d(values([])));
      v && v !== r && n.call(v, a) && (p = v);
      var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p);
      function defineIteratorMethods(t) {
        ["next", "throw", "return"].forEach(function (e) {
          define(t, e, function (t) {
            return this._invoke(e, t);
          });
        });
      }
      function AsyncIterator(t, e) {
        function invoke(r, o, i, a) {
          var c = tryCatch(t[r], t, o);
          if ("throw" !== c.type) {
            var u = c.arg,
              h = u.value;
            return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) {
              invoke("next", t, i, a);
            }, function (t) {
              invoke("throw", t, i, a);
            }) : e.resolve(h).then(function (t) {
              u.value = t, i(u);
            }, function (t) {
              return invoke("throw", t, i, a);
            });
          }
          a(c.arg);
        }
        var r;
        o(this, "_invoke", {
          value: function value(t, n) {
            function callInvokeWithMethodAndArg() {
              return new e(function (e, r) {
                invoke(t, n, e, r);
              });
            }
            return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
          }
        });
      }
      function makeInvokeMethod(e, r, n) {
        var o = h;
        return function (i, a) {
          if (o === f) throw Error("Generator is already running");
          if (o === s) {
            if ("throw" === i) throw a;
            return {
              value: t,
              done: !0
            };
          }
          for (n.method = i, n.arg = a;;) {
            var c = n.delegate;
            if (c) {
              var u = maybeInvokeDelegate(c, n);
              if (u) {
                if (u === y) continue;
                return u;
              }
            }
            if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) {
              if (o === h) throw o = s, n.arg;
              n.dispatchException(n.arg);
            } else "return" === n.method && n.abrupt("return", n.arg);
            o = f;
            var p = tryCatch(e, r, n);
            if ("normal" === p.type) {
              if (o = n.done ? s : l, p.arg === y) continue;
              return {
                value: p.arg,
                done: n.done
              };
            }
            "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg);
          }
        };
      }
      function maybeInvokeDelegate(e, r) {
        var n = r.method,
          o = e.iterator[n];
        if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y;
        var i = tryCatch(o, e.iterator, r.arg);
        if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y;
        var a = i.arg;
        return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y);
      }
      function pushTryEntry(t) {
        var e = {
          tryLoc: t[0]
        };
        1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e);
      }
      function resetTryEntry(t) {
        var e = t.completion || {};
        e.type = "normal", delete e.arg, t.completion = e;
      }
      function Context(t) {
        this.tryEntries = [{
          tryLoc: "root"
        }], t.forEach(pushTryEntry, this), this.reset(!0);
      }
      function values(e) {
        if (e || "" === e) {
          var r = e[a];
          if (r) return r.call(e);
          if ("function" == typeof e.next) return e;
          if (!isNaN(e.length)) {
            var o = -1,
              i = function next() {
                for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next;
                return next.value = t, next.done = !0, next;
              };
            return i.next = i;
          }
        }
        throw new TypeError(_typeof(e) + " is not iterable");
      }
      return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", {
        value: GeneratorFunctionPrototype,
        configurable: !0
      }), o(GeneratorFunctionPrototype, "constructor", {
        value: GeneratorFunction,
        configurable: !0
      }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
        var e = "function" == typeof t && t.constructor;
        return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name));
      }, e.mark = function (t) {
        return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t;
      }, e.awrap = function (t) {
        return {
          __await: t
        };
      }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () {
        return this;
      }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) {
        void 0 === i && (i = Promise);
        var a = new AsyncIterator(wrap(t, r, n, o), i);
        return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
          return t.done ? t.value : a.next();
        });
      }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () {
        return this;
      }), define(g, "toString", function () {
        return "[object Generator]";
      }), e.keys = function (t) {
        var e = Object(t),
          r = [];
        for (var n in e) r.push(n);
        return r.reverse(), function next() {
          for (; r.length;) {
            var t = r.pop();
            if (t in e) return next.value = t, next.done = !1, next;
          }
          return next.done = !0, next;
        };
      }, e.values = values, Context.prototype = {
        constructor: Context,
        reset: function reset(e) {
          if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t);
        },
        stop: function stop() {
          this.done = !0;
          var t = this.tryEntries[0].completion;
          if ("throw" === t.type) throw t.arg;
          return this.rval;
        },
        dispatchException: function dispatchException(e) {
          if (this.done) throw e;
          var r = this;
          function handle(n, o) {
            return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o;
          }
          for (var o = this.tryEntries.length - 1; o >= 0; --o) {
            var i = this.tryEntries[o],
              a = i.completion;
            if ("root" === i.tryLoc) return handle("end");
            if (i.tryLoc <= this.prev) {
              var c = n.call(i, "catchLoc"),
                u = n.call(i, "finallyLoc");
              if (c && u) {
                if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
                if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
              } else if (c) {
                if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
              } else {
                if (!u) throw Error("try statement without catch or finally");
                if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
              }
            }
          }
        },
        abrupt: function abrupt(t, e) {
          for (var r = this.tryEntries.length - 1; r >= 0; --r) {
            var o = this.tryEntries[r];
            if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
              var i = o;
              break;
            }
          }
          i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
          var a = i ? i.completion : {};
          return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a);
        },
        complete: function complete(t, e) {
          if ("throw" === t.type) throw t.arg;
          return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y;
        },
        finish: function finish(t) {
          for (var e = this.tryEntries.length - 1; e >= 0; --e) {
            var r = this.tryEntries[e];
            if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y;
          }
        },
        "catch": function _catch(t) {
          for (var e = this.tryEntries.length - 1; e >= 0; --e) {
            var r = this.tryEntries[e];
            if (r.tryLoc === t) {
              var n = r.completion;
              if ("throw" === n.type) {
                var o = n.arg;
                resetTryEntry(r);
              }
              return o;
            }
          }
          throw Error("illegal catch attempt");
        },
        delegateYield: function delegateYield(e, r, n) {
          return this.delegate = {
            iterator: values(e),
            resultName: r,
            nextLoc: n
          }, "next" === this.method && (this.arg = t), y;
        }
      }, e;
    }
    module.exports = _regeneratorRuntime, module.exports.__esModule = true, module.exports["default"] = module.exports;
  });

  // TODO(Babel 8): Remove this file.

  var runtime = regeneratorRuntime$1();
  var regenerator = runtime;

  // Copied from https://github.com/facebook/regenerator/blob/main/packages/runtime/runtime.js#L736=
  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    if ((typeof globalThis === "undefined" ? "undefined" : _typeof(globalThis)) === "object") {
      globalThis.regeneratorRuntime = runtime;
    } else {
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }

  var SkTheme = /*#__PURE__*/_createClass(function SkTheme(configEl) {
    _classCallCheck(this, SkTheme);
    this.configEl = configEl;
  });

  function _callSuper$16(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$16() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$16() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$16 = function _isNativeReflectConstruct() { return !!t; })(); }
  var DefaultTheme = /*#__PURE__*/function (_SkTheme) {
    function DefaultTheme() {
      _classCallCheck(this, DefaultTheme);
      return _callSuper$16(this, DefaultTheme, arguments);
    }
    _inherits(DefaultTheme, _SkTheme);
    return _createClass(DefaultTheme, [{
      key: "basePath",
      get: function get() {
        if (!this._basePath) {
          this._basePath = this.configEl ? "".concat(this.configEl.basePath, "/theme/default") : '/node_modules/skinny-widgets/theme/default';
        }
        return this._basePath;
      }
    }, {
      key: "styles",
      get: function get() {
        if (!this._styles) {
          this._styles = {
            'default.css': "".concat(this.basePath, "/default.css")
          };
        }
        return this._styles;
      }
    }]);
  }(SkTheme);

  var SkThemes = /*#__PURE__*/function () {
    function SkThemes() {
      _classCallCheck(this, SkThemes);
    }
    return _createClass(SkThemes, null, [{
      key: "capitalizeFirstLetter",
      value: function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
    }, {
      key: "byName",
      value: function byName(name, config) {
        if (name && name !== 'default') {
          var themePath = '/node_modules/sk-theme-' + name + '/src/' + name + '-theme.js';
          var className = SkThemes.capitalizeFirstLetter(name) + 'Theme';
          var dynImportOff = config.hasAttribute('dimport') && config.getAttribute('dimport') === 'false';
          if (dynImportOff) {
            return ResLoader.loadClass(className, themePath, null, false, true);
          } else {
            return ResLoader.loadClass(className, themePath);
          }
        } else {
          return Promise.resolve(new DefaultTheme(config));
        }
      }
    }]);
  }();

  function _defineProperty(e, r, t) {
    return (r = toPropertyKey(r)) in e ? Object.defineProperty(e, r, {
      value: t,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : e[r] = t, e;
  }

  var EventTarget = function EventTarget() {
    this.listeners = {};
  };
  EventTarget.prototype.listeners = null;
  EventTarget.prototype.addEventListener = function (type, callback) {
    if (!(type in this.listeners)) {
      this.listeners[type] = [];
    }
    this.listeners[type].push(callback);
  };
  EventTarget.prototype.removeEventListener = function (type, callback) {
    if (!(type in this.listeners)) {
      return;
    }
    var stack = this.listeners[type];
    for (var i = 0, l = stack.length; i < l; i++) {
      if (stack[i] === callback) {
        stack.splice(i, 1);
        return;
      }
    }
  };
  EventTarget.prototype.dispatchEvent = function (event) {
    if (!(event.type in this.listeners)) {
      return true;
    }
    var stack = this.listeners[event.type].slice();
    for (var i = 0, l = stack.length; i < l; i++) {
      stack[i].call(this, event);
    }
    return !event.defaultPrevented;
  };

  function _callSuper$15(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$15() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$15() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$15 = function _isNativeReflectConstruct() { return !!t; })(); }
  var SK_RENDER_EVT = 'skrender';
  var SkRenderEvent = /*#__PURE__*/function (_CustomEvent) {
    function SkRenderEvent(options) {
      _classCallCheck(this, SkRenderEvent);
      return _callSuper$15(this, SkRenderEvent, [SK_RENDER_EVT, options]);
    }
    _inherits(SkRenderEvent, _CustomEvent);
    return _createClass(SkRenderEvent);
  }(/*#__PURE__*/_wrapNativeSuper(CustomEvent));

  function ownKeys$1(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
  function _objectSpread$1(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys$1(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$1(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
  function _createForOfIteratorHelper$i(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$i(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$i(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$i(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$i(r, a) : void 0; } }
  function _arrayLikeToArray$i(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$14(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$14() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$14() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$14 = function _isNativeReflectConstruct() { return !!t; })(); }

  /**
   * elements than can have action attribute to be binded with event or callback, currently in sk-dialog and sk-form only
   * @type {string}
   */
  var ACTION_SUBELEMENTS_SL = 'button,sk-button';
  var SkComponentImpl = /*#__PURE__*/function (_EventTarget) {
    function SkComponentImpl(comp) {
      var _this;
      _classCallCheck(this, SkComponentImpl);
      _this = _callSuper$14(this, SkComponentImpl);
      _this.comp = comp;
      return _this;
    }
    _inherits(SkComponentImpl, _EventTarget);
    return _createClass(SkComponentImpl, [{
      key: "tplPath",
      get:
      /*    get tplPath() {
              if (! this._tplPath) {
                  this._tplPath = this.comp.tplPath || `${this.themePath}/${this.prefix}-sk-${this.suffix}.tpl.html`;
              }
              return this._tplPath;
          }*/

      function get() {
        if (!this._tplPath) {
          if (this.comp.tplPath) {
            this._tplPath = this.comp.tplPath;
          } else {
            this._tplPath = this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path') ? "".concat(this.comp.configEl.getAttribute('tpl-path'), "/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html") : "/node_modules/sk-".concat(this.comp.cnSuffix, "-").concat(this.prefix, "/src/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html");
          }
        }
        return this._tplPath;
      },
      set: function set(tplPath) {
        this.comp.setAttribute('tpl-path', tplPath);
        this.tplPath;
      }
    }, {
      key: "themePath",
      get: function get() {
        if (this.skTheme && this.skTheme.basePath) {
          return this.skTheme.basePath;
        } else {
          var basePath = this.comp.basePath.toString();
          this.logger.warn('theme path is computed from basePath', basePath, this.skTheme);
          if (basePath.endsWith('/')) {
            // remove traling slash
            basePath = basePath.substr(0, basePath.length - 1);
          }
          return "".concat(basePath, "/theme/").concat(this.comp.theme);
        }
      }
    }, {
      key: "mountedStyles",
      get: function get() {
        if (!this._mountedStyles) {
          this._mountedStyles = {};
        }
        return this._mountedStyles;
      },
      set: function set(mountedStyles) {
        this._mountedStyles = mountedStyles;
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {}
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {}
    }, {
      key: "dumpState",
      value: function dumpState() {}
    }, {
      key: "saveState",
      value: function saveState() {
        if (!this.comp.contentsState) {
          if (this.comp.useShadowRoot) {
            this.comp.contentsState = this.comp.el.host.innerHTML;
            this.comp.el.host.innerHTML = '';
          } else {
            this.comp.contentsState = this.comp.el.innerHTML;
            this.comp.el.innerHTML = '';
          }
        }
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {}
    }, {
      key: "enable",
      value: function enable() {}
    }, {
      key: "disable",
      value: function disable() {}
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        this.comp.dispatchEvent(new CustomEvent('skafterrendered'));
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        this.comp.dispatchEvent(new CustomEvent('skbeforerendered'));
      }
    }, {
      key: "unmountStyles",
      value: function unmountStyles() {}
    }, {
      key: "logger",
      get: function get() {
        return this.comp.logger;
      }
    }, {
      key: "findStyle",
      value: function findStyle(seek) {
        var links = document.querySelectorAll('link');
        var matched = [];
        var _iterator = _createForOfIteratorHelper$i(links),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var link = _step.value;
            if (Array.isArray(seek)) {
              var _iterator2 = _createForOfIteratorHelper$i(seek),
                _step2;
              try {
                for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                  var seekItem = _step2.value;
                  if (link.href.match(seekItem)) {
                    matched.push(link);
                    break;
                  }
                }
              } catch (err) {
                _iterator2.e(err);
              } finally {
                _iterator2.f();
              }
            } else {
              if (link.href.match(seek)) {
                matched.push(link);
              }
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        return matched;
      }
    }, {
      key: "attachStyleByName",
      value: function attachStyleByName(name, target) {
        if (!target) {
          target = this.comp.el;
        }
        var path = this.comp.configEl.styles[name];
        if (path) {
          var link = document.createElement('link');
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', path);
          target.appendChild(link);
        } else {
          this.logger.warn('style not found with config el', name);
        }
      }
    }, {
      key: "attachStyleByPath",
      value: function attachStyleByPath(path, target) {
        if (!target) {
          target = this.comp.el;
        }
        if (target.querySelector("link[href='".concat(path, "']")) == null) {
          var link = document.createElement('link');
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', path);
          target.appendChild(link);
        }
      }
    }, {
      key: "genStyleLinks",
      value: function genStyleLinks() {
        if (this.comp.hasAttribute('styles') && this.comp.getAttribute('styles') === 'false') {
          return false;
        }
        if (this.skTheme && this.skTheme.styles) {
          var styles = this.skTheme.styles;
          var configStyles = this.comp.configEl && this.comp.configEl.styles ? Object.keys(this.comp.configEl.styles) : [];
          for (var _i = 0, _Object$keys = Object.keys(styles); _i < _Object$keys.length; _i++) {
            var styleName = _Object$keys[_i];
            var href = configStyles.includes(styleName) ? this.comp.configEl.styles[styleName] : styles[styleName];
            var fileName = this.fileNameFromUrl(href);
            if (!this.mountedStyles[fileName]) {
              // mount only not mounted before
              var link = document.createElement('link');
              link.setAttribute('rel', 'stylesheet');
              link.setAttribute('href', href);
              this.comp.el.appendChild(link);
              this.mountedStyles[fileName] = link;
              this.logger.info("mounted style: ".concat(fileName, " as ").concat(href, " to component: ").concat(this.comp.tagName));
            }
          }
        }
      }
    }, {
      key: "mountStyles",
      value: function mountStyles() {
        if (!this.skTheme && this.comp.skThemeLoader) {
          // :TODO optimize this
          this.comp.skThemeLoader.then(function (skTheme) {
            this.genStyleLinks();
          }.bind(this));
        } else {
          this.genStyleLinks();
        }
      }
    }, {
      key: "mountStyle",
      value: function mountStyle(fileNames, target) {
        if (!target) {
          target = this.comp.el;
        }
        if (this.comp.useShadowRoot) {
          if (this.comp.configEl && this.comp.configEl.styles) {
            if (Array.isArray(fileNames)) {
              var _iterator3 = _createForOfIteratorHelper$i(fileNames),
                _step3;
              try {
                for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                  var name = _step3.value;
                  this.attachStyleByName(name, target);
                }
              } catch (err) {
                _iterator3.e(err);
              } finally {
                _iterator3.f();
              }
            } else {
              this.attachStyleByName(fileNames, target);
            }
          } else {
            var styles = this.findStyle(fileNames);
            if (styles && styles.length > 0) {
              var style = styles[0];
              if (style) {
                target.appendChild(document.importNode(style));
              } else {
                this.logger.warn('style not found', fileNames);
              }
            }
          }
        }
      }
    }, {
      key: "clearTplCache",
      value: function clearTplCache() {
        if (this.cachedTplId) {
          var tplEl = document.querySelector('#' + this.cachedTplId);
          if (tplEl !== null) {
            tplEl.remove();
          }
        }
      }
    }, {
      key: "cachedTplId",
      get: function get() {
        return this.comp.constructor.name + 'Tpl';
      }
    }, {
      key: "skFormParent",
      get: function get() {
        if (this._skFormParent === undefined) {
          this._skFormParent = this.findParent(this.comp, 'SK-FORM');
        }
        return this._skFormParent;
      }
    }, {
      key: "idGenEnabled",
      value: function idGenEnabled() {
        return this.comp.configEl && this.comp.configEl.hasAttribute('gen-ids') && this.comp.configEl.hasAttribute('gen-ids') !== 'false';
      }
    }, {
      key: "isInIE",
      value: function isInIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        return false;
      }
    }, {
      key: "initTheme",
      value: function initTheme() {
        if (!this.skTheme) {
          try {
            var cn = this.comp.capitalizeFirstLetter(this.comp.theme) + 'Theme';
            var def = Function('return ' + cn)();
            if (def) {
              this.skTheme = new def(this.comp.configEl);
              if (this.comp.configEl) {
                this.comp.configEl.skTheme = this.skTheme;
              }
              return Promise.resolve(this.skTheme);
            }
          } catch (_unused) {
            return new Promise(function (resolve) {
              this.comp.skThemeLoader.then(function (skTheme) {
                if (this.comp.configEl && this.comp.configEl.skTheme) {
                  this.skTheme = this.comp.configEl.skTheme;
                  resolve(this.skTheme);
                } else {
                  var _cn = this.comp.capitalizeFirstLetter(this.comp.theme) + 'Theme';
                  var _def = false;
                  if (skTheme[_cn]) {
                    _def = skTheme[_cn];
                  } else {
                    try {
                      _def = Function('return ' + _cn)();
                    } catch (_unused2) {
                      this.logger.warn('unable to load theme class');
                    }
                  }
                  if (_def) {
                    this.skTheme = new _def(this.comp.configEl);
                    if (this.comp.configEl) {
                      this.comp.configEl.skTheme = this.skTheme;
                    }
                  }
                  resolve(this.skTheme);
                }
              }.bind(this));
            }.bind(this));
          }
        } else {
          return Promise.resolve(this.skTheme);
        }
      }
    }, {
      key: "doRender",
      value: function doRender() {
        var id = this.getOrGenId();
        this.beforeRendered();
        this.renderWithVars(id);
        this.indexMountedStyles();
        this.afterRendered();
        this.bindEvents();
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        if (this.comp.configEl && this.comp.skConfigElPolicy() === SK_CONFIG_EL_INDIVIDUAL) {
          this.comp.el.appendChild(this.comp.configEl);
        }
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', {
          bubbles: true,
          composed: true
        })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({
          bubbles: true,
          composed: true
        }));
        if (this.comp.renderDeferred) {
          this.comp.renderDeferred.resolve(this.comp);
        }
      }
    }, {
      key: "noTplRendering",
      value: function noTplRendering() {
        if (!this.tplPath) {
          return true;
        }
        var undef = this.tplPath.match('undefined');
        if (undef !== null && undef.length > 0) {
          return true;
        }
        return false;
      }
    }, {
      key: "renderImpl",
      value: function renderImpl() {
        var _this2 = this;
        if (this.noTplRendering()) {
          this.doRender();
        } else {
          var themeLoaded = this.initTheme();
          themeLoaded["finally"](function () {
            var _this3 = this;
            _newArrowCheck(this, _this2);
            // we just need to try loading styles
            this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
            if (!this.comp.tpl) {
              this.comp.renderer.mountTemplate(this.tplPath, this.cachedTplId, this.comp, {
                themePath: this.themePath
              }).then(function (tpl) {
                _newArrowCheck(this, _this3);
                this.comp.tpl = tpl;
                this.doRender();
              }.bind(this));
            } else {
              this.doRender();
            }
          }.bind(this));
        }
      }
    }, {
      key: "getOrGenId",
      value: function getOrGenId() {
        var id;
        if (!this.comp.getAttribute('id') && this.idGenEnabled()) {
          id = this.comp.renderer.genElId(this.comp);
          this.comp.setAttribute('id', id);
        } else {
          id = this.comp.getAttribute('id');
        }
        return id;
      }
    }, {
      key: "tplVars",
      get: function get() {
        if (!this._tplVars) {
          this._tplVars = this.comp.tplVars ? this.comp.tplVars : {};
        }
        return this._tplVars;
      },
      set: function set(tplVars) {
        this._tplVars = tplVars;
      }
    }, {
      key: "i18n",
      get: function get() {
        if (!this._i18n) {
          this._i18n = this.comp._i18n ? this.comp._i18n : {};
        }
        return this._i18n;
      },
      set: function set(i18n) {
        this._i18n = i18n;
      }
    }, {
      key: "renderWithVars",
      value: function renderWithVars(id, targetEl, tpl) {
        tpl = tpl || this.comp.tpl;
        if (!tpl) {
          return false;
        }
        targetEl = targetEl || this.comp.el;
        var el = this.comp.renderer.prepareTemplate(tpl);
        var rendered;
        if (this.comp.renderer.variableRender && this.comp.renderer.variableRender !== 'false') {
          rendered = this.comp.renderer.renderMustacheVars(el, _objectSpread$1(_objectSpread$1({
            id: id,
            themePath: this.themePath
          }, this.tplVars), this.i18n));
          targetEl.innerHTML = '';
          targetEl.innerHTML = rendered;
        } else {
          targetEl.innerHTML = '';
          targetEl.appendChild(el);
        }
      }
    }, {
      key: "fileNameFromUrl",
      value: function fileNameFromUrl(url) {
        return url ? url.substring(url.lastIndexOf('/') + 1) : null;
      }
    }, {
      key: "indexMountedStyles",
      value: function indexMountedStyles() {
        if (this.comp.hasAttribute('styles') && this.comp.getAttribute('styles') === 'false') {
          return false;
        }
        var links = this.comp.el.querySelectorAll('link');
        this.mountedStyles = {};
        var _iterator4 = _createForOfIteratorHelper$i(links),
          _step4;
        try {
          for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
            var link = _step4.value;
            var fileName = this.fileNameFromUrl(link.getAttribute('href'));
            this.mountedStyles[fileName] = link;
          }
        } catch (err) {
          _iterator4.e(err);
        } finally {
          _iterator4.f();
        }
        var styles = this.comp.el.querySelectorAll('style');
        var _iterator5 = _createForOfIteratorHelper$i(styles),
          _step5;
        try {
          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var style = _step5.value;
            var _fileName = style.hasAttribute('data-file-name') ? style.getAttribute('data-file-name') : null;
            if (_fileName) {
              this.mountedStyles[_fileName] = style;
            }
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
      }
    }, {
      key: "findParent",
      value: function findParent(el, tag) {
        while (el.parentNode) {
          el = el.parentNode;
          if (el.tagName === tag) return el;
        }
        return null;
      }
    }, {
      key: "validateWithAttrs",
      value: function validateWithAttrs() {
        if (this.skFormParent) {
          // if has sk-form parents, rely on it
          return true;
        }
        this.hasOwnValidator = false;
        var validatorNames = this.skValidators ? Object.keys(this.skValidators) : [];
        var _iterator6 = _createForOfIteratorHelper$i(validatorNames),
          _step6;
        try {
          for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
            var validatorName = _step6.value;
            // :TODO check there is no parent sk-form first
            if (this.comp.hasAttribute(validatorName)) {
              this.hasOwnValidator = true;
              var validator = this.skValidators[validatorName];
              var result = validator.validate(this.comp);
              return result;
            }
          }
        } catch (err) {
          _iterator6.e(err);
        } finally {
          _iterator6.f();
        }
        if (!this.hasOwnValidator) {
          // any value is valid if no validator defined
          return true;
        }
        return false;
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          this.renderValidation();
        }
      }
    }, {
      key: "showValid",
      value: function showValid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          this.flushValidation();
        }
      }
    }, {
      key: "renderValidation",
      value: function renderValidation(validity) {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          var msgEl = this.comp.el.querySelector('.form-validation-message');
          if (!msgEl) {
            msgEl = this.comp.renderer.createEl('span');
            msgEl.classList.add('form-validation-message');
            msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
            this.comp.el.appendChild(msgEl);
          } else {
            msgEl.innerHTML = '';
            msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
          }
        }
      }
    }, {
      key: "flushValidation",
      value: function flushValidation() {
        var msgEl = this.comp.el.querySelector('.form-validation-message');
        if (msgEl) {
          if (msgEl.parentElement) {
            msgEl.parentElement.removeChild(msgEl);
          } else {
            msgEl.parentNode.removeChild(msgEl);
          }
        }
      }
    }, {
      key: "getScrollTop",
      value: function getScrollTop() {
        if (typeof pageYOffset != 'undefined') {
          return pageYOffset;
        } else {
          var body = _document.body;
          var _document = _document.documentElement;
          _document = _document.clientHeight ? _document : body;
          return _document.scrollTop;
        }
      }
    }, {
      key: "actionSubElementsSl",
      get: function get() {
        if (!this._actionSubElementsSl) {
          this._actionSubElementsSl = ACTION_SUBELEMENTS_SL;
        }
        return this._actionSubElementsSl;
      },
      set: function set(sl) {
        this._actionSubElementsSl = sl;
      }
    }, {
      key: "bindAction",
      value: function bindAction(button, action) {
        if (this['on' + action]) {
          // handler defined on custom element
          button.onclick = function (event) {
            this['on' + action]();
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        } else if (this.comp['on' + action]) {
          // handler defined in implementation
          button.onclick = function (event) {
            this.comp['on' + action]();
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        } else {
          // no handler binded just throw and event
          button.onclick = function (event) {
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        }
      }
    }, {
      key: "bindActions",
      value: function bindActions(target) {
        target = target ? target : this;
        var buttons = target.querySelectorAll(this.actionSubElementsSl);
        var _iterator7 = _createForOfIteratorHelper$i(buttons),
          _step7;
        try {
          for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
            var button = _step7.value;
            if (button.hasAttribute('action')) {
              var action = button.getAttribute('action');
              this.bindAction(button, action);
            }
            if (button.dataset.action) {
              var _action = button.dataset.action;
              this.bindAction(button, _action);
            }
          }
        } catch (err) {
          _iterator7.e(err);
        } finally {
          _iterator7.f();
        }
      }
    }, {
      key: "subEls",
      get: function get() {
        return [];
      }
    }, {
      key: "clearAllElCache",
      value: function clearAllElCache() {
        var _iterator8 = _createForOfIteratorHelper$i(this.subEls),
          _step8;
        try {
          for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
            var el = _step8.value;
            if (this[el]) {
              this[el] = null;
            }
          }
        } catch (err) {
          _iterator8.e(err);
        } finally {
          _iterator8.f();
        }
        this.comp.clearElCache();
      }
    }, {
      key: "removeEl",
      value: function removeEl(el) {
        if (typeof el.remove === 'function') {
          el.remove();
        } else if (el.parentElement) {
          el.parentElement.removeChild(el);
        }
      }
    }]);
  }(EventTarget);

  var SkLocaleEn = {};

  var SkLocaleDe = {
    'Field value is invalid': 'Das ist ungültig',
    'This field is required': 'Dies ist erforderlich',
    'The value is less than required': 'Das muss größer sein',
    'The value is greater than required': 'Das muss weniger sein',
    'The value must me a valid email': 'Das muss eine E-Mail sein',
    'The value must match requirements': 'Dies muss den Anforderungen entsprechen',
    'Ok': 'Ок',
    'Cancel': 'Cancel'
  };

  var SkLocaleRu = {
    'Field value is invalid': 'Ошибка ввода данных',
    'This field is required': 'Это поле обязательное',
    'The value is less than required': 'Значение меньше допустимого',
    'The value is greater than required': 'Значение больше допустимого',
    'The value must me a valid email': 'Значение должно быть email адресом',
    'The value must match requirements': 'Значение должно соответствовать',
    'Ok': 'Ок',
    'Cancel': 'Отмена'
  };

  var SkLocaleCn = {
    'Field value is invalid': '不对',
    'This field is required': '必填',
    'The value is less than required': '太小',
    'The value is greater than required': '太大',
    'The value must me a valid email': '需要电子邮件',
    'The value must match requirements': '不对',
    'Ok': '好的',
    'Cancel': '消除'
  };

  var SkLocaleHi = {
    'Field value is invalid': 'यह अमान्य है',
    'This field is required': 'यह आवश्यक है',
    'The value is less than required': 'यह अधिक होना चाहिए',
    'The value is greater than required': 'यह कम होना चाहिए',
    'The value must me a valid email': 'यह ईमेल होना चाहिए',
    'The value must match requirements': 'यह आवश्यकताओं से मेल खाना चाहिए',
    'Ok': 'Ок',
    'Cancel': 'रद्द करें'
  };

  var LANGS_BY_CODES = {
    'en_US': 'EN',
    'en_UK': 'EN',
    'en': 'EN',
    'EN': 'EN',
    'us': 'EN',
    'ru_RU': 'RU',
    'ru': 'RU',
    'cn': 'CN',
    'CN': 'CN',
    'zh': 'CN',
    'de_DE': 'DE',
    'de': 'DE',
    'DE': 'DE',
    'hi-IN': 'HI',
    'hi_IN': 'HI',
    'HI': 'HI',
    'hi': 'HI'
  };
  var LOCALES = {
    'EN': SkLocaleEn,
    'DE': SkLocaleDe,
    'RU': SkLocaleRu,
    'CN': SkLocaleCn,
    'HI': SkLocaleHi
  };
  var SkLocale = /*#__PURE__*/function () {
    function SkLocale(lang) {
      _classCallCheck(this, SkLocale);
      this.lang = LANGS_BY_CODES[lang];
    }
    return _createClass(SkLocale, [{
      key: "tr",
      value: function tr(string) {
        if (this.lang) {
          return LOCALES[this.lang][string] ? LOCALES[this.lang][string] : string;
        } else {
          return SkLocaleEn[string] ? SkLocaleEn[string] : string;
        }
      }
    }]);
  }();

  function _callSuper$13(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$13() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$13() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$13 = function _isNativeReflectConstruct() { return !!t; })(); }
  var SK_ATTR_CHANGE_EVT = 'skattrchange';
  var SkAttrChangeEvent = /*#__PURE__*/function (_CustomEvent) {
    function SkAttrChangeEvent(options) {
      _classCallCheck(this, SkAttrChangeEvent);
      return _callSuper$13(this, SkAttrChangeEvent, [SK_ATTR_CHANGE_EVT, options]);
    }
    _inherits(SkAttrChangeEvent, _CustomEvent);
    return _createClass(SkAttrChangeEvent);
  }(/*#__PURE__*/_wrapNativeSuper(CustomEvent));

  var JsonPathPlusDriver = /*#__PURE__*/function () {
    function JsonPathPlusDriver() {
      _classCallCheck(this, JsonPathPlusDriver);
    }
    return _createClass(JsonPathPlusDriver, [{
      key: "query",
      value: function query(data, path) {
        return window.JSONPath.JSONPath({
          path: path,
          json: data
        });
      }
    }]);
  }();

  function _createForOfIteratorHelper$h(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$h(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$h(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$h(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$h(r, a) : void 0; } }
  function _arrayLikeToArray$h(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  var JSONPATH_CN = "JsonPath";
  var JSONPATH_PT = "/node_modules/sk-core/src/json-path.js";
  if (window.hasOwnProperty('jsonpath')) {
    if (typeof window.jsonpath.JSONPath === 'function') {
      new window.jsonpath.JSONPath();
    }
  }
  var logger$1 = ConsoleLogger.instance();
  var SkJsonPathDriver = /*#__PURE__*/function () {
    function SkJsonPathDriver() {
      _classCallCheck(this, SkJsonPathDriver);
    }
    return _createClass(SkJsonPathDriver, [{
      key: "libPath",
      get: function get() {
        return window.skJsonPathUrl ? window.skJsonPathUrl : '/node_modules/sk-jsonpath/src/jsonpath.js';
      }
    }, {
      key: "lib",
      value: function lib() {
        if (!this._lib) {
          if (window.hasOwnProperty('jsonpath')) {
            this._lib = typeof window.jsonpath.JSONPath === 'function' ? window.jsonpath.JSONPath() : null;
          } else {
            this.libLoadPromise = new Promise(function (resolve, reject) {
              var _this = this;
              var onLoaded = ResLoader.loadClass('JSONPath', this.libPath, null, false, true);
              onLoaded.then(function () {
                _newArrowCheck(this, _this);
                if (window.hasOwnProperty('jsonpath')) {
                  this._lib = new window.jsonpath.JSONPath();
                  resolve(this._lib);
                } else {
                  reject('Unable to load sk-jsonpath library');
                }
              }.bind(this));
            }.bind(this));
          }
        }
        return this._lib;
      }
    }, {
      key: "query",
      value: function query() {
        var args = arguments;
        var impl = null;
        if (this._lib && typeof this._lib.query === 'function') {
          impl = this._lib;
        }
        if (!impl) {
          if (!this.notLoadedWarnShown) {
            logger$1.warn('JSONPath lib not inited, using fallback impl');
            this.notLoadedWarnShown = true;
          }
          var pathString = args[1] ? args[1].replace('$.', '') : '';
          // fallback simple recursive path drill impl
          var data = args[0];
          var path = pathString.split('.');
          if (path.length == 1) {
            return [data[path[0]]];
          } else {
            var item = data;
            var _iterator = _createForOfIteratorHelper$h(path),
              _step;
            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var token = _step.value;
                item = item[token];
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
            return item;
          }
        } else {
          var _impl;
          return (_impl = impl).query.apply(_impl, _toConsumableArray(args));
        }
      }
    }]);
  }();
  var JsonPath = /*#__PURE__*/function () {
    function JsonPath() {
      _classCallCheck(this, JsonPath);
    }
    return _createClass(JsonPath, [{
      key: "driClass",
      get: function get() {
        return window.JsonPathPlusEsmDriver ?
        // esm driver bundled at compile time
        window.JsonPathPlusEsmDriver : window.JSONPath && window.JSONPath.JSONPath ?
        // jsonpath-plus library pluged statically at runtime
        JsonPathPlusDriver : SkJsonPathDriver; //fallback
      }
    }, {
      key: "dri",
      get: function get() {
        if (!this._dri) {
          this._dri = new this.driClass();
        }
        return this._dri;
      },
      set: function set(dri) {
        this._dri = dri;
      }
    }, {
      key: "query",
      value: function query() {
        var _this$dri;
        return (_this$dri = this.dri).query.apply(_this$dri, arguments);
      }
    }]);
  }();

  var CONFIG_ATTRS_BY_CC = new Map([['theme', 'theme'], ['basePath', 'base-path'], ['tplPath', 'tpl-path'], ['themePath', 'theme-path'], ['lang', 'lang'], ['styles', 'styles'], ['useShadowRoot', 'use-shadow-root'], ['reflective', 'reflective'], ['genIds', 'gen-ids'], ['rdVarRender', 'rd-var-render'], ['rdCache', 'rd-cache'], ['rdCacheGlobal', 'rd-cache-global'], ['rdTplFmt', 'rd-tpl-fmt'], ['stStore', 'st-store'], ['logLv', 'log-lv'], ['logT', 'log-t']]);
  var StringUtils = /*#__PURE__*/function () {
    function StringUtils() {
      _classCallCheck(this, StringUtils);
    }
    return _createClass(StringUtils, null, [{
      key: "css2CamelCase",
      value:
      /**
       * converts string-in-css-format to camelCase
       * @param str
       * @returns str
       */
      function css2CamelCase(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
          return index === 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '').replace(/-/g, '');
      }
    }, {
      key: "capitalizeFirstLetter",
      value: function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
    }, {
      key: "camelCase2Css",
      value: function camelCase2Css(str) {
        var _this = this;
        if (!StringUtils._camel2CssCache) {
          StringUtils._camel2CssCache = CONFIG_ATTRS_BY_CC;
        }
        if (!StringUtils._camel2CssCache.get(str)) {
          var arr = str.split(/(?=[A-Z])/);
          var cssStr = arr.map(function (item, index) {
            _newArrowCheck(this, _this);
            return item.slice(0, 1).toLowerCase() + item.slice(1, item.length);
          }.bind(this)).join("-");
          StringUtils._camel2CssCache.set(str, cssStr);
        }
        return StringUtils._camel2CssCache.get(str);
      }
    }]);
  }();

  var _this$c = undefined;
  var hash = function hash(string) {
    _newArrowCheck(this, _this$c);
    var hash = 0;
    string = string.toString();
    for (var i = 0; i < string.length; i++) {
      hash = (hash << 5) - hash + string.charCodeAt(i) & 0xFFFFFFFF;
    }
    return hash;
  }.bind(undefined);
  var MAX_RECURSION = 3;
  var object = function object(obj, level) {
    _newArrowCheck(this, _this$c);
    if (obj && typeof obj.getTime === 'function') {
      return obj.getTime();
    }
    var result = 0;
    for (var property in obj) {
      if (Object.hasOwnProperty.call(obj, property)) {
        result += hash(property + value(obj[property], level));
      }
    }
    return result;
  }.bind(undefined);
  var value = function value(_value, level) {
    _newArrowCheck(this, _this$c);
    if (level && level > MAX_RECURSION) {
      return 0;
    }
    level = level ? level : 0;
    level++;
    var type = _value == undefined ? undefined : _typeof(_value);
    return MAPPER[type] ? MAPPER[type](_value, level) + hash(type) : 0;
  }.bind(undefined);
  var MAPPER = {
    string: hash,
    number: hash,
    "boolean": hash,
    object: object
  };

  function _callSuper$12(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$12() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$12() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$12 = function _isNativeReflectConstruct() { return !!t; })(); }
  function _createForOfIteratorHelper$g(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$g(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$g(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$g(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$g(r, a) : void 0; } }
  function _arrayLikeToArray$g(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  ResLoader.cacheClassDefs(SkLocale);
  var SK_RENDERED_EVT = "skrendered";
  var CONFIG_EL_REF_NAME = "configEl";
  var CONFIG_OBJ_REF_NAME = "skConfig";
  var DISABLED_AN = "disabled";
  var SELECTED_AN = "selected";
  var SK_CONFIG_EL_GLOBAL = "skConfigElGlobal";
  var SK_CONFIG_EL_INDIVIDUAL = "skConfigElIndividual";
  var SK_CONFIG_DISABLED = "skConfigElDisabled";
  var JSONPATH_DRI_CN_AN = "jsonpath-dri-cn";
  var JSONPATH_DRI_PATH_AN = "jsonpath-dri-path";
  var ATTR_PLUGINS_AN = "attr-plugins";
  var BASE_PATH_AN = "base-path";
  var BASE_PATH_DEFAULT = "/node_modules/skinny-widgets/src";
  var TPL_PATH_AN = "tpl-path";
  var IMPL_PATH_AN = "impl-path";
  var TPL_VARS_AN = "tpl-vars";
  var THEME_AN = "theme";
  var THEME_DEFAULT = "default";
  var LANG_AN = "lang";
  var LANG_DEFAULT = "en_US";
  var LOG_LEVEL_AN = "log-lv";
  var LOG_TARGET_AN = "log-t";
  var CONFIG_SL_AN = "config-sl";
  var STATE_STORE_AN = "st-store";
  var STATE_AN = "state";
  var RD_NOT_SHARED_AN = "rd-no-shrd";
  var SK_CONFIG_EL_GLOBAL_CN = "sk-config-el-global";
  var SK_CONFIG_EL_INDIVIDUAL_CN = "sk-config-el-individual";
  var USE_SHADOW_ROOT_AN = "use-shadow-root";
  var REFLECTIVE_AN = "reflective";
  var I18N_AN = "i18n";
  var NO_ATTR_EVTS_AN = "no-attr-evts";
  var STORED_CFG_AN = "stored-cfg";
  var STORED_PROPS_AN = "stored-props";
  function _bindAttrPlugins(target) {
    var self = target ? target : this;
    var targetPlugins = target ? target.plugins : this.plugins;
    var plugins = new Map();
    var _iterator = _createForOfIteratorHelper$g(self.attributes),
      _step;
    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var attr = _step.value;
        var cnRes = attr.name.match(/plg-(.*)-cn/);
        if (cnRes) {
          var pluginName = cnRes[1];
          if (!plugins.get(pluginName)) {
            plugins.set(pluginName, {});
          }
          plugins.get(pluginName).className = self.getAttribute(attr.name);
        }
        var pathRes = attr.name.match(/plg-(.*)-path/);
        if (pathRes) {
          var _pluginName = pathRes[1];
          if (!plugins.get(_pluginName)) {
            plugins.set(_pluginName, {});
          }
          plugins.get(_pluginName).path = self.getAttribute(attr.name);
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
    var _iterator2 = _createForOfIteratorHelper$g(plugins),
      _step2;
    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var _step2$value = _slicedToArray(_step2.value, 2),
          name = _step2$value[0],
          plugin = _step2$value[1];
        if (plugin.className && plugin.path) {
          var dimportOff = self.getAttribute(DIMPORTS_AN) === 'false';
          ResLoader.dynLoad(plugin.className, plugin.path, function (def) {
            targetPlugins.push(new def(target));
          }.bind(target), false, false, false, false, false, false, dimportOff);
        }
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  }
  var SkElement = /*#__PURE__*/function (_HTMLElement) {
    function SkElement() {
      _classCallCheck(this, SkElement);
      return _callSuper$12(this, SkElement, arguments);
    }
    _inherits(SkElement, _HTMLElement);
    return _createClass(SkElement, [{
      key: "impl",
      get: function get() {
        if (!this._impl) {
          var StubImpl = /*#__PURE__*/function (_SkComponentImpl) {
            function StubImpl() {
              _classCallCheck(this, StubImpl);
              return _callSuper$12(this, StubImpl, arguments);
            }
            _inherits(StubImpl, _SkComponentImpl);
            return _createClass(StubImpl);
          }(SkComponentImpl);
          this._impl = new StubImpl(this);
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "theme",
      get: function get() {
        return this.getAttribute(THEME_AN) || THEME_DEFAULT;
      },
      set: function set(theme) {
        return this.setAttribute(THEME_AN, theme);
      }
    }, {
      key: "basePath",
      get: function get() {
        // defaulting for packaged library usage
        return this.getAttribute(BASE_PATH_AN) || BASE_PATH_DEFAULT;
      },
      set: function set(basePath) {
        return this.setAttribute(BASE_PATH_AN, basePath);
      }
    }, {
      key: "tplPath",
      get: function get() {
        return this.getAttribute(TPL_PATH_AN);
      },
      set: function set(tplPath) {
        return this.setAttribute(TPL_PATH_AN, tplPath);
      }
    }, {
      key: "useShadowRoot",
      get: function get() {
        if (this.getAttribute(USE_SHADOW_ROOT_AN) === 'false') {
          return false;
        }
        return true;
      }
    }, {
      key: "lang",
      get: function get() {
        return this.getAttribute(LANG_AN) || LANG_DEFAULT;
      }
    }, {
      key: "locale",
      get: function get() {
        if (!this._locale) {
          if (this.configEl) {
            if (this.configEl.locale) {
              if (this.lang === this.configEl.locale.lang) {
                this._locale = this.configEl.locale;
              } else {
                this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
                  return new def(this.lang);
                }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
              }
            } else {
              if (this.lang === this.configEl.lang) {
                this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
                  return new def(this.lang);
                }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
                this.configEl.locale = this.locale;
              } else {
                this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
                  return new def(this.lang);
                }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
              }
            }
          } else {
            this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
              return new def(this.lang);
            }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
          }
        }
        return this._locale;
      },
      set: function set(locale) {
        this._locale = locale;
      }
    }, {
      key: "reflective",
      get: function get() {
        return this.getAttribute(REFLECTIVE_AN) || true;
      }
    }, {
      key: "el",
      get: function get() {
        return this.useShadowRoot ? this.shadowRoot : this;
      }

      /**
       * copied to impl and used in SkCommonImpl.renderWithVars method
       * @returns {{}}
       */
    }, {
      key: "tplVars",
      get: function get() {
        if (!this._tplVars) {
          this._tplVars = this.hasAttribute(TPL_VARS_AN) ? JSON.parse(this.getAttribute(TPL_VARS_AN)) : {};
        }
        return this._tplVars;
      },
      set: function set(tplVars) {
        this._tplVars = tplVars;
      }

      /**
       * copied to impl and used in SkCommonImpl.renderWithVars method
       * @returns {{}}
       */
    }, {
      key: "i18n",
      get: function get() {
        if (!this._i18n) {
          this._i18n = {};
          try {
            if (this.configEl) {
              if (this.configEl.i18n) {
                Object.assign(this._i18n, this.configEl.i18n);
              } else if (this.configEl.hasAttribute(I18N_AN)) {
                // sk-element exists by not registred
                Object.assign(this._i18n, JSON.parse(this.configEl.i18n));
              }
            }
          } catch (e) {
            this.logger.error('error loading localization from sk-config element');
          }
          try {
            if (this.hasAttribute(I18N_AN)) {
              Object.assign(this._i18n, JSON.parse(this.getAttribute(I18N_AN)));
            }
          } catch (e) {
            this.logger.error("error loading ".concat(this.constructor.name, " element's own localization"));
          }
          if (this.renderer && this.renderer.translations && _typeof(this.renderer.translations[this.lang]) === 'object') {
            Object.assign(this._i18n, this.renderer.translations[this.lang]);
          }
        }
        return this._i18n;
      },
      set: function set(i18n) {
        this._i18n = i18n;
      }
    }, {
      key: "implClassName",
      get: function get() {
        return this.capitalizeFirstLetter(this.theme) + 'Sk' + this.capitalizeFirstLetter(this.cnSuffix);
      }
    }, {
      key: "implPath",
      get: function get() {
        return this.getAttribute(IMPL_PATH_AN) || "/node_modules/sk-".concat(this.cnSuffix, "-").concat(this.theme, "/src/").concat(this.theme, "-sk-").concat(this.cnSuffix, ".js");
      }
    }, {
      key: "disabled",
      get: function get() {
        return this.hasAttribute(DISABLED_AN);
      },
      set: function set(disabled) {
        if (disabled) {
          this.setAttribute(DISABLED_AN, '');
        } else {
          this.removeAttribute(DISABLED_AN);
        }
      }
    }, {
      key: "logLevels",
      get: function get() {
        return this.hasAttribute(LOG_LEVEL_AN) ? this.getAttribute(LOG_LEVEL_AN) : this.hasAttribute('log_lv') ? this.getAttribute('log_lv') : LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this.setAttribute(LOG_LEVEL_AN, level);
      }
    }, {
      key: "confValOrDefault",
      value: function confValOrDefault(attrName, defaultVal) {
        if (this.hasAttribute(attrName)) {
          return this.getAttribute(attrName);
        } else if (this.configEl && this.configEl.hasAttribute(attrName)) {
          return this.configEl.getAttribute(attrName);
        } else {
          return defaultVal;
        }
      }
    }, {
      key: "confValEnabled",
      value: function confValEnabled(attrName) {
        var result = false;
        if (this.hasAttribute(attrName)) {
          result = this.getAttribute(attrName);
        } else if (this.configEl && this.configEl.hasAttribute(attrName)) {
          result = this.configEl.getAttribute(attrName);
        } else {
          result = false;
        }
        return result !== false && result !== null && result !== undefined && result !== "false" && result !== "no" && result !== "null";
      }
    }, {
      key: "createLogger",
      value: function createLogger(el) {
        var logger;
        if (el.hasAttribute(LOG_TARGET_AN)) {
          var logTarget = el.getAttribute(LOG_TARGET_AN);
          if (logTarget === 'memory') {
            logger = new MemoryLogger();
          } else if (logTarget === 'session') {
            logger = new SessionLogger();
          } else {
            logger = new ConsoleLogger();
          }
        } else {
          logger = new ConsoleLogger();
        }
        return logger;
      }
    }, {
      key: "logger",
      get: function get() {
        if (!this._logger) {
          if (!this.hasAttribute(LOG_LEVEL_AN)) {
            if (this.configEl && this.configEl.logger) {
              this._logger = this.configEl.logger;
            } else {
              if (this.configEl) {
                this._logger = this.createLogger(this.configEl);
                this._logger.levels = this.logLevels = this.configEl.hasAttribute(LOG_LEVEL_AN) ? this.configEl.getAttribute(LOG_LEVEL_AN) : LOG_LEVEL_DEFAULT;
              } else {
                this._logger = ConsoleLogger.instance();
              }
            }
          } else {
            this._logger = this.createLogger(this);
            this._logger.levels = this.logLevels = this.getAttribute(LOG_LEVEL_AN);
          }
          if (this.configEl && !this.configEl.logger) {
            if (this.configEl.getAttribute(LOG_LEVEL_AN) === this.logLevels || !this.configEl.hasAttribute(LOG_LEVEL_AN) && this.logLevels === LOG_LEVEL_DEFAULT) {
              this.configEl.logger = this._logger;
            }
          }
        }
        return this._logger;
      },
      set: function set(log) {
        this._logger = logger;
      }
    }, {
      key: "dynamicImportSupported",
      value: function dynamicImportSupported() {
        try {
          new Function('import("")');
          return true;
        } catch (err) {
          return false;
        }
      }
    }, {
      key: "isInIE",
      value: function isInIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        return false;
      }
    }, {
      key: "initImpl",
      value: function initImpl() {
        var _this = this;
        try {
          var def = Function('return ' + this.implClassName)();
          this._impl = new def(this);
        } catch (_unused) {
          this._implPromise = ResLoader.loadClass(this.implClassName, this.implPath);
          this._implPromise.then(function (m) {
            _newArrowCheck(this, _this);
            if (typeof m[this.implClassName] === 'function') {
              this._impl = new m[this.implClassName](this);
              this._impl.renderImpl();
            } else {
              this.logger.warn("expected constructor for ".concat(this.implClassName, " not found"));
            }
          }.bind(this));
        }
      }
    }, {
      key: "stateStoreBackend",
      get: function get() {
        return this.getAttribute(STATE_STORE_AN) || false;
      }
    }, {
      key: "contentsState",
      get: function get() {
        if (!this.stateStoreBackend) {
          return JSON.parse(this.getAttribute(STATE_AN));
        } else if (this.stateStoreBackend === 'session') {
          return JSON.parse(sessionStorage.getItem(this.cid));
        }
      },
      set: function set(state) {
        if (!this.stateStoreBackend) {
          this.setAttribute(STATE_AN, JSON.stringify(state));
        } else if (this.stateStoreBackend === 'session') {
          this.cid = this.hasAttribute('id') ? this.getAttribute('id') : null;
          if (!this.cid) {
            this.cid = this.renderer.genElId(this);
          }
          sessionStorage.setItem(this.cid, JSON.stringify(state));
        }
      }
    }, {
      key: "configSl",
      get: function get() {
        return this.getAttribute(CONFIG_SL_AN) || this.getAttribute('configSl') || SK_CONFIG_TN;
      }
    }, {
      key: "configEl",
      get: function get() {
        if (!this._configEl) {
          if (!this.hasAttribute(CONFIG_SL_AN) && !this.hasAttribute('configSl')) {
            if (window[CONFIG_EL_REF_NAME]) {
              this._configEl = window[CONFIG_EL_REF_NAME];
            } else {
              var configEl = document.querySelector(this.configSl);
              if (configEl !== null) {
                this._configEl = configEl;
                window[CONFIG_EL_REF_NAME] = this._configEl;
              }
            }
          } else {
            this._configEl = document.querySelector(this.configSl);
          }
        }
        return this._configEl;
      },
      set: function set(configEl) {
        this._configEl = configEl;
      }
    }, {
      key: "subEls",
      get: function get() {
        return [];
      }
    }, {
      key: "renderer",
      get: function get() {
        return this._renderer;
      },
      set: function set(renderer) {
        this._renderer = renderer;
      }
    }, {
      key: "skThemeLoader",
      get: function get() {
        if (this.configEl && this.configEl.skTheme) {
          return Promise.resolve(this.configEl.skTheme);
        } else {
          if (this.configEl) {
            this.configEl.skThemeLoader = SkThemes.byName(this.theme, this.configEl || this);
            return this.configEl.skThemeLoader;
          } else {
            return SkThemes.byName(this.theme, this.configEl || this);
          }
        }
      }
    }, {
      key: "validationMessage",
      get: function get() {
        return this._validationMessage || this.locale.tr('Field value is invalid');
      },
      set: function set(validationMessage) {
        this._validationMessage = validationMessage;
      }
    }, {
      key: "connections",
      get: function get() {
        if (!this._connections) {
          this._connections = this.constructor.connections || [];
        }
        return this._connections;
      },
      set: function set(connections) {
        this._connections = connections;
      }
    }, {
      key: "jsonPath",
      get: function get() {
        if (!this._jsonPath) {
          this._jsonPath = ResLoader.dynLoad(JSONPATH_CN, JSONPATH_PT, function (def) {
            return new def();
          }.bind(this), false, false, false, false, false, false, true, true, JsonPath);
        }
        return this._jsonPath;
      },
      set: function set(jsonPath) {
        this._jsonPath = jsonPath;
      }
    }, {
      key: "storedCfg",
      get: function get() {
        if (!this._storedCfg) {
          if (this.hasAttribute(STORED_CFG_AN)) {
            try {
              this._storedCfg = JSON.parse(this.getAttribute(STORED_CFG_AN));
            } catch (e) {
              this.logger.warn("Error parsing stored-cfg for element", this);
            }
          }
          if (!this._storedCfg) {
            this._storedCfg = {};
          }
        }
        return this._storedCfg;
      },
      set: function set(storedCfg) {
        this._storedCfg = storedCfg;
        this.setAttribute(STORED_CFG_AN, JSON.stringify(this._storedCfg));
      }
    }, {
      key: "toBool",
      value: function toBool(s) {
        var regex = /^\s*(true|1|on)\s*$/i;
        return regex.test(s);
      }
    }, {
      key: "applyToTarget",
      value: function applyToTarget(source, target, targetType, el, value) {
        if (targetType === 'event' || !targetType) {
          // event is default target type
          var event = target ? new CustomEvent(target, {
            detail: {
              value: value
            }
          }) : new Event('click'); // click is default event
          el.dispatchEvent(event);
        } else if (targetType === 'method') {
          el[target].call(el, value);
        } else if (targetType === 'attr') {
          el.setAttribute(target, value);
        } else if (targetType === 'togglattr') {
          if (value && typeof value === 'string' && value.indexOf("|") > 0) {
            var variants = value.split("|");
            if (el.getAttribute(target) === variants[0]) {
              el.setAttribute(target, variants[1]);
            } else {
              el.setAttribute(target, variants[0]);
            }
          } else if (value && value === 'true' || value === 'false') {
            var val = this.toBool(el.getAttribute(target));
            el.setAttribute(target, !val);
          } else {
            if (el.hasAttribute(target)) {
              el.removeAttribute(target);
            } else {
              el.setAttribute(target, value);
            }
          }
        } else if (targetType === 'prop') {
          el[target] = value;
        } else if (targetType === 'togglprop') {
          var _val = this.toBool(value);
          if (el[target] === _val) {
            el[target] = !_val;
          } else {
            el[target] = _val;
          }
        } else if (targetType === 'function') {
          target.call(el, value);
        }
        this['_' + source] = value;
      }
    }, {
      key: "findTarget",
      value: function findTarget(sl) {
        var el = sl ? this.querySelector(sl) || this.el ? this.el.querySelector(sl) : null : null;
        if (!el) {
          el = document.querySelector(sl);
          el = el ? el : sl ? null : this;
        }
        return el;
      }
    }, {
      key: "attrsToJson",
      value: function attrsToJson(attrs, targetEl, css2Camel) {
        var json = {};
        var _iterator3 = _createForOfIteratorHelper$g(attrs),
          _step3;
        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var attr = _step3.value;
            if (_typeof(attr) === 'object') {
              if (attr.type === 'array' || attr.type === 'object') {
                var key = css2Camel ? this.css2CamelCase(attr.name) : attr.name;
                if (targetEl.hasAttribute(attr.name)) {
                  try {
                    json[key] = JSON.parse(targetEl.getAttribute(attr.name));
                  } catch (_unused2) {
                    this.logger.error('can not parse element attribute value as json');
                  }
                }
              }
            } else {
              if (targetEl.hasAttribute(attr)) {
                var _key = css2Camel ? this.css2CamelCase(attr) : attr;
                json[_key] = targetEl.getAttribute(attr);
              }
            }
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }
        return json;
      }
    }, {
      key: "setupConnections",
      value: function setupConnections() {
        var _this2 = this;
        if (this.connections.length > 0) {
          var _iterator4 = _createForOfIteratorHelper$g(this.connections),
            _step4;
          try {
            var _loop = function _loop() {
              var connection = _step4.value;
              var from = connection.from,
                sourceType = connection.sourceType,
                source = connection.source,
                to = connection.to,
                targetType = connection.targetType,
                target = connection.target,
                value = connection.value;
              var fromEl = _this2.findTarget(from);
              if (sourceType === 'event' || !sourceType) {
                // event is default source type
                source = source ? source : 'click'; // click is default event
                target = target ? target : 'click';
                if (fromEl) {
                  fromEl.addEventListener(source, function (event) {
                    var toEl = this.findTarget(to);
                    this.applyToTarget(source, target, targetType, toEl, value || event.target.value || event);
                  }.bind(_this2));
                } else {
                  _this2.logger.warn("".concat(from, " element specified in connections not found"));
                }
              } else if (sourceType === 'prop') {
                var descriptor = Object.getOwnPropertyDescriptor(fromEl, source);
                if (descriptor) {
                  Object.defineProperty(fromEl, source, {
                    set: function (value) {
                      descriptor.set(value);
                      var toEl = this.findTarget(to);
                      this.applyToTarget(source, target, targetType, toEl, value);
                    }.bind(_this2),
                    get: function () {
                      return this['_' + source];
                    }.bind(_this2),
                    configurable: true
                  });
                } else {
                  Object.defineProperty(fromEl, source, {
                    set: function (value) {
                      this['_' + source] = value;
                      var toEl = this.findTarget(to);
                      this.applyToTarget(source, target, targetType, toEl, value);
                    }.bind(_this2),
                    get: function () {
                      return this['_' + source];
                    }.bind(_this2),
                    configurable: true
                  });
                }
              } else if (sourceType === 'attr') {
                var observer = new MutationObserver(function () {
                  var toEl = this.findTarget(to);
                  this.applyToTarget(source, target, targetType, toEl, value);
                }.bind(_this2));
                observer.observe(fromEl, {
                  attributes: true
                });
              }
            };
            for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
              _loop();
            }
          } catch (err) {
            _iterator4.e(err);
          } finally {
            _iterator4.f();
          }
        }
      }
    }, {
      key: "onReflection",
      value: function onReflection(event) {
        var _this3 = this;
        this.logger.debug('sk-element configChanged', event);
        this.configEl.reconfigureElement(this, event.detail);
        if (this.impl.contentsState) {
          this.contentsState = JSON.parse(JSON.stringify(this.impl.contentsState));
        }
        if (event.detail.name && event.detail.name === 'theme') {
          this.impl.unmountStyles();
          this.impl.unbindEvents();
          this.impl.clearTplCache();
          this.impl = null;
        }
        this.addEventListener(SK_RENDER_EVT, function (event) {
          _newArrowCheck(this, _this3);
          this.impl.restoreState({
            contentsState: this.contentsState
          });
        }.bind(this));
        this.render();
      }
    }, {
      key: "fromEntries",
      value: function fromEntries(entries) {
        if (!entries || !entries[Symbol.iterator]) {
          throw new Error('SkElement.fromEntries() requires a single iterable argument');
        }
        var obj = {};
        var _iterator5 = _createForOfIteratorHelper$g(entries),
          _step5;
        try {
          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var _step5$value = _slicedToArray(_step5.value, 2),
              key = _step5$value[0],
              value = _step5$value[1];
            obj[key] = value;
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
        return obj;
      }
    }, {
      key: "initJsonPath",
      value: function initJsonPath(cn, path) {
        this.jsonPath = new JsonPath();
        var dynImportsOff = this.confValOrDefault(DIMPORTS_AN, null) === "false";
        ResLoader.dynLoad(cn, path, function (def) {
          this.jsonPath.dri = new def();
        }.bind(this), false, false, false, false, false, false, dynImportsOff);
      }
    }, {
      key: "setupJsonPath",
      value: function setupJsonPath() {
        var jsonPathDriCn = this.confValOrDefault(JSONPATH_DRI_CN_AN, false);
        var jsonPathDriPath = this.confValOrDefault(JSONPATH_DRI_PATH_AN, false);
        if (this.hasAttribute(JSONPATH_DRI_CN_AN) && this.hasAttribute(JSONPATH_DRI_PATH_AN)) {
          this.initJsonPath(jsonPathDriCn, jsonPathDriPath);
        } else {
          if (this.configEl) {
            if (this.configEl.jsonPath) {
              this.jsonPath = this.configEl.jsonPath;
            } else {
              if (jsonPathDriCn && jsonPathDriPath) {
                this.initJsonPath(jsonPathDriCn, jsonPathDriPath);
              } else {
                this.jsonPath;
              }
              this.configEl.jsonPath = this.jsonPath;
            }
          } else {
            this.jsonPath;
          }
        }
      }
    }, {
      key: "setupRenderer",
      value: function setupRenderer() {
        // if no renderer were injected, check if we can use shared by config renderer
        if (!this.renderer) {
          if (!this.hasAttribute(RD_NOT_SHARED_AN) || this.configEl && !this.configEl.hasAttribute(RD_NOT_SHARED_AN)) {
            if (this.configEl) {
              if (!this.configEl.renderer) {
                // configEl exists but renderer not yet bootstrapped
                Renderer.configureForElement(this.configEl);
              }
              this.renderer = this.configEl.renderer;
            } else {
              // no configEl, create shared renderer in window
              if (!window.skRenderer) {
                window.skRenderer = new Renderer();
              }
              this.renderer = window.skRenderer;
            }
          }
        }
        // instanciate new render if no one were provided before or element tag has render options defined
        Renderer.configureForElement(this);
      }
    }, {
      key: "skConfigElPolicy",
      value: function skConfigElPolicy() {
        if (document.documentElement && document.documentElement.classList.contains(SK_CONFIG_EL_GLOBAL_CN) || document.body && document.body.classList.contains(SK_CONFIG_EL_GLOBAL_CN)) {
          return SK_CONFIG_EL_GLOBAL;
        } else if (document.documentElement && document.documentElement.classList.contains(SK_CONFIG_EL_INDIVIDUAL_CN) || document.body && document.body.classList.contains(SK_CONFIG_EL_INDIVIDUAL_CN)) {
          return SK_CONFIG_EL_INDIVIDUAL;
        } else {
          return SK_CONFIG_DISABLED;
        }
      }
    }, {
      key: "setupConfigEl",
      value: function setupConfigEl() {
        if (window[CONFIG_OBJ_REF_NAME]) {
          this.config = window[CONFIG_OBJ_REF_NAME];
          var globalConfigElAbsent = false;
          if (!this.configEl) {
            globalConfigElAbsent = true;
            this.configEl = document.createElement('sk-config');
          }
          for (var _i = 0, _Object$keys = Object.keys(this.config); _i < _Object$keys.length; _i++) {
            var propKey = _Object$keys[_i];
            var attrName = this.camelCase2Css(propKey);
            if (!this.configEl.hasAttribute(attrName)) {
              this.configEl.setAttribute(attrName, this.config[propKey]);
            }
          }
          if (globalConfigElAbsent && this.skConfigElPolicy() === SK_CONFIG_EL_GLOBAL) {
            document.body.appendChild(this.configEl);
          }
        }
      }
    }, {
      key: "setup",
      value: function setup() {
        this.setupConfigEl();
        this.setupRenderer();
        this.setupJsonPath();
        var attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false) || this.hasAttribute(ATTR_PLUGINS_AN);
        if (attrPlugins) {
          this.bindAttrPlugins();
        }
        if (this.configEl && this.configEl.configureElement) {
          this.configEl.configureElement(this);
          if (this.reflective && this.reflective !== 'false') {
            this.configEl.addEventListener('configChanged', this.onReflection.bind(this));
          }
        } else {
          if (this.configEl) {
            // sk-config was not registred, use fallback configuration
            for (var _i2 = 0, _Object$keys2 = Object.keys(EL_SHARED_ATTRS); _i2 < _Object$keys2.length; _i2++) {
              var attrName = _Object$keys2[_i2];
              var value = this.getAttribute(attrName);
              if (value === null && this.configEl.getAttribute(attrName) !== null) {
                this.setAttribute(attrName, this.configEl.getAttribute(attrName));
              }
            }
          }
          if (!this.hasAttribute(BASE_PATH_AN)) {
            this.logger.warn(this.constructor.name, " ", "configEl not found or sk-config element not registred, " + "you will have to add base-path and theme(if not default) attributes for each sk element");
          }
        }
        if (this.useShadowRoot) {
          if (!this.shadowRoot) {
            this.attachShadow({
              mode: 'open'
            });
          }
        }
      }
    }, {
      key: "withUpdatesLocked",
      value: function withUpdatesLocked(callback) {
        if (typeof callback === 'function') {
          this.updatesLocked = true;
          callback.call(this);
          this.updatesLocked = false;
        }
      }
    }, {
      key: "storedProps",
      get: function get() {
        if (!this._storedProps) {
          if (this.hasAttribute(STORED_PROPS_AN)) {
            try {
              this._storedProps = JSON.parse(this.getAttribute(STORED_PROPS_AN));
            } catch (e) {
              this.logger.warn('Error parsing stored attrs attribute for element', this);
              this._storedProps = [];
            }
          } else {
            this._storedProps = [];
          }
        }
        return this._storedProps;
      },
      set: function set(props) {
        this._storedProps = props;
      }
    }, {
      key: "loadStoredCfg",
      value: function loadStoredCfg() {
        var _this4 = this;
        if (this.storedProps.length > 0) {
          var _iterator6 = _createForOfIteratorHelper$g(this.storedProps),
            _step6;
          try {
            var _loop2 = function _loop2() {
              var _this5 = this;
              var prop = _step6.value;
              if (_this4.storedCfg[prop]) {
                _this4.withUpdatesLocked(function () {
                  _newArrowCheck(this, _this5);
                  _this4[prop] = _this4.storedCfg[prop];
                }.bind(this));
              }
            };
            for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
              _loop2();
            }
          } catch (err) {
            _iterator6.e(err);
          } finally {
            _iterator6.f();
          }
        }
      }
    }, {
      key: "render",
      value: function () {
        var _render = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                this.callPluginHook('onRenderStart');
                if (this.impl) {
                  this.addToRendering();
                  this.impl.renderImpl();
                  //this.impl.bindEvents();
                  /*        this.bindAutoRender();
                          this.callPluginHook('onRenderEnd');
                          this.dispatchEvent(new CustomEvent('skrender', { bubbles: true, composed: true }));*/
                } else {
                  this.bindAutoRender();
                  this.setupConnections();
                  if (this.skConfigElPolicy() === SK_CONFIG_EL_INDIVIDUAL) {
                    this.el.appendChild(this.configEl);
                  }
                  this.callPluginHook('onRenderEnd');
                  this.renderTimest = Date.now();
                  this.dispatchEvent(new CustomEvent('rendered', {
                    bubbles: true,
                    composed: true
                  })); // :DEPRECATED
                  this.dispatchEvent(new SkRenderEvent({
                    bubbles: true,
                    composed: true
                  }));
                }
              case 2:
              case "end":
                return _context.stop();
            }
          }, _callee, this);
        }));
        function render() {
          return _render.apply(this, arguments);
        }
        return render;
      }()
    }, {
      key: "css2CamelCase",
      value: function css2CamelCase(str) {
        return StringUtils.css2CamelCase(str);
      }
    }, {
      key: "camelCase2Css",
      value: function camelCase2Css(str) {
        return StringUtils.camelCase2Css(str);
      }
    }, {
      key: "capitalizeFirstLetter",
      value: function capitalizeFirstLetter(str) {
        return StringUtils.capitalizeFirstLetter(str);
      }
    }, {
      key: "bindByCn",
      value: function bindByCn(attrName, el, type) {
        if (attrName != null) {
          var propName = this.css2CamelCase(attrName);
          var descriptor = Object.getOwnPropertyDescriptor(this, propName);
          if (descriptor) {
            Object.defineProperty(this, propName, {
              set: function (value) {
                descriptor.set(value);
                if (type === 'at') {
                  if (propName.indexOf('_') > 0) {
                    var _propName$split = propName.split('_'),
                      _propName$split2 = _slicedToArray(_propName$split, 2);
                      _propName$split2[0];
                      var target = _propName$split2[1];
                    el.setAttribute(target, value);
                  } else {
                    el.setAttribute(propName, value);
                  }
                } else if (type === 'cb') {
                  var callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';
                  if (typeof this[callbackName] === 'function') {
                    this[callbackName].call(this, el, value);
                  }
                } else {
                  el.innerHTML = value;
                }
                this['_' + propName] = value;
              }.bind(this),
              get: function () {
                return this['_' + propName];
              }.bind(this),
              configurable: true
            });
          } else {
            Object.defineProperty(this, propName, {
              set: function (value) {
                if (type === 'at') {
                  if (propName.indexOf('_') > 0) {
                    var _propName$split3 = propName.split('_'),
                      _propName$split4 = _slicedToArray(_propName$split3, 2);
                      _propName$split4[0];
                      var target = _propName$split4[1];
                    el.setAttribute(target, value);
                  } else {
                    el.setAttribute(propName, value);
                  }
                } else if (type === 'cb') {
                  var callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';
                  if (typeof this[callbackName] === 'function') {
                    this[callbackName].call(this, el, value);
                  }
                } else {
                  el.innerHTML = value;
                }
                this['_' + propName] = value;
              }.bind(this),
              get: function () {
                return this['_' + propName];
              }.bind(this),
              configurable: true
            });
          }
        }
      }
    }, {
      key: "bindAutoRender",
      value: function bindAutoRender(target) {
        target = target ? target : this.el;
        var autoRenderedEls = target.querySelectorAll('[class*="sk-prop-"]');
        var _iterator7 = _createForOfIteratorHelper$g(autoRenderedEls),
          _step7;
        try {
          for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
            var autoEl = _step7.value;
            // bind in and at classed elements contents and attrs to element's attributes or class props
            // if class property with name matched found we bind it
            // if class property with name matched not found we define and bind it
            var inNames = autoEl.className.match('sk-prop-in-(\\S+)');
            if (inNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(inNames[1], autoEl);
            }
            var atNames = autoEl.className.match('sk-prop-at-(\\S+)');
            if (atNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(atNames[1], autoEl, 'at');
            }
            var cbNames = autoEl.className.match('sk-prop-cb-(\\S+)');
            if (cbNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(cbNames[1], autoEl, 'cb');
            }
          }
        } catch (err) {
          _iterator7.e(err);
        } finally {
          _iterator7.f();
        }
      }
    }, {
      key: "initRendering",
      value: function initRendering() {
        if (!window.skRendering) {
          window.skRendering = new Set();
        }
      }
    }, {
      key: "addToRendering",
      value: function addToRendering() {
        this.initRendering();
        window.skRendering.add(this);
      }
    }, {
      key: "removeFromRendering",
      value: function removeFromRendering() {
        if (window.skRendering) {
          window.skRendering["delete"](this);
          if (window.skRendering.size <= 0) {
            window.dispatchEvent(new CustomEvent(SK_RENDERED_EVT));
            window.skRendered = true;
          }
        } else {
          this.logger.warn('No rendering set found');
        }
      }

      /**
       * preload templates by specified by preLoadedTpls array property with names and matching ${name}Path properties
       * tplName must be like itemTpl, itemActiveTpl, buttonTpl, it will be injected in this class with provided name
       * tplName + 'Path' attribute must be defined in class, e.g. itemTplPath, itemActiveTplPath
       * @returns {Promise<unknown>}
       */
    }, {
      key: "preloadTemplates",
      value: (function () {
        var _preloadTemplates = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2() {
          var _this6 = this;
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", new Promise(function (resolve) {
                  var _this7 = this;
                  _newArrowCheck(this, _this6);
                  var loadedTpls = [];
                  var tplMap = new Map();
                  // tplName must be like itemTpl, itemActiveTpl, buttonTpl,
                  var _iterator8 = _createForOfIteratorHelper$g(this.preLoadedTpls),
                    _step8;
                  try {
                    for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
                      var tplName = _step8.value;
                      var id = this.constructor.name + this.capitalizeFirstLetter(tplName);
                      var tplLoad = this.renderer.mountTemplate(this[tplName + 'Path'], id, this);
                      loadedTpls.push(tplLoad);
                      tplMap.set(id, tplName);
                    }
                  } catch (err) {
                    _iterator8.e(err);
                  } finally {
                    _iterator8.f();
                  }
                  Promise.all(loadedTpls).then(function (loadedTpls) {
                    _newArrowCheck(this, _this7);
                    // inject templates by attr name w/o path
                    var _iterator9 = _createForOfIteratorHelper$g(loadedTpls),
                      _step9;
                    try {
                      for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
                        var loadedTpl = _step9.value;
                        var propName = tplMap.get(loadedTpl.getAttribute('id'));
                        this[propName] = loadedTpl;
                      }
                    } catch (err) {
                      _iterator9.e(err);
                    } finally {
                      _iterator9.f();
                    }
                    resolve(loadedTpls);
                  }.bind(this));
                }.bind(this)));
              case 1:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this);
        }));
        function preloadTemplates() {
          return _preloadTemplates.apply(this, arguments);
        }
        return preloadTemplates;
      }())
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        var _this8 = this;
        this.initRendering();
        this.loadStoredCfg();
        this.setup();
        if (this.preLoadedTpls && Array.isArray(this.preLoadedTpls) && this.preLoadedTpls.length > 0) {
          this.preloadTemplates().then(function () {
            _newArrowCheck(this, _this8);
            this.render();
          }.bind(this));
        } else {
          this.render();
        }
      }
    }, {
      key: "setCustomValidity",
      value: function setCustomValidity(validity) {
        this.validationMessage = validity;
        this.impl.renderValidation(validity);
      }
    }, {
      key: "flushValidity",
      value: function flushValidity() {
        this.impl.flushValidation();
      }
    }, {
      key: "plugins",
      get: function get() {
        if (!this._plugins) {
          this._plugins = [];
        }
        if (this.constructor.plugins && this.constructor.plugins.length > 0) {
          return this._plugins.concat(this.constructor.plugins);
        } else {
          return this._plugins;
        }
      },
      set: function set(plugins) {
        this._plugins = plugins;
      }
    }, {
      key: "addPlugin",
      value: function addPlugin(plugin) {
        var global = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        if (global) {
          if (!this.constructor.plugins) {
            this.constructor.plugins = [];
          }
          this.constructor.plugins.push(plugin);
        } else {
          this.plugins.push(plugin);
        }
      }
    }, {
      key: "walkInPlugins",
      value: function walkInPlugins(name, plugins) {
        for (var _len = arguments.length, rest = new Array(_len > 2 ? _len - 2 : 0), _key2 = 2; _key2 < _len; _key2++) {
          rest[_key2 - 2] = arguments[_key2];
        }
        var _iterator10 = _createForOfIteratorHelper$g(plugins),
          _step10;
        try {
          for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
            var plugin = _step10.value;
            if (typeof plugin[name] === 'function') {
              var _plugin$name;
              (_plugin$name = plugin[name]).call.apply(_plugin$name, [this].concat(rest));
            } else if (typeof plugin[name] === 'string') {
              var functionName = plugin[name];
              if (typeof this[functionName] === 'function') {
                var _this$functionName;
                (_this$functionName = this[functionName]).call.apply(_this$functionName, [this].concat(rest));
              } else if (this.impl && typeof this.impl[functionName] === 'function') {
                var _this$impl$functionNa;
                (_this$impl$functionNa = this.impl[functionName]).call.apply(_this$impl$functionNa, [this].concat(rest));
              } else if (typeof window[functionName] === 'function') {
                var _window$functionName;
                (_window$functionName = window[functionName]).call.apply(_window$functionName, [this].concat(rest));
              }
            }
          }
        } catch (err) {
          _iterator10.e(err);
        } finally {
          _iterator10.f();
        }
      }
    }, {
      key: "bindAttrPlugins",
      value: function bindAttrPlugins() {
        _bindAttrPlugins.call.apply(_bindAttrPlugins, [this].concat(Array.prototype.slice.call(arguments)));
      }
    }, {
      key: "callPluginHook",
      value: function callPluginHook(name) {
        for (var _len2 = arguments.length, rest = new Array(_len2 > 1 ? _len2 - 1 : 0), _key3 = 1; _key3 < _len2; _key3++) {
          rest[_key3 - 1] = arguments[_key3];
        }
        if (this.configEl && Array.isArray(this.configEl.plugins) && this.configEl.plugins.length > 0) {
          this.walkInPlugins.apply(this, [name, this.configEl.plugins].concat(rest));
        }
        if (Array.isArray(this.plugins) && this.plugins.length > 0) {
          this.walkInPlugins.apply(this, [name, this.plugins].concat(rest));
        }
      }
    }, {
      key: "clearElCache",
      value: function clearElCache() {
        var _iterator11 = _createForOfIteratorHelper$g(this.subEls),
          _step11;
        try {
          for (_iterator11.s(); !(_step11 = _iterator11.n()).done;) {
            var el = _step11.value;
            if (this[el]) {
              this[el] = null;
            }
          }
        } catch (err) {
          _iterator11.e(err);
        } finally {
          _iterator11.f();
        }
      }
    }, {
      key: "whenRendered",
      value: function whenRendered(callback) {
        return this.renderer ? this.renderer.whenElRendered(this, callback) : Renderer.callWhenRendered(this, callback);
      }
    }, {
      key: "renderDeferred",
      get: function get() {
        if (!this._renderDeferred) {
          if (this.renderer.jquery) {
            this._renderDeferred = new this.renderer.jquery.Deferred();
          } else {
            this._renderDeferred = null;
          }
        }
        return this._renderDeferred;
      }
    }, {
      key: "onRendered",
      value: function onRendered(callback) {
        var _this9 = this;
        if (this.renderDeferred) {
          this.renderDeferred.then(function (result) {
            _newArrowCheck(this, _this9);
            callback.call(this, result);
          }.bind(this));
        } else {
          if (!this.renderer.jquery) {
            this.logger.warn('onRendered() requires jquery to be loaded for renderer');
          }
        }
      }
    }, {
      key: "runSerial",
      value: function () {
        var _runSerial = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(taskFunctions) {
          var _this10 = this;
          var starterPromise;
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                starterPromise = Promise.resolve(null);
                _context3.next = 3;
                return taskFunctions.reduce(function (p, def) {
                  var _this11 = this;
                  _newArrowCheck(this, _this10);
                  if (typeof def === 'function') {
                    return p.then(function () {
                      _newArrowCheck(this, _this11);
                      return def.call(this);
                    }.bind(this));
                  } else {
                    var args = def.args ? def.args : [];
                    return p.then(function () {
                      var _def$func;
                      _newArrowCheck(this, _this11);
                      return (_def$func = def.func).call.apply(_def$func, [this].concat(_toConsumableArray(args)));
                    }.bind(this));
                  }
                }.bind(this), starterPromise);
              case 3:
              case "end":
                return _context3.stop();
            }
          }, _callee3, this);
        }));
        function runSerial(_x) {
          return _runSerial.apply(this, arguments);
        }
        return runSerial;
      }()
    }, {
      key: "disconnectedCallback",
      value: function disconnectedCallback() {
        if (this.impl && typeof this.impl.onDisconnected === 'function') {
          this.impl.onDisconnected();
        }
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
        if (!this.confValOrDefault(NO_ATTR_EVTS_AN, false)) {
          this.dispatchEvent(new SkAttrChangeEvent({
            detail: {
              name: name,
              oldValue: oldValue,
              newValue: newValue
            }
          }));
        }
      }
    }, {
      key: "focus",
      value: function focus() {
        if (this !== null && this !== void 0 && this.impl.focus && typeof this.impl.focus === 'function') {
          this.impl.focus();
        }
      }

      /**
       * translate string with placeholders in {0} and {{ var }} formats,
       * first one is chosen when more than one extra argument passed and
       * it is not an object. Initial but rendered string returned if translate
       * not found.
       * e.g. this.tr('Hello, {0}, your email is {1}', 'John', 'mail@example.com');
       * e.g. this.tr('Hello, {{ name }}, your email is {{ email }}', { username: 'John', email: 'mail@example.com' });
       * @param target
       * @param placeholders
       * @returns string
       */
    }, {
      key: "tr",
      value: function tr(target) {
        var str = this.i18n[target] ? this.i18n[target] : target;
        return arguments.length > 1 ? this.format.apply(this, [str].concat(_toConsumableArray(Array.prototype.slice.call(arguments, 1)))) : str;
      }
    }, {
      key: "format",
      value: function format(str) {
        var values = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : [];
        if (values.length === 1 && _typeof(values[0]) === 'object') {
          return this.renderer.replaceVars(str, values[0]);
        } else {
          return str.replace(/{(\d+)}/g, function (match, number) {
            return typeof values[number] != 'undefined' ? values[number] : match;
          });
        }
      }
    }, {
      key: "loadDataLink",
      value: function () {
        var _loadDataLink = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee4(dataLink, target) {
          var value;
          return regenerator.wrap(function _callee4$(_context4) {
            while (1) switch (_context4.prev = _context4.next) {
              case 0:
                value = null;
                target = target ? target : this;
                if (!(typeof dataLink === 'string')) {
                  _context4.next = 36;
                  break;
                }
                if (!this[dataLink]) {
                  _context4.next = 13;
                  break;
                }
                if (!(typeof target[dataLink] === 'function')) {
                  _context4.next = 10;
                  break;
                }
                _context4.next = 7;
                return target[dataLink].call(this);
              case 7:
                value = _context4.sent;
                _context4.next = 11;
                break;
              case 10:
                value = target[dataLink];
              case 11:
                _context4.next = 36;
                break;
              case 13:
                if (!window[dataLink]) {
                  _context4.next = 23;
                  break;
                }
                if (!(typeof window[dataLink] === 'function')) {
                  _context4.next = 20;
                  break;
                }
                _context4.next = 17;
                return window[dataLink].call(window);
              case 17:
                value = _context4.sent;
                _context4.next = 21;
                break;
              case 20:
                value = window[dataLink];
              case 21:
                _context4.next = 36;
                break;
              case 23:
                value = this.jsonPath.query(this, dataLink)[0];
                if (!value) {
                  _context4.next = 31;
                  break;
                }
                if (!(typeof value === 'function')) {
                  _context4.next = 29;
                  break;
                }
                _context4.next = 28;
                return value.call(this);
              case 28:
                value = _context4.sent;
              case 29:
                _context4.next = 36;
                break;
              case 31:
                value = this.jsonPath.query(window, dataLink)[0];
                if (!(typeof value === 'function')) {
                  _context4.next = 36;
                  break;
                }
                _context4.next = 35;
                return value.call(this);
              case 35:
                value = _context4.sent;
              case 36:
                return _context4.abrupt("return", Promise.resolve(value));
              case 37:
              case "end":
                return _context4.stop();
            }
          }, _callee4, this);
        }));
        function loadDataLink(_x2, _x3) {
          return _loadDataLink.apply(this, arguments);
        }
        return loadDataLink;
      }()
    }]);
  }(/*#__PURE__*/_wrapNativeSuper(HTMLElement));

  function _callSuper$11(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$11() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$11() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$11 = function _isNativeReflectConstruct() { return !!t; })(); }
  var EL_SHARED_ATTRS = {
    'theme': 'theme',
    'base-path': 'basePath',
    'use-shadow-root': 'useShadowRoot',
    'lang': 'lang',
    'reflective': 'reflective',
    'gen-ids': 'genIds',
    'st-store': 'stateStoreBackend',
    'styles': 'themeStyles',
    'log-lv': 'logLevel',
    'tpl-vars': 'tplVars'
  };
  var SK_CONFIG_TN = "sk-config";
  var DIMPORTS_AN = "dimports";
  var SkConfig = /*#__PURE__*/function (_HTMLElement) {
    function SkConfig() {
      _classCallCheck(this, SkConfig);
      return _callSuper$11(this, SkConfig, arguments);
    }
    _inherits(SkConfig, _HTMLElement);
    return _createClass(SkConfig, [{
      key: "renderer",
      get: function get() {
        return this._renderer || null;
      },
      set: function set(renderer) {
        this._renderer = renderer;
      }
    }, {
      key: "styles",
      get: function get() {
        if (!this._styles) {
          this._styles = this.getAttribute('styles') ? JSON.parse(this.getAttribute('styles')) : [];
        }
        return this._styles;
      }
    }, {
      key: "plugins",
      get: function get() {
        if (!this._plugins) {
          this._plugins = [];
        }
        if (this.constructor.plugins && this.constructor.plugins.length > 0) {
          return this._plugins.concat(this.constructor.plugins);
        } else {
          return this._plugins;
        }
      },
      set: function set(plugins) {
        this._plugins = plugins;
      }
    }, {
      key: "logger",
      get: function get() {
        if (!this._logger) {
          this._logger = ConsoleLogger.instance();
        }
      },
      set: function set(logger) {
        this._logger = logger;
      }
    }, {
      key: "configureElement",
      value: function configureElement(el) {
        for (var _i = 0, _Object$keys = Object.keys(EL_SHARED_ATTRS); _i < _Object$keys.length; _i++) {
          var attrName = _Object$keys[_i];
          var value = el.getAttribute(attrName);
          if (value === null && this.getAttribute(attrName) !== null) {
            el.setAttribute(attrName, this.getAttribute(attrName));
          }
        }
      }
    }, {
      key: "reconfigureElement",
      value: function reconfigureElement(el, detail) {
        if (detail) {
          this.log("el attribute ".concat(detail.name, " changed from: ").concat(detail.oldValue, " to: ").concat(detail.newValue));
          el.setAttribute(detail.name, detail.newValue);
        } else {
          for (var _i2 = 0, _Object$keys2 = Object.keys(EL_SHARED_ATTRS); _i2 < _Object$keys2.length; _i2++) {
            var attrName = _Object$keys2[_i2];
            var value = el.getAttribute(attrName);
            if (this.getAttribute(attrName) !== null && value !== this.getAttribute(attrName)) {
              el.setAttribute(attrName, this.getAttribute(attrName));
            }
          }
        }
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (EL_SHARED_ATTRS[name] && oldValue !== newValue) {
          if (newValue) {
            this.dispatchEvent(new CustomEvent('configChanged', {
              detail: {
                name: name,
                oldValue: oldValue,
                newValue: newValue
              }
            }));
            this.dispatchEvent(new CustomEvent('skconfigchanged', {
              detail: {
                name: name,
                oldValue: oldValue,
                newValue: newValue
              }
            }));
          }
        }
      }
    }, {
      key: "bindAttrPlugins",
      value: function bindAttrPlugins() {
        _bindAttrPlugins.call.apply(_bindAttrPlugins, [this].concat(Array.prototype.slice.call(arguments)));
      }
    }, {
      key: "setup",
      value: function setup() {
        Renderer.configureForElement(this);
        if (this.hasAttribute(ATTR_PLUGINS_AN)) {
          this.bindAttrPlugins();
        }
      }
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        this.setup();
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return Object.keys(EL_SHARED_ATTRS);
      }
    }]);
  }(/*#__PURE__*/_wrapNativeSuper(HTMLElement));

  function _superPropBase(t, o) {
    for (; !{}.hasOwnProperty.call(t, o) && null !== (t = _getPrototypeOf(t)););
    return t;
  }

  function _get() {
    return _get = "undefined" != typeof Reflect && Reflect.get ? Reflect.get.bind() : function (e, t, r) {
      var p = _superPropBase(e, t);
      if (p) {
        var n = Object.getOwnPropertyDescriptor(p, t);
        return n.get ? n.get.call(arguments.length < 3 ? e : r) : n.value;
      }
    }, _get.apply(null, arguments);
  }

  var Registry = /*#__PURE__*/function () {
    function Registry() {
      _classCallCheck(this, Registry);
      this.registry = {};
    }
    return _createClass(Registry, [{
      key: "getDep",
      value: function getDep(depName) {
        return this.registry[depName];
      }

      /**
       * If you use shared registry beaware deps with same name are not overriding by default, use ov param for that
       * example:
       *
       * window.fmRegistry = window.fmRegistry || new Registry();
       fmRegistry.wire({
          Renderer,
          catalogTr: { val: { foo: 'bar' }},
          FmProductGrid: { def: FmProductGrid, deps: {
                  'renderer': Renderer
              }, is: 'fm-product-grid'}
          });
        * @param definitionName
       * @param map
       * @returns {*}
       */
    }, {
      key: "initDef",
      value: function initDef(definitionName, map) {
        if (!this.registry[definitionName] || map[definitionName].ov) {
          if (map[definitionName] && map[definitionName].defName) {
            // definition
            var def = Function('return ' + map[definitionName].defName)();
            if (map[definitionName].is) {
              this.registry[definitionName] = def;
            } else {
              this.registry[definitionName] = new def();
            }
          } else if (map[definitionName] && map[definitionName].def) {
            // definition
            if (map[definitionName].is) {
              this.registry[definitionName] = map[definitionName].def;
            } else {
              this.registry[definitionName] = new map[definitionName].def();
            }
          } else if (map[definitionName] && map[definitionName].val !== undefined && map[definitionName].val !== null) {
            this.registry[definitionName] = map[definitionName].val;
          } else if (map[definitionName] && map[definitionName].f) {
            // explicit factory
            if (typeof new map[definitionName].f() === 'function') {
              this.registry[definitionName] = map[definitionName].f.call(map[definitionName].f);
            } else {
              console.error('factory must be a function');
            }
          } else {
            // function or constructor
            if (typeof map[definitionName] === 'function') {
              // guess constructor
              try {
                this.registry[definitionName] = new map[definitionName]();
              } catch (_unused) {
                // if not constructor then factory
                console.warn('constructor not provided for ' + definitionName + ', guess this is a factory');
                this.registry[definitionName] = map[definitionName].call(map[definitionName]);
              }
            } else {
              // just assign the link
              this.registry[definitionName] = map[definitionName];
            }
          }
        }
        return this.registry[definitionName];
      }
    }, {
      key: "wireDef",
      value: function wireDef(definitionName, definition, map) {
        this.initDef(definitionName, map);
        if (definition.deps) {
          var depDefs = Object.keys(definition.deps);
          for (var _i = 0, _depDefs = depDefs; _i < _depDefs.length; _i++) {
            var propName = _depDefs[_i];
            var propDef = definition.deps[propName];
            var defName = typeof propDef === 'string' ? propDef : propDef.name;
            var dependency = this.registry[defName];
            if (!dependency) {
              dependency = this.registry[propDef.name] = this.wireDef(defName, map);
              if (!dependency && propDef) {
                // dependency not found in registry, probably inline value
                dependency = propDef;
              }
            }
            if (definition.is) {
              Object.defineProperty(this.registry[definitionName].prototype, propName, {
                value: dependency,
                writable: true
              });
            } else {
              Object.defineProperty(this.registry[definitionName], propName, {
                value: dependency,
                writable: true
              });
            }
          }
        }
        if (definition.bootstrap) {
          definition.bootstrap(this.registry[definitionName]);
        }
        if (definition.is) {
          if (!window.customElements.get(definition.is)) {
            window.customElements.define(definition.is, this.registry[definitionName]);
          }
        }
      }
    }, {
      key: "checkAndWire",
      value: function checkAndWire(definitionName, definition, map) {
        if (!map[definitionName]) {
          try {
            var def = Function('return ' + definitionName)();
            if (def) {
              this.wireDef(definitionName, definition, map);
            } else {
              console.warn(definitionName + ' definition not loaded for wiring');
            }
          } catch (_unused2) {
            console.warn("dependency ".concat(definitionName, " not found"));
          }
        } else {
          this.wireDef(definitionName, definition, map);
        }
      }
    }, {
      key: "wire",
      value: function wire(map, dynImportOff) {
        var _this2 = this;
        var _loop = function _loop() {
          var definitionName = _Object$keys[_i2];
          var definition = map[definitionName];
          if (definition.path) {
            if (!window[definitionName] || definition.forceLoad) {
              ResLoader.loadClass(definitionName, definition.path, function (m) {
                window[definitionName] = m[definitionName];
                this.checkAndWire(definitionName, definition, map);
              }.bind(_this2), false, dynImportOff);
            } else {
              // definition already loaded
              _this2.checkAndWire(definitionName, definition, map);
            }
          } else {
            _this2.checkAndWire(definitionName, definition, map);
          }
        };
        for (var _i2 = 0, _Object$keys = Object.keys(map); _i2 < _Object$keys.length; _i2++) {
          _loop();
        }
      }
    }, {
      key: "dynamicImportSupported",
      value: function dynamicImportSupported() {
        try {
          new Function('import("")');
          return true;
        } catch (err) {
          return false;
        }
      }
    }]);
  }();

  function _callSuper$10(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$10() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$10() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$10 = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$z(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var CompRegistry = /*#__PURE__*/function (_Registry) {
    function CompRegistry() {
      _classCallCheck(this, CompRegistry);
      return _callSuper$10(this, CompRegistry, arguments);
    }
    _inherits(CompRegistry, _Registry);
    return _createClass(CompRegistry, [{
      key: "initDef",
      value:
      /**
       * Overriden method to wire one definition, adds connections and plugins property support
       * @param definitionName
       * @param map
       * @returns {*}
       */
      function initDef(definitionName, map) {
        this.registry[definitionName] = _superPropGet$z(CompRegistry, "initDef", this, 3)([definitionName, map]);
        if (map[definitionName] && map[definitionName].connections) {
          this.registry[definitionName].prototype.connections = map[definitionName].connections;
        }
        if (map[definitionName] && map[definitionName].plugins) {
          this.registry[definitionName].prototype.plugins = map[definitionName].plugins;
        }
        return this.registry[definitionName];
      }
    }]);
  }(Registry);

  function _createForOfIteratorHelper$f(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$f(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$f(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$f(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$f(r, a) : void 0; } }
  function _arrayLikeToArray$f(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$$(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$$() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$$() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$$ = function _isNativeReflectConstruct() { return !!t; })(); }
  var REG_ITEM_TAG_NAME = 'sk-registry-item';
  var CONN_ITEM_TAG_NAME = 'sk-registry-connection';
  var CONN_ITEM_ATTRS = ['from', 'source-type', 'source', 'to', 'target-type', 'target', 'value'];
  var SkRegistry = /*#__PURE__*/function (_SkElement) {
    function SkRegistry() {
      _classCallCheck(this, SkRegistry);
      return _callSuper$$(this, SkRegistry, arguments);
    }
    _inherits(SkRegistry, _SkElement);
    return _createClass(SkRegistry, [{
      key: "itemTn",
      get: function get() {
        return REG_ITEM_TAG_NAME;
      }
    }, {
      key: "connectionTn",
      get: function get() {
        return CONN_ITEM_TAG_NAME;
      }
    }, {
      key: "buildDeps",
      value: function buildDeps() {
        this.deps = new Set();
        var items = this.querySelectorAll(this.itemTn);
        var _iterator = _createForOfIteratorHelper$f(items),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var item = _step.value;
            var dep = this.attrsToJson(['key', 'is', 'defName', {
              name: 'val',
              type: 'object'
            }, {
              name: 'plugins',
              type: 'array'
            }, {
              name: 'deps',
              type: 'object'
            }], item, true);
            if (item.hasAttribute('plugins')) {
              dep.plugins = JSON.parse(item.getAttribute('plugins'));
            }
            if (item.hasAttribute('full-path')) {
              dep.path = item.getAttribute('full-path');
            } else {
              if (item.hasAttribute('path') && this.basePath) {
                if (this.basePath) {
                  dep.path = this.basePath + '/' + item.getAttribute('path');
                } else {
                  dep.path = item.getAttribute('path');
                }
              }
            }
            if (item.hasAttribute('force-load')) {
              dep.forceLoad = item.getAttribute('force-load');
            }
            //dep.def = (Function('return ' + item.getAttribute("def")))();
            var depConnections = [];
            var connections = item.querySelectorAll(this.connectionTn);
            var _iterator2 = _createForOfIteratorHelper$f(connections),
              _step2;
            try {
              for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                var connection = _step2.value;
                var depConnection = this.attrsToJson(CONN_ITEM_ATTRS, connection, true);
                depConnections.push(depConnection);
              }
            } catch (err) {
              _iterator2.e(err);
            } finally {
              _iterator2.f();
            }
            if (depConnections.length > 0) {
              dep.connections = depConnections;
            }
            this.deps[dep.key] = dep;
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }, {
      key: "setup",
      value: function setup() {
        //this.setupRenderer();
        if (this.configEl && this.configEl.configureElement) {
          this.configEl.configureElement(this);
          if (this.reflective && this.reflective !== 'false') {
            this.configEl.addEventListener('configChanged', this.onReflection.bind(this));
          }
        } else {
          if (this.configEl && this.configEl.hasAttribute('base-path')) {
            this.setAttribute('base-path', this.configEl.getAttribute('base-path'));
          }
          if (!this.hasAttribute('base-path')) {
            this.logger.warn(this.constructor.name, " ", "configEl not found or sk-config element not registred, " + "you will have to add base-path and theme(if not default) attributes for each sk element");
          }
          if (this.configEl && this.configEl.hasAttribute('dimport')) {
            this.setAttribute('dimport', this.configEl.getAttribute('dimport'));
          }
        }
        /*        if (this.useShadowRoot) {
                    if (! this.shadowRoot) {
                        this.attachShadow({mode: 'open'});
                    }
                }*/
      }
    }, {
      key: "loadDepsAndCons",
      value: function loadDepsAndCons() {
        this.buildDeps();
        this.registry = window.registry || new CompRegistry();
        var dynImportOff = this.hasAttribute('dimport') && this.getAttribute('dimport') === 'false';
        this.registry.wire(this.deps, dynImportOff);
        this.setupConnections();
      }
    }, {
      key: "connections",
      get: function get() {
        var _this = this;
        if (!this._connections) {
          var connections = Array.from(this.querySelectorAll(this.connectionTn)).filter(function (ele) {
            _newArrowCheck(this, _this);
            return ele.parentElement === this;
          }.bind(this));
          var connectionsJson = [];
          var _iterator3 = _createForOfIteratorHelper$f(connections),
            _step3;
          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var connection = _step3.value;
              connectionsJson.push(this.attrsToJson(CONN_ITEM_ATTRS, connection, true));
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
          this._connections = connectionsJson;
        }
        return this._connections;
      }
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        this.setup();
        if (this.hasAttribute('after-content')) {
          window.addEventListener('DOMContentLoaded', this.loadDepsAndCons.bind(this));
        } else {
          this.loadDepsAndCons();
        }
      }
    }]);
  }(SkElement);

  function _callSuper$_(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$_() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$_() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$_ = function _isNativeReflectConstruct() { return !!t; })(); }
  var JqueryTheme = /*#__PURE__*/function (_SkTheme) {
    function JqueryTheme() {
      _classCallCheck(this, JqueryTheme);
      return _callSuper$_(this, JqueryTheme, arguments);
    }
    _inherits(JqueryTheme, _SkTheme);
    return _createClass(JqueryTheme, [{
      key: "basePath",
      get: function get() {
        if (!this._basePath) {
          this._basePath = this.configEl && typeof this.configEl.hasAttribute === 'function' && this.configEl.hasAttribute('theme-path') ? "".concat(this.configEl.getAttribute('theme-path')) : '/node_modules/sk-theme-jquery';
        }
        return this._basePath;
      }
    }, {
      key: "styles",
      get: function get() {
        if (!this._styles) {
          this._styles = {
            'jquery-ui.css': "".concat(this.basePath, "/jquery-ui.css"),
            'theme.css': "".concat(this.basePath, "/theme.css"),
            'jquery-theme.css': "".concat(this.basePath, "/jquery-theme.css")
          };
        }
        return this._styles;
      }
    }]);
  }(SkTheme);

  var SkFormValidator = /*#__PURE__*/function () {
    function SkFormValidator() {
      _classCallCheck(this, SkFormValidator);
    }
    return _createClass(SkFormValidator, [{
      key: "validate",
      value: function validate() {
        return true;
      }
    }]);
  }();

  function _callSuper$Z(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$Z() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$Z() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$Z = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkRequired = /*#__PURE__*/function (_SkFormValidator) {
    function SkRequired() {
      _classCallCheck(this, SkRequired);
      return _callSuper$Z(this, SkRequired, arguments);
    }
    _inherits(SkRequired, _SkFormValidator);
    return _createClass(SkRequired, [{
      key: "msg",
      get: function get() {
        return 'The value must match requirements';
      }
    }, {
      key: "attrName",
      get: function get() {
        return 'sk-required';
      }
    }, {
      key: "validate",
      value: function validate(field) {
        if (field.tagName === 'SK-CHECKBOX' || field.tagName === 'SK-SWITCH' || field.type === 'CHECKBOX') {
          if (!field.hasAttribute('checked')) {
            return this.msg;
          }
        } else {
          var value = field.getAttribute('value');
          if (value === null || value === undefined || value === '') {
            return this.msg;
          }
        }
      }
    }]);
  }(SkFormValidator);

  function _callSuper$Y(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$Y() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$Y() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$Y = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkMax = /*#__PURE__*/function (_SkFormValidator) {
    function SkMax() {
      _classCallCheck(this, SkMax);
      return _callSuper$Y(this, SkMax, arguments);
    }
    _inherits(SkMax, _SkFormValidator);
    return _createClass(SkMax, [{
      key: "msg",
      get: function get() {
        return 'The value is less than required';
      }
    }, {
      key: "attrName",
      get: function get() {
        return 'sk-max';
      }
    }, {
      key: "validate",
      value: function validate(field) {
        var value = field.getAttribute('value');
        var fieldType = field.getAttribute('type');
        var min = field.getAttribute('sk-min');
        if (fieldType === 'number') {
          if (parseFloat(value) < parseFloat(min)) {
            return this.msg;
          }
        } else {
          if (value === null || value.length < min) {
            return this.msg;
          }
        }
      }
    }]);
  }(SkFormValidator);

  function _callSuper$X(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$X() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$X() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$X = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkEmail = /*#__PURE__*/function (_SkFormValidator) {
    function SkEmail() {
      _classCallCheck(this, SkEmail);
      return _callSuper$X(this, SkEmail, arguments);
    }
    _inherits(SkEmail, _SkFormValidator);
    return _createClass(SkEmail, [{
      key: "msg",
      get: function get() {
        return 'The value must me a valid email';
      }
    }, {
      key: "attrName",
      get: function get() {
        return 'sk-email';
      }
    }, {
      key: "validate",
      value: function validate(field) {
        var value = field.getAttribute('value');
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!re.test(String(value).toLowerCase())) {
          return this.msg;
        }
      }
    }]);
  }(SkFormValidator);

  function _callSuper$W(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$W() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$W() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$W = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkPattern = /*#__PURE__*/function (_SkFormValidator) {
    function SkPattern() {
      _classCallCheck(this, SkPattern);
      return _callSuper$W(this, SkPattern, arguments);
    }
    _inherits(SkPattern, _SkFormValidator);
    return _createClass(SkPattern, [{
      key: "msg",
      get: function get() {
        return 'The value must match requirements';
      }
    }, {
      key: "attrName",
      get: function get() {
        return 'sk-pattern';
      }
    }, {
      key: "validate",
      value: function validate(field) {
        var value = field.getAttribute('value');
        if (value !== null) {
          var pattern = new RegExp(field.getAttribute('sk-pattern'));
          if (!pattern.test(value.toString())) {
            return this.msg;
          }
        }
      }
    }]);
  }(SkFormValidator);

  function _callSuper$V(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$V() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$V() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$V = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkMin = /*#__PURE__*/function (_SkFormValidator) {
    function SkMin() {
      _classCallCheck(this, SkMin);
      return _callSuper$V(this, SkMin, arguments);
    }
    _inherits(SkMin, _SkFormValidator);
    return _createClass(SkMin, [{
      key: "msg",
      get: function get() {
        return 'The value is less than required';
      }
    }, {
      key: "attrName",
      get: function get() {
        return 'sk-min';
      }
    }, {
      key: "validate",
      value: function validate(field) {
        var value = field.getAttribute('value');
        var fieldType = field.getAttribute('type');
        var min = field.getAttribute('sk-min');
        if (fieldType === 'number') {
          if (parseFloat(value) < parseFloat(min)) {
            return this.msg;
          }
        } else {
          if (value === null || value.length < min) {
            return this.msg;
          }
        }
      }
    }]);
  }(SkFormValidator);

  function _callSuper$U(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$U() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$U() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$U = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$y(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var FORWARDED_ATTRS = {
    'name': 'name',
    'value': 'value',
    'type': 'type',
    'disabled': 'disabled',
    'list': 'list',
    'placeholder': 'placeholder',
    'minlength': 'minlength',
    'maxlength': 'maxlength',
    'required': 'required',
    'pattern': 'pattern',
    'min': 'min',
    'max': 'max',
    'step': 'step'
  };
  var SkInputImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkInputImpl() {
      _classCallCheck(this, SkInputImpl);
      return _callSuper$U(this, SkInputImpl, arguments);
    }
    _inherits(SkInputImpl, _SkComponentImpl);
    return _createClass(SkInputImpl, [{
      key: "suffix",
      get: function get() {
        return 'input';
      }
    }, {
      key: "input",
      get: function get() {
        if (!this._input) {
          this._input = this.comp.el.querySelector('input');
        }
        return this._input;
      },
      set: function set(el) {
        this._input = el;
      }
    }, {
      key: "inputEl",
      get: function get() {
        if (!this._inputEl) {
          this._inputEl = this.comp.el.querySelector('input');
        }
        return this._inputEl;
      },
      set: function set(el) {
        this._inputEl = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['inputEl', 'input'];
      }
    }, {
      key: "onInput",
      value: function onInput(event) {
        this.comp.setAttribute('value', event.target.value);
        var result = this.validateWithAttrs();
        if (typeof result === 'string') {
          this.input.setCustomValidity(result);
          this.showInvalid();
        } else {
          this.input.setCustomValidity('');
          this.showValid();
        }
        this.comp.dispatchEvent(new CustomEvent('input', {
          target: this.comp,
          detail: {
            value: event.target.value
          },
          bubbles: true,
          composed: true
        }));
        this.comp.dispatchEvent(new CustomEvent('skinput', {
          target: this.comp,
          detail: {
            value: event.target.value
          },
          bubbles: true,
          composed: true
        }));
      }
    }, {
      key: "onFocus",
      value: function onFocus(event) {
        this.removeUntouched();
      }
    }, {
      key: "removeUntouched",
      value: function removeUntouched() {
        if (this.input && this.input.classList.contains('untouched')) {
          this.input.classList.remove('untouched');
        }
      }
    }, {
      key: "setUntouched",
      value: function setUntouched() {
        if (this.input) {
          this.input.classList.add('untouched');
        }
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$y(SkInputImpl, "bindEvents", this, 3)([]);
        this.input.oninput = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onInput(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.input.onfocus = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onFocus(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$y(SkInputImpl, "unbindEvents", this, 3)([]);
        this.input.oninput = null;
        this.input.onfocus = null;
      }
    }, {
      key: "skValidators",
      get: function get() {
        if (!this._skValidators) {
          this._skValidators = {
            'sk-required': new SkRequired(),
            'sk-min': new SkMin(),
            'sk-max': new SkMax(),
            'sk-email': new SkEmail(),
            'sk-pattern': new SkPattern()
          };
        }
        return this._skValidators;
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        _superPropGet$y(SkInputImpl, "showInvalid", this, 3)([]);
        this.inputEl.classList.add('form-invalid');
      }
    }, {
      key: "showValid",
      value: function showValid() {
        _superPropGet$y(SkInputImpl, "showValid", this, 3)([]);
        this.inputEl.classList.remove('form-invalid');
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        _superPropGet$y(SkInputImpl, "restoreState", this, 3)([state]);
        this.clearAllElCache();
        this.forwardAttributes();
        this.bindEvents();
      }
    }, {
      key: "forwardAttributes",
      value: function forwardAttributes() {
        for (var _i = 0, _Object$keys = Object.keys(FORWARDED_ATTRS); _i < _Object$keys.length; _i++) {
          var attrName = _Object$keys[_i];
          var value = this.comp.getAttribute(attrName);
          if (value) {
            this.input.setAttribute(attrName, value);
          } else {
            if (this.comp.hasAttribute(attrName)) {
              this.input.setAttribute(attrName, '');
            }
          }
          if (attrName === 'list' && value && this.comp.useShadowRoot) {
            // import datalist into shadow root
            var listEl = document.getElementById(value);
            if (listEl) {
              this.comp.el.append(document.importNode(listEl, true));
            }
          }
        }
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$y(SkInputImpl, "afterRendered", this, 3)([]);
        this.restoreState();
        this.setUntouched();
      }
    }, {
      key: "msgEl",
      get: function get() {
        return this.comp.el.querySelector('.form-validation-message');
      }
    }, {
      key: "renderValidation",
      value: function renderValidation(validity) {
        validity = validity ? validity : this.comp.inputEl.validationMessage;
        this.comp.inputEl.setCustomValidity(validity);
        _superPropGet$y(SkInputImpl, "renderValidation", this, 3)([validity]);
      }
    }, {
      key: "flushValidation",
      value: function flushValidation() {
        _superPropGet$y(SkInputImpl, "flushValidation", this, 3)([]);
        this.comp.inputEl.setCustomValidity('');
      }
    }, {
      key: "focus",
      value: function focus() {
        var _this = this;
        setTimeout(function () {
          _newArrowCheck(this, _this);
          this.comp.inputEl.focus();
        }.bind(this), 0);
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$T(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$T() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$T() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$T = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$x(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkInput = /*#__PURE__*/function (_SkInputImpl) {
    function JquerySkInput() {
      _classCallCheck(this, JquerySkInput);
      return _callSuper$T(this, JquerySkInput, arguments);
    }
    _inherits(JquerySkInput, _SkInputImpl);
    return _createClass(JquerySkInput, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$x(JquerySkInput, "afterRendered", this, 3)([]);
        this.mountStyles();
      }
    }]);
  }(SkInputImpl);

  function _callSuper$S(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$S() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$S() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$S = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$w(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkInput = /*#__PURE__*/function (_SkElement) {
    function SkInput() {
      _classCallCheck(this, SkInput);
      return _callSuper$S(this, SkInput, arguments);
    }
    _inherits(SkInput, _SkElement);
    return _createClass(SkInput, [{
      key: "cnSuffix",
      get: function get() {
        return 'input';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "value",
      get: function get() {
        return this.getAttribute('value');
      },
      set: function set(value) {
        if (value === null) {
          this.removeAttribute('value');
        } else {
          this.setAttribute('value', value);
        }
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['inputEl'];
      }
    }, {
      key: "inputEl",
      get: function get() {
        if (!this._inputEl && this.el) {
          this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
      },
      set: function set(el) {
        this._inputEl = el;
      }
    }, {
      key: "validity",
      get: function get() {
        return this.inputEl ? this.inputEl.validity : false;
      }
    }, {
      key: "validationMessage",
      get: function get() {
        return this.inputEl && this.inputEl.validationMessage && this.inputEl.validationMessage !== 'undefined' ? this.inputEl.validationMessage : this.locale.tr('Field value is invalid');
      },
      set: function set(validationMessage) {
        this._validationMessage = validationMessage;
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (!this.inputEl) {
          return false;
        }
        if (FORWARDED_ATTRS[name] && oldValue !== newValue) {
          if (newValue !== null && newValue !== undefined) {
            this.inputEl.setAttribute(name, newValue);
          } else {
            this.inputEl.removeAttribute(name);
          }
        }
        if (name === 'value' && this.inputEl) {
          this.inputEl.value = newValue;
        }
        if (name === 'form-invalid') {
          if (newValue !== null) {
            this.impl.showInvalid();
          } else {
            this.impl.showValid();
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }, {
      key: "setCustomValidity",
      value: function setCustomValidity(validity) {
        _superPropGet$w(SkInput, "setCustomValidity", this, 3)([validity]);
        this.inputEl.setCustomValidity(validity);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['form-invalid'].concat(_toConsumableArray(Object.keys(FORWARDED_ATTRS)));
      }
    }]);
  }(SkElement);

  function _callSuper$R(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$R() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$R() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$R = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$v(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var BTN_FORWARDED_ATTRS = {
    'type': 'type',
    'disabled': 'disabled'
  };
  var SkButtonImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkButtonImpl() {
      _classCallCheck(this, SkButtonImpl);
      return _callSuper$R(this, SkButtonImpl, arguments);
    }
    _inherits(SkButtonImpl, _SkComponentImpl);
    return _createClass(SkButtonImpl, [{
      key: "suffix",
      get: function get() {
        return 'button';
      }
    }, {
      key: "buttonEl",
      get: function get() {
        if (!this._buttonEl) {
          this._buttonEl = this.comp.el.querySelector('button');
        }
        return this._buttonEl;
      },
      set: function set(el) {
        this._buttonEl = el;
      }
    }, {
      key: "button",
      get: function get() {
        if (!this._button) {
          this._button = this.comp.el.querySelector('button');
        }
        return this._button;
      },
      set: function set(el) {
        this._button = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['buttonEl', 'button'];
      }
    }, {
      key: "buttonType",
      get: function get() {
        return this.comp.getAttribute('button-type');
      }
    }, {
      key: "enable",
      value: function enable() {
        _superPropGet$v(SkButtonImpl, "enable", this, 3)([]);
        this.buttonEl.removeAttribute('disabled');
      }
    }, {
      key: "disable",
      value: function disable() {
        _superPropGet$v(SkButtonImpl, "disable", this, 3)([]);
        //this.buttonEl.setAttribute('disabled', 'disabled');
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        _superPropGet$v(SkButtonImpl, "restoreState", this, 3)([state]);
        this.forwardAttributes();
      }
    }, {
      key: "forwardAttributes",
      value: function forwardAttributes() {
        if (this.buttonEl) {
          for (var _i = 0, _Object$keys = Object.keys(BTN_FORWARDED_ATTRS); _i < _Object$keys.length; _i++) {
            var attrName = _Object$keys[_i];
            var value = this.comp.getAttribute(attrName);
            if (value) {
              this.buttonEl.setAttribute(attrName, value);
            } else {
              if (this.comp.hasAttribute(attrName)) {
                this.buttonEl.setAttribute(attrName, '');
              }
            }
            if (attrName === 'list' && value && this.comp.useShadowRoot) {
              // import datalist into shadow root
              this.comp.el.append(document.importNode(document.getElementById(value), true));
            }
          }
        }
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$v(SkButtonImpl, "bindEvents", this, 3)([]);
        if (this.comp.getAttribute('type') === 'submit') {
          this.buttonEl.onclick = function (event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onSubmit(event);
            this.comp.callPluginHook('onEventEnd', event);
          }.bind(this);
        }
      }
      /*
      
          onClick(event) {
              this.comp.el.dispatchEvent(new CustomEvent('skclick', { target: this.comp, bubbles: true, composed: true }));
              this.comp.el.dispatchEvent(new CustomEvent('click', { target: this.comp, bubbles: true, composed: true }));
          }
      */
    }, {
      key: "onSubmit",
      value: function onSubmit(event) {
        this.comp.el.dispatchEvent(new CustomEvent('formsubmit', {
          target: this.comp,
          bubbles: true,
          composed: true
        }));
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$v(SkButtonImpl, "unbindEvents", this, 3)([]);
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$v(SkButtonImpl, "afterRendered", this, 3)([]);
        this.restoreState();
      }
    }, {
      key: "focus",
      value: function focus() {
        var _this = this;
        setTimeout(function () {
          _newArrowCheck(this, _this);
          this.comp.buttonEl.focus();
        }.bind(this), 0);
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$Q(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$Q() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$Q() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$Q = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$u(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkButton = /*#__PURE__*/function (_SkButtonImpl) {
    function JquerySkButton() {
      _classCallCheck(this, JquerySkButton);
      return _callSuper$Q(this, JquerySkButton, arguments);
    }
    _inherits(JquerySkButton, _SkButtonImpl);
    return _createClass(JquerySkButton, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'button';
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$u(JquerySkButton, "afterRendered", this, 3)([]);
        if (this.buttonType) {
          this.button.classList.add("sk-btn-".concat(this.buttonType));
        }
        this.mountStyles();
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        _superPropGet$u(JquerySkButton, "restoreState", this, 3)([state]);
        this.clearAllElCache();
        var disabled = this.comp.getAttribute('disabled');
        if (disabled) {
          this.buttonEl.setAttribute('disabled', 'disabled');
          this.buttonEl.classList.add('ui-state-disabled');
        }
      }
    }, {
      key: "enable",
      value: function enable() {
        _superPropGet$u(JquerySkButton, "enable", this, 3)([]);
        this.buttonEl.classList.remove('ui-state-disabled');
      }
    }, {
      key: "disable",
      value: function disable() {
        _superPropGet$u(JquerySkButton, "disable", this, 3)([]);
        this.buttonEl.classList.add('ui-state-disabled');
      }
    }]);
  }(SkButtonImpl);

  function _callSuper$P(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$P() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$P() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$P = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkButton = /*#__PURE__*/function (_SkElement) {
    function SkButton() {
      _classCallCheck(this, SkButton);
      return _callSuper$P(this, SkButton, arguments);
    }
    _inherits(SkButton, _SkElement);
    return _createClass(SkButton, [{
      key: "buttonEl",
      get: function get() {
        return this.el ? this.el.querySelector('button') : null;
      }
    }, {
      key: "cnSuffix",
      get: function get() {
        return 'button';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'disabled' && oldValue !== newValue && this.implRenderTimest) {
          if (newValue) {
            this.impl.disable();
          } else {
            this.impl.enable();
          }
        }
        if (BTN_FORWARDED_ATTRS[name] && oldValue !== newValue) {
          if (newValue !== null && newValue !== undefined) {
            if (this.buttonEl) {
              this.buttonEl.setAttribute(name, newValue);
            }
          } else {
            this.buttonEl.removeAttribute(name);
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return Object.keys(BTN_FORWARDED_ATTRS);
      }
    }]);
  }(SkElement);

  function _callSuper$O(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$O() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$O() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$O = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$t(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkAlertImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkAlertImpl() {
      _classCallCheck(this, SkAlertImpl);
      return _callSuper$O(this, SkAlertImpl, arguments);
    }
    _inherits(SkAlertImpl, _SkComponentImpl);
    return _createClass(SkAlertImpl, [{
      key: "suffix",
      get: function get() {
        return 'alert';
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$t(SkAlertImpl, "beforeRendered", this, 3)([]);
        this.saveState();
      }
    }, {
      key: "slot",
      get: function get() {
        return this.comp.el.querySelector('slot');
      }
    }, {
      key: "dumpState",
      value: function dumpState() {
        return {
          'contents': this.comp.contentsState
        };
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this.slot.innerHTML = state.contentsState;
      }
    }, {
      key: "onClose",
      value: function onClose() {
        this.comp.style.display = 'none';
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$t(SkAlertImpl, "bindEvents", this, 3)([]);
        if (this.comp.hasAttribute('closable')) {
          this.closeBtnEl.onclick = function (event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onClose(event);
            this.comp.callPluginHook('onEventEnd', event);
          }.bind(this);
        }
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$t(SkAlertImpl, "unbindEvents", this, 3)([]);
        this.closeBtnEl.onclick = null;
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$t(SkAlertImpl, "afterRendered", this, 3)([]);
        this.restoreState({
          contentsState: this.contentsState || this.comp.contentsState
        });
        if (!this.comp.hasAttribute('closable')) {
          if (this.closeBtnEl) {
            this.closeBtnEl.style.display = 'none';
          }
        }
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$N(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$N() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$N() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$N = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$s(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkAlert = /*#__PURE__*/function (_SkAlertImpl) {
    function JquerySkAlert() {
      _classCallCheck(this, JquerySkAlert);
      return _callSuper$N(this, JquerySkAlert, arguments);
    }
    _inherits(JquerySkAlert, _SkAlertImpl);
    return _createClass(JquerySkAlert, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'alert';
      }
    }, {
      key: "closeBtnEl",
      get: function get() {
        if (!this._closeBtnEl) {
          this._closeBtnEl = this.comp.el.querySelector('.sk-alert-close-icon');
        }
        return this._closeBtnEl;
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$s(JquerySkAlert, "afterRendered", this, 3)([]);
        this.mountStyles();
      }
    }]);
  }(SkAlertImpl);

  function _callSuper$M(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$M() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$M() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$M = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkAlert = /*#__PURE__*/function (_SkElement) {
    function SkAlert() {
      _classCallCheck(this, SkAlert);
      return _callSuper$M(this, SkAlert, arguments);
    }
    _inherits(SkAlert, _SkElement);
    return _createClass(SkAlert, [{
      key: "cnSuffix",
      get: function get() {
        return 'alert';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "type",
      get: function get() {
        return this.getAttribute('type') || 'success';
      }
    }, {
      key: "close",
      value: function close() {
        this.impl.onClose();
      }
    }]);
  }(SkElement);

  function _createForOfIteratorHelper$e(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$e(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$e(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$e(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$e(r, a) : void 0; } }
  function _arrayLikeToArray$e(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$L(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$L() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$L() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$L = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$r(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkAccordionImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkAccordionImpl() {
      _classCallCheck(this, SkAccordionImpl);
      return _callSuper$L(this, SkAccordionImpl, arguments);
    }
    _inherits(SkAccordionImpl, _SkComponentImpl);
    return _createClass(SkAccordionImpl, [{
      key: "suffix",
      get: function get() {
        return 'accordion';
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$r(SkAccordionImpl, "beforeRendered", this, 3)([]);
        this.saveState();
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this.contentEl.innerHTML = '';
        this.contentEl.insertAdjacentHTML('beforeend', state.contentsState);
        this.renderTabs();
        this.bindTabSwitch();
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$r(SkAccordionImpl, "afterRendered", this, 3)([]);
        this.clearAllElCache();
        this.restoreState({
          contentsState: this.contentsState || this.comp.contentsState
        });
        this.mountStyles();
      }
    }, {
      key: "bindTabSwitch",
      value: function bindTabSwitch() {
        this.contentEl.querySelectorAll('.accordion-header').forEach(function (link) {
          link.onclick = function (event) {
            this.comp.callPluginHook('onEventStart', event);
            if (this.comp.getAttribute('disabled')) {
              return false;
            }
            var target = event.currentTarget;
            var tabId = target.getAttribute('data-tab');
            this.toggleTab(tabId);
            this.comp.callPluginHook('onEventEnd', event);
          }.bind(this);
        }.bind(this));
      }
    }, {
      key: "closeAllTabs",
      value: function closeAllTabs() {
        var tabs = this.contentEl.querySelectorAll('.accordion-header');
        var _iterator = _createForOfIteratorHelper$e(tabs),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var tab = _step.value;
            var tabNum = tab.dataset.tab;
            this.closeTab(tabNum);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }, {
      key: "closeTab",
      value: function closeTab(tabNum) {
        var tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
          tab.removeAttribute('open');
          var activeTab = this.contentEl.querySelector("#header-".concat(tabNum));
          activeTab.classList.remove('accordion-header-active', 'state-active');
          var activeTabPane = this.contentEl.querySelector("#panel-".concat(tabNum));
          activeTabPane.style.display = 'none';
          this.selectedTab = null;
        }
      }
    }, {
      key: "toggleTab",
      value: function toggleTab(tabNum) {
        var tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
          this.closeTab(tabNum);
        } else {
          if (this.comp.hasAttribute('mono')) {
            if (this.selectedTab) {
              this.closeTab(this.selectedTab);
            } else {
              this.closeAllTabs();
            }
          }
          var activeTab = this.contentEl.querySelector("#header-".concat(tabNum));
          activeTab.classList.add('accordion-header-active', 'state-active');
          var activeTabPane = this.contentEl.querySelector("#panel-".concat(tabNum));
          activeTabPane.style.display = 'block';
          this.selectedTab = tabNum;
          tab.setAttribute('open', '');
        }
      }
    }]);
  }(SkComponentImpl);

  function _createForOfIteratorHelper$d(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$d(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$d(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$d(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$d(r, a) : void 0; } }
  function _arrayLikeToArray$d(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$K(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$K() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$K() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$K = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$q(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkAccordion = /*#__PURE__*/function (_SkAccordionImpl) {
    function JquerySkAccordion() {
      _classCallCheck(this, JquerySkAccordion);
      return _callSuper$K(this, JquerySkAccordion, arguments);
    }
    _inherits(JquerySkAccordion, _SkAccordionImpl);
    return _createClass(JquerySkAccordion, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'accordion';
      }
    }, {
      key: "contentEl",
      get: function get() {
        if (!this._contentEl) {
          this._contentEl = this.comp.el.querySelector('[role=tablist]');
        }
        return this._contentEl;
      },
      set: function set(el) {
        this._contentEl = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['contentEl'];
      }
    }, {
      key: "bindTabSwitch",
      value: function bindTabSwitch() {
        this.contentEl.querySelectorAll('.ui-accordion-header').forEach(function (link) {
          link.onclick = function (event) {
            if (this.comp.getAttribute('disabled')) {
              return false;
            }
            var tabId = event.target.getAttribute('data-tab');
            this.toggleTab(tabId);
          }.bind(this);
        }.bind(this));
      }
    }, {
      key: "closeAllTabs",
      value: function closeAllTabs() {
        var tabs = this.contentEl.querySelectorAll('.ui-accordion-header');
        var _iterator = _createForOfIteratorHelper$d(tabs),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var tab = _step.value;
            var tabNum = tab.dataset.tab;
            this.closeTab(tabNum);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }, {
      key: "closeTab",
      value: function closeTab(tabNum) {
        var tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
          tab.removeAttribute('open');
          var activeTab = this.contentEl.querySelector("#header-".concat(tabNum));
          activeTab.classList.remove('accordion-header-active', 'ui-state-active');
          var triIcon = activeTab.querySelector('.ui-accordion-header-icon');
          triIcon.classList.add('ui-icon-triangle-1-e');
          triIcon.classList.remove('ui-icon-triangle-1-s');
          var activeTabPane = this.contentEl.querySelector("#panel-".concat(tabNum));
          activeTabPane.style.display = 'none';
          this.selectedTab = null;
        }
      }
    }, {
      key: "toggleTab",
      value: function toggleTab(tabNum) {
        var tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
          this.closeTab(tabNum);
        } else {
          if (this.comp.hasAttribute('mono')) {
            if (this.selectedTab) {
              this.closeTab(this.selectedTab);
            } else {
              this.closeAllTabs();
            }
          }
          var activeTab = this.contentEl.querySelector("#header-".concat(tabNum));
          activeTab.classList.add('accordion-header-active', 'ui-state-active');
          var triIcon = activeTab.querySelector('.ui-accordion-header-icon');
          triIcon.classList.remove('ui-icon-triangle-1-e');
          triIcon.classList.add('ui-icon-triangle-1-s');
          var activeTabPane = this.contentEl.querySelector("#panel-".concat(tabNum));
          activeTabPane.style.display = 'block';
          this.selectedTab = tabNum;
          tab.setAttribute('open', '');
        }
      }
    }, {
      key: "renderTabs",
      value: function renderTabs() {
        var tabs = this.comp.el.querySelectorAll('sk-tab');
        var num = 1;
        this.tabs = {};
        var _iterator2 = _createForOfIteratorHelper$d(tabs),
          _step2;
        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var tab = _step2.value;
            var isOpen = tab.hasAttribute('open');
            var title = tab.getAttribute('title') ? tab.getAttribute('title') : '';
            this.contentEl.insertAdjacentHTML('beforeend', "\n                <h3 class=\"ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons".concat(isOpen ? ' accordion-header-active state-active' : '', "\" \n                    role=\"tab\" id=\"header-").concat(num, "\" aria-controls=\"ui-accordion-mainMenu-panel-0\" \n                    aria-selected=\"").concat(!isOpen ? "true" : "false", "\" aria-expanded=\"").concat(!isOpen ? "true" : "false", "\" tabindex=\"").concat(num - 1, "\" data-tab=\"").concat(num, "\" ").concat(isOpen ? 'open' : '', ">\n                    <span class=\"ui-accordion-header-icon ui-icon ui-icon-triangle-1-e\"></span>").concat(title, "</h3>\n                <div class=\"ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom\" ").concat(!isOpen ? 'style="display: none;"' : '', "  \n                    id=\"panel-").concat(num, "\" aria-labelledby=\"header-").concat(num, "\" role=\"tabpanel\" aria-hidden=\"").concat(!isOpen ? "true" : "false", "\">\n                    ").concat(tab.outerHTML, "\n                </div>        \n            "));
            this.removeEl(tab);
            this.tabs['tabs-' + num] = this.contentEl.querySelector('#header-' + num);
            num++;
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
      }
    }, {
      key: "disable",
      value: function disable() {
        var _this = this;
        _superPropGet$q(JquerySkAccordion, "enable", this, 3)([]);
        this.contentEl.querySelectorAll('.ui-accordion-header').forEach(function (tab) {
          _newArrowCheck(this, _this);
          tab.classList.add('ui-state-disabled');
        }.bind(this));
      }
    }, {
      key: "enable",
      value: function enable() {
        var _this2 = this;
        _superPropGet$q(JquerySkAccordion, "disable", this, 3)([]);
        this.contentEl.querySelectorAll('.ui-accordion-header').forEach(function (tab) {
          _newArrowCheck(this, _this2);
          tab.classList.add('ui-state-disabled');
        }.bind(this));
      }
    }]);
  }(SkAccordionImpl);

  function _callSuper$J(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$J() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$J() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$J = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkAccordion = /*#__PURE__*/function (_SkElement) {
    function SkAccordion() {
      _classCallCheck(this, SkAccordion);
      return _callSuper$J(this, SkAccordion, arguments);
    }
    _inherits(SkAccordion, _SkElement);
    return _createClass(SkAccordion, [{
      key: "cnSuffix",
      get: function get() {
        return 'accordion';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }]);
  }(SkElement);

  function _callSuper$I(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$I() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$I() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$I = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$p(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkAppImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkAppImpl() {
      _classCallCheck(this, SkAppImpl);
      return _callSuper$I(this, SkAppImpl, arguments);
    }
    _inherits(SkAppImpl, _SkComponentImpl);
    return _createClass(SkAppImpl, [{
      key: "suffix",
      get: function get() {
        return 'app';
      }
    }, {
      key: "dynamicImportSupported",
      value: function dynamicImportSupported() {
        try {
          new Function('import("")');
          return true;
        } catch (err) {
          return false;
        }
      }
    }, {
      key: "setupRouter",
      value: function setupRouter() {
        var _this = this;
        var root = this.comp.confValOrDefault('ro-root', "/");
        var useHash = this.comp.confValOrDefault('ro-use-hash', false);
        this.router = new Navigo(root, {
          hash: useHash === 'true'
        });
        var _loop = function _loop() {
          var route = _Object$keys[_i];
          var path = _this.comp.routes[route];
          if (typeof path === 'string') {
            if (customElements.get(_this.comp.routes[route])) {
              _this.router.on(route, function (event) {
                this.comp.dispatchEvent(new CustomEvent('skroutestart', {
                  target: this.comp,
                  detail: {
                    value: route
                  },
                  bubbles: true,
                  composed: true
                }));
                this.renderCompByRoute(path);
                this.comp.dispatchEvent(new CustomEvent('skrouteend', {
                  target: this.comp,
                  detail: {
                    value: route
                  },
                  bubbles: true,
                  composed: true
                }));
              }.bind(_this));
            } else {
              _this.logger.warn('Attempt to use unregistred custom element for route: ', path);
            }
          } else {
            var _loop2 = function _loop2() {
              var _this2 = this;
              var key = _Object$keys2[_i2];
              if (path[key].indexOf('.js') > 0) {
                if (_this.dynamicImportSupported() && _this.comp.getAttribute('dimport') !== 'false') {
                  require("".concat(path[key])).then(function (m) {
                    _newArrowCheck(this, _this2);
                    _this.router.on(route, function (event) {
                      this.comp.dispatchEvent(new CustomEvent('skroutestart', {
                        target: this.comp,
                        detail: {
                          value: route
                        },
                        bubbles: true,
                        composed: true
                      }));
                      var tagName = 'sk-' + key.toLowerCase();
                      this.renderCompByRoute(tagName, m[key]);
                      this.comp.dispatchEvent(new CustomEvent('skrouteend', {
                        target: this.comp,
                        detail: {
                          value: route
                        },
                        bubbles: true,
                        composed: true
                      }));
                    }.bind(_this));
                    _this.router.resolve();
                  }.bind(this));
                } else {
                  _this.router.on(route, function (event) {
                    var _this3 = this;
                    try {
                      var def = Function('return ' + key)();
                      this.comp.dispatchEvent(new CustomEvent('skroutestart', {
                        target: this.comp,
                        detail: {
                          value: route
                        },
                        bubbles: true,
                        composed: true
                      }));
                      var tagName = 'sk-' + key.toLowerCase();
                      this.renderCompByRoute(tagName, def);
                      this.comp.dispatchEvent(new CustomEvent('skrouteend', {
                        target: this.comp,
                        detail: {
                          value: route
                        },
                        bubbles: true,
                        composed: true
                      }));
                    } catch (_unused) {
                      var scriptPromise = new Promise(function (resolve, reject) {
                        _newArrowCheck(this, _this3);
                        var script = document.createElement('script');
                        script.onload = resolve;
                        script.onerror = reject;
                        script.async = true;
                        script.src = path[key];
                        document.body.appendChild(script);
                      }.bind(this));
                      scriptPromise.then(function () {
                        this.comp.dispatchEvent(new CustomEvent('skroutestart', {
                          target: this.comp,
                          detail: {
                            value: route
                          },
                          bubbles: true,
                          composed: true
                        }));
                        var tagName = 'sk-' + key.toLowerCase();
                        var def = Function('return ' + key)();
                        this.renderCompByRoute(tagName, def);
                        this.comp.dispatchEvent(new CustomEvent('skrouteend', {
                          target: this.comp,
                          detail: {
                            value: route
                          },
                          bubbles: true,
                          composed: true
                        }));
                      }.bind(this));
                    }
                  }.bind(_this));
                }
              }
            };
            for (var _i2 = 0, _Object$keys2 = Object.keys(path); _i2 < _Object$keys2.length; _i2++) {
              _loop2();
            }
          }
        };
        for (var _i = 0, _Object$keys = Object.keys(this.comp.routes); _i < _Object$keys.length; _i++) {
          _loop();
        }
        this.router.resolve();
      }
    }, {
      key: "renderCompByRoute",
      value: function renderCompByRoute(tagName, comp) {
        if (comp && !customElements.get(tagName)) {
          customElements.define("".concat(tagName), comp);
        }
        this.comp.slot.innerHTML = '';
        this.comp.slot.insertAdjacentHTML('beforeend', "<".concat(tagName, "></").concat(tagName, ">"));
      }
    }, {
      key: "afterRendered",
      value: function () {
        var _afterRendered = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                _superPropGet$p(SkAppImpl, "afterRendered", this, 3)([]);
                this.logger.debug('app after rendered, config routes', this.comp.configEl);
                if (Object.keys(this.comp.routes).length > 0) {
                  this.setupRouter();
                }
              case 3:
              case "end":
                return _context.stop();
            }
          }, _callee, this);
        }));
        function afterRendered() {
          return _afterRendered.apply(this, arguments);
        }
        return afterRendered;
      }()
    }]);
  }(SkComponentImpl);

  function _callSuper$H(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$H() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$H() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$H = function _isNativeReflectConstruct() { return !!t; })(); }
  var JquerySkApp = /*#__PURE__*/function (_SkAppImpl) {
    function JquerySkApp() {
      _classCallCheck(this, JquerySkApp);
      return _callSuper$H(this, JquerySkApp, arguments);
    }
    _inherits(JquerySkApp, _SkAppImpl);
    return _createClass(JquerySkApp, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'app';
      }
    }]);
  }(SkAppImpl);

  function _callSuper$G(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$G() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$G() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$G = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkApp = /*#__PURE__*/function (_SkElement) {
    function SkApp() {
      _classCallCheck(this, SkApp);
      return _callSuper$G(this, SkApp, arguments);
    }
    _inherits(SkApp, _SkElement);
    return _createClass(SkApp, [{
      key: "cnSuffix",
      get: function get() {
        return 'app';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "routes",
      get: function get() {
        if (!this._routes) {
          this._routes = JSON.parse(this.confValOrDefault('routes', '{}'));
        }
        return this._routes;
      },
      set: function set(routes) {
        this._routes = routes;
        this.setAttribute(JSON.stringify(this._routes));
      }
    }, {
      key: "slot",
      get: function get() {
        if (!this._slot) {
          this._slot = this.el.querySelector('slot');
        }
        return this._slot;
      }
    }]);
  }(SkElement);

  function _callSuper$F(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$F() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$F() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$F = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$o(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var CHK_FORWARDED_ATTRS = {
    'disabled': 'disabled',
    'checked': 'checked',
    'required': 'required'
  };
  var SkCheckboxImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkCheckboxImpl() {
      _classCallCheck(this, SkCheckboxImpl);
      return _callSuper$F(this, SkCheckboxImpl, arguments);
    }
    _inherits(SkCheckboxImpl, _SkComponentImpl);
    return _createClass(SkCheckboxImpl, [{
      key: "suffix",
      get: function get() {
        return 'checkbox';
      }
    }, {
      key: "input",
      get: function get() {
        return this.comp.el.querySelector('input');
      }
    }, {
      key: "onChange",
      value: function onChange(event) {
        this.comp.setAttribute('value', event.target.value);
        if (event.target.checked) {
          this.comp.setAttribute('checked', 'checked');
        } else {
          this.comp.removeAttribute('checked');
        }
        var result = this.validateWithAttrs();
        if (typeof result === 'string') {
          this.input.setCustomValidity(result);
          this.showInvalid();
        }
        this.comp.dispatchEvent(new CustomEvent('change', {
          target: this.comp,
          detail: {
            value: event.target.value,
            checked: event.target.checked
          },
          bubbles: true,
          composed: true
        }));
        this.comp.dispatchEvent(new CustomEvent('skchange', {
          target: this.comp,
          detail: {
            value: event.target.value,
            checked: event.target.checked
          },
          bubbles: true,
          composed: true
        }));
      }
    }, {
      key: "onFocus",
      value: function onFocus(event) {
        this.removeUntouched();
      }
    }, {
      key: "removeUntouched",
      value: function removeUntouched() {
        if (this.input && this.input.classList.contains('untouched')) {
          this.input.classList.remove('untouched');
        }
      }
    }, {
      key: "setUntouched",
      value: function setUntouched() {
        if (this.input) {
          this.input.classList.add('untouched');
        }
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$o(SkCheckboxImpl, "bindEvents", this, 3)([]);
        this.input.onchange = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onChange(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.input.onfocus = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onFocus(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$o(SkCheckboxImpl, "unbindEvents", this, 3)([]);
        this.input.onchange = null;
        this.input.oninput = null;
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        _superPropGet$o(SkCheckboxImpl, "restoreState", this, 3)([state]);
        this.comp.clearElCache();
        this.forwardAttributes();
        this.bindEvents();
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$o(SkCheckboxImpl, "afterRendered", this, 3)([]);
        this.restoreState();
        this.setUntouched();
      }
    }, {
      key: "forwardAttributes",
      value: function forwardAttributes() {
        if (this.input) {
          for (var _i = 0, _Object$keys = Object.keys(CHK_FORWARDED_ATTRS); _i < _Object$keys.length; _i++) {
            var attrName = _Object$keys[_i];
            var value = this.comp.getAttribute(attrName);
            if (value) {
              this.input.setAttribute(attrName, value);
            } else {
              if (this.comp.hasAttribute(attrName)) {
                this.input.setAttribute(attrName, '');
              }
            }
          }
        }
      }
    }, {
      key: "check",
      value: function check() {
        this.input.checked = 'checked';
      }
    }, {
      key: "uncheck",
      value: function uncheck() {
        this.input.checked = false;
      }
    }, {
      key: "disable",
      value: function disable() {}
    }, {
      key: "enable",
      value: function enable() {}
    }, {
      key: "skValidators",
      get: function get() {
        if (!this._skValidators) {
          this._skValidators = {
            'sk-required': new SkRequired()
          };
        }
        return this._skValidators;
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        _superPropGet$o(SkCheckboxImpl, "showInvalid", this, 3)([]);
        this.input.classList.add('form-invalid');
      }
    }, {
      key: "showValid",
      value: function showValid() {
        _superPropGet$o(SkCheckboxImpl, "showValid", this, 3)([]);
        this.input.classList.remove('form-invalid');
      }
    }, {
      key: "renderValidation",
      value: function renderValidation(validity) {
        this.comp.inputEl.setCustomValidity(validity);
        _superPropGet$o(SkCheckboxImpl, "renderValidation", this, 3)([]);
      }
    }, {
      key: "flushValidation",
      value: function flushValidation() {
        _superPropGet$o(SkCheckboxImpl, "flushValidation", this, 3)([]);
        this.comp.inputEl.setCustomValidity('');
      }
    }, {
      key: "focus",
      value: function focus() {
        var _this = this;
        setTimeout(function () {
          _newArrowCheck(this, _this);
          this.comp.inputEl.focus();
        }.bind(this), 0);
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$E(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$E() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$E() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$E = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$n(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkCheckbox = /*#__PURE__*/function (_SkCheckboxImpl) {
    function JquerySkCheckbox() {
      _classCallCheck(this, JquerySkCheckbox);
      return _callSuper$E(this, JquerySkCheckbox, arguments);
    }
    _inherits(JquerySkCheckbox, _SkCheckboxImpl);
    return _createClass(JquerySkCheckbox, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'checkbox';
      }
    }, {
      key: "checked",
      get: function get() {
        return this.input.hasAttribute('checked');
      },
      set: function set(checked) {
        if (checked) {
          this.input.checked = 'checked';
          this.input.setAttribute('checked', 'checked');
        } else {
          this.input.checked = false;
          this.input.removeAttribute('checked');
        }
      }
      // sk-checkbox element's context
    }, {
      key: "onClick",
      value: function onClick(event) {
        this.checked = !this.checked;
        if (this.checked) {
          this.inact = true;
          this.comp.setAttribute('checked', 'checked');
          this.inact = false;
        } else {
          this.inact = true;
          this.comp.removeAttribute('checked');
          this.inact = false;
        }
        var result = this.validateWithAttrs();
        if (typeof result === 'string') {
          this.comp.inputEl.setCustomValidity(result);
          this.showInvalid();
        }
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        if (this.checked) {
          this.input.setAttribute('checked', this.checked);
        }
        this.comp.onclick = function (event) {
          this.onClick(event);
        }.bind(this);
      }
    }, {
      key: "enable",
      value: function enable() {
        _superPropGet$n(JquerySkCheckbox, "enable", this, 3)([]);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$n(JquerySkCheckbox, "unbindEvents", this, 3)([]);
        this.comp.removeEventListener('click', this.clickHandle);
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        _superPropGet$n(JquerySkCheckbox, "showInvalid", this, 3)([]);
      }
    }, {
      key: "showValid",
      value: function showValid() {
        _superPropGet$n(JquerySkCheckbox, "showValid", this, 3)([]);
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$n(JquerySkCheckbox, "afterRendered", this, 3)([]);
        this.mountStyles();
      }
    }]);
  }(SkCheckboxImpl);

  function _callSuper$D(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$D() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$D() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$D = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkCheckbox = /*#__PURE__*/function (_SkElement) {
    function SkCheckbox() {
      _classCallCheck(this, SkCheckbox);
      return _callSuper$D(this, SkCheckbox, arguments);
    }
    _inherits(SkCheckbox, _SkElement);
    return _createClass(SkCheckbox, [{
      key: "cnSuffix",
      get: function get() {
        return 'checkbox';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['inputEl'];
      }
    }, {
      key: "inputEl",
      get: function get() {
        if (!this._inputEl && this.el) {
          this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
      },
      set: function set(el) {
        this._inputEl = el;
      }
    }, {
      key: "validationMessage",
      get: function get() {
        return this.inputEl && this.inputEl.validationMessage && this.inputEl.validationMessage !== 'undefined' ? this.inputEl.validationMessage : this.locale.tr('Field value is invalid');
      },
      set: function set(validationMessage) {
        this._validationMessage = validationMessage;
      }
    }, {
      key: "validity",
      get: function get() {
        return this.inputEl ? this.inputEl.validity : false;
      }
    }, {
      key: "checked",
      get: function get() {
        return this.getAttribute('checked');
      },
      set: function set(checked) {
        this.setAttribute('checked', checked);
      }
    }, {
      key: "value",
      get: function get() {
        if (this.getAttribute('checked')) {
          return this.getAttribute('value');
        } else {
          return null;
        }
      },
      set: function set(value) {
        if (value === null) {
          this.removeAttribute('value');
        } else {
          this.setAttribute('value', value);
        }
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (!this.inputEl) {
          return false;
        }
        if (CHK_FORWARDED_ATTRS[name] && oldValue !== newValue) {
          if (newValue !== null && newValue !== undefined) {
            this.inputEl.setAttribute(name, newValue);
          } else {
            this.inputEl.removeAttribute(name);
          }
        }
        if (name === 'checked' && !this.inact) {
          if (newValue) {
            this.impl.check();
          } else {
            this.impl.uncheck();
          }
        }
        if (name === 'disabled') {
          if (newValue) {
            this.impl.disable();
          } else {
            this.impl.enable();
          }
        }
        if (name === 'form-invalid') {
          if (newValue !== null) {
            this.impl.showInvalid();
          } else {
            this.impl.showValid();
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['form-invalid'].concat(_toConsumableArray(Object.keys(CHK_FORWARDED_ATTRS)));
      }
    }]);
  }(SkElement);

  function _callSuper$C(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$C() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$C() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$C = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$m(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var DATEPICKER_FORWARDED_ATTRS = {
    'name': 'name',
    'value': 'value',
    'disabled': 'disabled',
    'required': 'required',
    'pattern': 'pattern',
    'min': 'min',
    'max': 'max',
    'step': 'step'
  };
  var SkDatepickerImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkDatepickerImpl() {
      _classCallCheck(this, SkDatepickerImpl);
      return _callSuper$C(this, SkDatepickerImpl, arguments);
    }
    _inherits(SkDatepickerImpl, _SkComponentImpl);
    return _createClass(SkDatepickerImpl, [{
      key: "suffix",
      get: function get() {
        return 'datepicker';
      }
    }, {
      key: "forwardAttributes",
      value: function forwardAttributes() {
        for (var _i = 0, _Object$keys = Object.keys(DATEPICKER_FORWARDED_ATTRS); _i < _Object$keys.length; _i++) {
          var attrName = _Object$keys[_i];
          var value = this.comp.getAttribute(attrName);
          if (value) {
            this.input.setAttribute(attrName, value);
          } else {
            if (this.comp.hasAttribute(attrName)) {
              this.input.setAttribute(attrName, '');
            }
          }
        }
      }
    }, {
      key: "onInputChange",
      value: function onInputChange(event) {
        this.comp.setAttribute('value', event.target.value);
        var result = this.validateWithAttrs();
        if (typeof result === 'string') {
          this.input.setCustomValidity(result);
          this.showInvalid();
        } else {
          this.showValid();
        }
        this.comp.dispatchEvent(new CustomEvent('change', {
          target: event.target,
          detail: {
            value: event.target.value,
            checked: event.target.checked
          },
          bubbles: true,
          composed: true
        }));
        this.comp.dispatchEvent(new CustomEvent('skchange', {
          target: event.target,
          detail: {
            value: event.target.value,
            checked: event.target.checked
          },
          bubbles: true,
          composed: true
        }));
      }
    }, {
      key: "onInputInput",
      value: function onInputInput(event) {
        this.comp.setAttribute('value', event.target.value);
        var result = this.validateWithAttrs();
        if (typeof result === 'string') {
          this.input.setCustomValidity(result);
          this.showInvalid();
        } else {
          this.showValid();
        }
        this.comp.dispatchEvent(new CustomEvent('input', {
          target: event.target,
          detail: {
            value: event.target.value,
            checked: event.target.checked
          },
          bubbles: true,
          composed: true
        }));
        this.comp.dispatchEvent(new CustomEvent('skinput', {
          target: event.target,
          detail: {
            value: event.target.value,
            checked: event.target.checked
          },
          bubbles: true,
          composed: true
        }));
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        _superPropGet$m(SkDatepickerImpl, "restoreState", this, 3)([state]);
        this.clearAllElCache();
        this.forwardAttributes();
        this.bindChangeEvents();
      }
    }, {
      key: "bindChangeEvents",
      value: function bindChangeEvents() {
        var _this = this;
        if (this.input) {
          this.input.onchange = function (event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onInputChange(event);
            this.comp.callPluginHook('onEventEnd', event);
          }.bind(this);
          this.input.oninput = function (event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onInputInput(event);
            this.comp.callPluginHook('onEventEnd', event);
          }.bind(this);
          this.comp.addEventListener('skinput', function (event) {
            _newArrowCheck(this, _this);
            this.comp.callPluginHook('onEventStart', event);
            var result = this.validateWithAttrs();
            if (typeof result === 'string') {
              this.input.setCustomValidity(result);
              this.showInvalid();
            } else {
              this.input.setCustomValidity('');
              this.showValid();
            }
            this.comp.callPluginHook('onEventEnd', event);
          }.bind(this));
        }
      }
    }, {
      key: "skValidators",
      get: function get() {
        if (!this._skValidators) {
          this._skValidators = {
            'sk-required': new SkRequired(),
            'sk-min': new SkMin(),
            'sk-max': new SkMax()
          };
        }
        return this._skValidators;
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        _superPropGet$m(SkDatepickerImpl, "showInvalid", this, 3)([]);
        this.input.classList.add('form-invalid');
      }
    }, {
      key: "showValid",
      value: function showValid() {
        _superPropGet$m(SkDatepickerImpl, "showValid", this, 3)([]);
        this.input.classList.remove('form-invalid');
      }
    }, {
      key: "focus",
      value: function focus() {
        var _this2 = this;
        setTimeout(function () {
          _newArrowCheck(this, _this2);
          this.comp.inputEl.focus();
        }.bind(this), 0);
      }
    }]);
  }(SkComponentImpl);

  var DateTime = /*#__PURE__*/function () {
    function DateTime(date) {
      _classCallCheck(this, DateTime);
      if (arguments.length === 0) this.date = new Date();else if (date instanceof Date) this.date = new Date(date);else throw Error('Argument must be a date object. ' + date + ' was given');
    }

    /**
     * Returns DateTime for given date by setting time to midnight
     * @param year
     * @param month
     * @param day
     * @returns {DateTime} new DateTime object or throws error
     */
    return _createClass(DateTime, [{
      key: "withResetMS",
      value:
      /**
       * Returns new DateTime with milliseconds set to 0
       */
      function withResetMS() {
        var newDate = this.clone();
        newDate.date.setMilliseconds(0);
        return newDate;
      }

      /**
       * Returns new DateTime with given hours and minutes and 0 milliseconds
       * @param h 0-23
       * @param m 0-59
       */
    }, {
      key: "withTime",
      value: function withTime(h, m) {
        if (typeof h === 'string') {
          var hoursAndMinutes = h.split(':');
          h = hoursAndMinutes[0];
          m = hoursAndMinutes[1];
        }
        var dateWithTime = this.clone();
        dateWithTime.date.setHours(h);
        dateWithTime.date.setMinutes(m);
        dateWithTime.date.setSeconds(0);
        dateWithTime.date.setMilliseconds(0);
        return dateWithTime;
      }

      /**
       * Returns new DateTime with current time
       * @returns {DateTime}
       */
    }, {
      key: "getTime",
      value:
      /**
       * Returns time in milliseconds
       * @returns {number} milliseconds
       */
      function getTime() {
        return this.date.getTime();
      }

      /**
       * Returns year
       * @returns {number} year
       */
    }, {
      key: "getFullYear",
      value: function getFullYear() {
        return this.date.getFullYear();
      }

      /**
       * Returns day of month
       * @returns {number} 1-31
       */
    }, {
      key: "getDate",
      value: function getDate() {
        return this.date.getDate();
      }

      /**
       * Returns month
       * @returns {number} 1-12
       */
    }, {
      key: "getMonth",
      value: function getMonth() {
        return this.date.getMonth() + 1;
      }

      /**
       * Returns day of week. 0=sunday, 1=monday, ...
       * @returns {number} 0-6
       */
    }, {
      key: "getDay",
      value: function getDay() {
        return this.date.getDay();
      }

      /**
       * Returns hours
       * @returns {number} 0-23
       */
    }, {
      key: "getHours",
      value: function getHours() {
        return this.date.getHours();
      }

      /**
       * Returns minutes
       * @returns {number} 0-59
       */
    }, {
      key: "getMinutes",
      value: function getMinutes() {
        return this.date.getMinutes();
      }

      /**
       * Returns seconds
       * @returns {number} 0-59
       */
    }, {
      key: "getSeconds",
      value: function getSeconds() {
        return this.date.getSeconds();
      }

      /**
       * Returns milliseconds
       * @returns {number} 0-999
       */
    }, {
      key: "getMilliseconds",
      value: function getMilliseconds() {
        return this.date.getMilliseconds();
      }

      /**
       * Returns days in month for current DateTime
       * @returns {number}
       */
    }, {
      key: "getDaysInMonth",
      value: function getDaysInMonth() {
        return DateTime.getDaysInMonth(this.getFullYear(), this.getMonth());
      }

      /**
       * Returns days in year for current Date
       * @returns {*}
       */
    }, {
      key: "getDayInYear",
      value: function getDayInYear() {
        return DateTime.getDayInYear(this.getFullYear(), this.getMonth(), this.getDate());
      }

      /**
       * Returns new DateTime with given days later
       * @param days
       * @returns {DateTime}
       */
    }, {
      key: "plusDays",
      value: function plusDays(days) {
        var newDateTime = DateTime.fromMillis(this.getTime() + days * DateTime.DAY);
        var hours = this.getHours();

        // Fix the DateTime offset caused by daylight saving time
        var delta = hours - newDateTime.getHours();
        if (delta !== 0) {
          // Correct the delta to be between [-12, 12]
          if (delta > 12) {
            delta -= 24;
          }
          if (delta < -12) {
            delta += 24;
          }
          return DateTime.fromMillis(newDateTime.getTime() + delta * DateTime.HOUR);
        }
        return newDateTime;
      }

      /**
       * Returns new DateTime with given minutes later
       * @param minutes
       * @returns {DateTime}
       */
    }, {
      key: "plusMinutes",
      value: function plusMinutes(minutes) {
        return DateTime.fromMillis(this.clone().getTime() + minutes * DateTime.MINUTE);
      }

      /**
       * Returns new DateTime with given minutes earlier
       * @param minutes
       * @returns {DateTime}
       */
    }, {
      key: "minusMinutes",
      value: function minusMinutes(minutes) {
        return this.plusMinutes(-minutes);
      }

      /**
       * Returns new DateTime with given days earlier
       * @param days
       * @returns {DateTime}
       */
    }, {
      key: "minusDays",
      value: function minusDays(days) {
        return this.plusDays(-days);
      }

      /**
       * Compares DateTimes. Examples:
       * earlier.compareTo(later)) < 0
       * later.compareTo(earlier)) > 0
       * later.compareTo(later)) == 0
        * @param date {DateTime}
       * @returns {number}
       */
    }, {
      key: "compareTo",
      value: function compareTo(date) {
        if (!date) {
          return 1;
        }
        var diff = this.getTime() - date.getTime();
        return diff === 0 ? 0 : diff / Math.abs(diff);
      }

      /**
       * Returns true if DateTime is within today
       */
    }, {
      key: "isToday",
      value: function isToday() {
        return this.equalsOnlyDate(DateTime.today());
      }

      /**
       * Returns the week number of current DateTime
       * @param {string} weekNumberingSystem US or ISO
       * @returns {number}
       */
    }, {
      key: "getWeekInYear",
      value: function getWeekInYear(weekNumberingSystem) {
        if (weekNumberingSystem !== 'US' && weekNumberingSystem !== 'ISO') {
          throw 'Week numbering system must be either US or ISO, was ' + weekNumberingSystem;
        }
        var firstDay = new Date(this.getFullYear(), 0, 1).getDay();
        if (weekNumberingSystem === 'US') {
          return Math.ceil((this.getDayInYear() + firstDay) / 7);
        }
        var THU = 4;
        var weekday = this.getDay();
        if (weekday === 0) weekday = 7;
        if (firstDay === 0) firstDay = 7;
        // If Dec 29 falls on Mon, Dec 30 on Mon or Tue, Dec 31 on Mon - Wed, it's on the first week of next year
        if (this.getMonth() === 12 && this.getDate() >= 29 && this.getDate() - weekday > 27) {
          return 1;
        }
        // If Jan 1-3 falls on Fri, Sat or Sun, it's on the last week of the previous year
        if (this.getMonth() === 1 && this.getDate() < 4 && weekday > THU) {
          return new DateTime(new Date(this.getFullYear() - 1, 11, 31)).getWeekInYear('ISO');
        }
        var week = Math.ceil((this.getDayInYear() + firstDay - 1) / 7);
        // If first days of this year are on last year's last week, the above gives one week too much
        if (firstDay > THU) week--;
        return week;
      }

      /**
       * Creates clone of current DateTime
       * @returns {DateTime}
       */
    }, {
      key: "clone",
      value: function clone() {
        return new DateTime(this.date);
      }

      /**
       * Returs true if month is odd, ie. january=true
       * @returns {boolean}
       */
    }, {
      key: "isOddMonth",
      value: function isOddMonth() {
        return this.getMonth() % 2 === 0;
      }

      /**
       * Returns true if given DateTime has same day as current DateTime
       * @param date
       * @returns {boolean}
       */
    }, {
      key: "equalsOnlyDate",
      value: function equalsOnlyDate(date) {
        if (!date) return false;
        return this.getMonth() === date.getMonth() && this.getDate() === date.getDate() && this.getFullYear() === date.getFullYear();
      }

      /**
       * Returns first date of month from current date
       * @returns {DateTime}
       */
    }, {
      key: "firstDateOfMonth",
      value: function firstDateOfMonth() {
        return DateTime.fromDate(this.getFullYear(), this.getMonth(), 1);
      }

      /**
       * Returns last date of month from current date
       * @returns {DateTime}
       */
    }, {
      key: "lastDateOfMonth",
      value: function lastDateOfMonth() {
        return DateTime.fromDate(this.getFullYear(), this.getMonth(), this.getDaysInMonth());
      }

      /**
       * Returns number of days between current and given date
       * @param date
       * @returns {number}
       */
    }, {
      key: "distanceInDays",
      value: function distanceInDays(date) {
        var first = parseInt(this.getTime() / DateTime.DAY, 10);
        var last = parseInt(date.getTime() / DateTime.DAY, 10);
        return last - first;
      }

      /**
       * Returns new DateTime from same week with given weekDay
       * @param weekday 0=sunday, 1=monday, ...
       * @returns {DateTime}
       */
    }, {
      key: "withWeekday",
      value: function withWeekday(weekday) {
        return this.plusDays(weekday - this.getDay());
      }

      /**
       * Returns new DateTime with midnight time
       * @returns {DateTime}
       */
    }, {
      key: "getOnlyDate",
      value: function getOnlyDate() {
        return DateTime.fromDate(this.getFullYear(), this.getMonth(), this.getDate());
      }

      /**
       * Returns true if date is in weekend
       * @returns {boolean}
       */
    }, {
      key: "isWeekend",
      value: function isWeekend() {
        return this.getDay() === 6 || this.getDay() === 0;
      }

      /**
       * Returns default string representation
       */
    }, {
      key: "toString",
      value: function toString() {
        return this.toISOString();
      }

      /**
       * Returns first date from same week
       * @param locale Based on locale it can be a monday or a sunday
       * @returns {DateTime}
       */
    }, {
      key: "getFirstDateOfWeek",
      value: function getFirstDateOfWeek(locale) {
        var firstWeekday = locale ? locale.firstWeekday : DateTime.MONDAY;
        if (firstWeekday == this.getDay) return this.clone();else return this.plusDays(firstWeekday - this.getDay() - (firstWeekday > this.getDay() ? 7 : 0));
      }

      /**
       * Returns ISO DateTime string: YYYY-MM-DDT:HH:MM:SS
       * @returns {string}
       */
    }, {
      key: "toISOString",
      value: function toISOString() {
        return isoDate.call(this) + 'T' + isoTime.call(this);
      }

      /**
       * Returns ISO Date string: YYYY-MM-DD
       */
    }, {
      key: "toISODateString",
      value: function toISODateString() {
        return isoDate.call(this);
      }

      /**
       * Returns true if current DateTime is between start and end DateTimes
       * @param {DateTime} start
       * @param {DateTime} end
       * @returns {boolean}
       */
    }, {
      key: "isBetweenDates",
      value: function isBetweenDates(start, end) {
        if (start.getTime() > end.getTime()) throw Error("start date can't be after end date");
        var onlyDate = this.getOnlyDate();
        return onlyDate.compareTo(start.getOnlyDate()) >= 0 && onlyDate.compareTo(end.getOnlyDate()) <= 0;
      }

      /**
       * Returns number of days for given month
       * @param {Number} year Year of month
       * @param {Number} month Number of month (1-12)
       * @returns {Number} [28-31]
       */
    }], [{
      key: "fromDate",
      value: function fromDate(year, month, day) {
        return DateTime.fromDateTime(year, month, day, 0, 0, 0);
      }

      /**
       * Returns DateTime for given date and time
       * @param year
       * @param month 1-12
       * @param day 1-31
       * @param hours 0-23
       * @param minutes 0-59
       * @param seconds 0-59
       * @returns {DateTime} new DateTime object or throws error
       */
    }, {
      key: "fromDateTime",
      value: function fromDateTime(year, month, day, hours, minutes, seconds) {
        return new DateTime(createSafeDate(+year, +month, +day, +hours, +minutes, +seconds || 0));
      }

      /**
       * Returns DateTime from given Date object
       * @param date
       * @returns {DateTime}
       */
    }, {
      key: "fromDateObject",
      value: function fromDateObject(date) {
        return new DateTime(date);
      }

      /**
       * Returns DateTime from ISO date ignoring time information
       * @param isoDate String YYYY-MM-DDTHH-MM
       * @return {DateTime}
       */
    }, {
      key: "fromIsoDate",
      value: function fromIsoDate(isoDate) {
        var optionalTimePattern = /^\d{4}-[01]\d-[0-3]\d(T[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z?))?$/;
        if (!optionalTimePattern.test(isoDate)) throw Error(isoDate + ' is not valid ISO Date (YYYY-MM-DD or YYYY-MM-DDTHH:MM)');
        var date = parseDate(isoDate.split('T')[0]);
        return DateTime.fromDate(date.year, date.month, date.day);
      }

      /**
       * Returns DateTime with time from ISO date
       * @param isoDateTime String YYYY-MM-DDTHH-MM
       * @return {DateTime} Returns DateTime or throws error for invalid syntax
       */
    }, {
      key: "fromIsoDateTime",
      value: function fromIsoDateTime(isoDateTime) {
        var fullPatternTest = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z?)/;
        if (!fullPatternTest.test(isoDateTime)) throw Error(isoDateTime + ' is not valid ISO Date (YYYY-MM-DDTHH:MM)');
        var dateAndTime = isoDateTime.split('T');
        var time = parseTime(dateAndTime.length === 2 && dateAndTime[1]);
        var date = parseDate(dateAndTime[0]);
        return DateTime.fromDateTime(date.year, date.month, date.day, time.hours, time.minutes, time.seconds);
      }

      /**
       * Returns DateTime from current time in milliseconds
       * @param ms
       * @returns {DateTime}
       */
    }, {
      key: "fromMillis",
      value: function fromMillis(ms) {
        return new DateTime(new Date(ms));
      }
    }, {
      key: "now",
      value: function now() {
        return new DateTime();
      }

      /**
       * Returns new DateTime with current date and midnight time
       */
    }, {
      key: "today",
      value: function today() {
        return DateTime.now().getOnlyDate();
      }
    }, {
      key: "getDaysInMonth",
      value: function getDaysInMonth(year, month) {
        if (month > 12 || month < 1) throw new Error('Month must be between 1-12');
        var yearAndMonth = year * 12 + month;
        return DateTime.fromDate(Math.floor(yearAndMonth / 12), yearAndMonth % 12 + 1, 1).minusDays(1).getDate();
      }

      /**
       * Returns index of given day from beginning of year
       * @param year year
       * @param month month
       * @param day day
       * @returns {Number} index number starting grom beginning of year
       */
    }, {
      key: "getDayInYear",
      value: function getDayInYear(year, month, day) {
        return DateTime.fromDate(year, 1, 1).distanceInDays(DateTime.fromDate(year, month, day)) + 1;
      }
    }]);
  }();
  DateTime.SUNDAY = 0;
  DateTime.MONDAY = 1;
  DateTime.TUESDAY = 2;
  DateTime.WEDNESDAY = 3;
  DateTime.THURSDAY = 4;
  DateTime.FRIDAY = 5;
  DateTime.SATURDAY = 6;
  DateTime.daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  DateTime.y2kYear = 50;
  DateTime.monthNumbers = {
    Jan: 0,
    Feb: 1,
    Mar: 2,
    Apr: 3,
    May: 4,
    Jun: 5,
    Jul: 6,
    Aug: 7,
    Sep: 8,
    Oct: 9,
    Nov: 10,
    Dec: 11
  };
  DateTime.SECOND = 1000;
  DateTime.MINUTE = 60 * DateTime.SECOND;
  DateTime.HOUR = 60 * DateTime.MINUTE;
  DateTime.DAY = 24 * DateTime.HOUR;
  DateTime.WEEK = 7 * DateTime.DAY;
  function isoDate() {
    return this.getFullYear() + '-' + twoDigits(this.getMonth()) + '-' + twoDigits(this.getDate());
  }
  function isoTime() {
    return twoDigits(this.getHours()) + ':' + twoDigits(this.getMinutes()) + ':' + twoDigits(this.getSeconds());
  }
  function twoDigits(value) {
    return value < 10 ? '0' + value : '' + value;
  }
  function createSafeDate(year, month, date, hours, minutes, seconds) {
    hours = hours || 0;
    minutes = minutes || 0;
    seconds = seconds || 0;
    var newDate = new Date(year, month - 1, date, hours, minutes, seconds, 0);
    if (newDate.toString() === 'Invalid Date' || month !== newDate.getMonth() + 1 || year !== newDate.getFullYear() || date !== newDate.getDate() || hours !== newDate.getHours() || minutes !== newDate.getMinutes() || seconds !== newDate.getSeconds()) throw Error('Invalid Date: ' + year + '-' + month + '-' + date + ' ' + hours + ':' + minutes + ':' + seconds);
    return newDate;
  }
  function parseDate(str) {
    var dateComponents = str.split('-');
    return {
      year: +dateComponents[0],
      month: +dateComponents[1],
      day: +dateComponents[2]
    };
  }
  function parseTime(str) {
    if (str) {
      var timeComponents = str.split(':');
      return {
        hours: +timeComponents[0],
        minutes: +timeComponents[1],
        seconds: +timeComponents[2] || 0
      };
    } else {
      return {
        hours: 0,
        minutes: 0
      };
    }
  }

  var _this$b = undefined;
  var codes = {
    d: function d(_d) {
      _newArrowCheck(this, _this$b);
      return leftPad(_d.getDate(), 2, '0');
    }.bind(undefined),
    D: function D(d, l) {
      _newArrowCheck(this, _this$b);
      return l.shortDayNames[d.getDay()];
    }.bind(undefined),
    j: function j(d) {
      _newArrowCheck(this, _this$b);
      return d.getDate();
    }.bind(undefined),
    l: function l(d, _l) {
      _newArrowCheck(this, _this$b);
      return _l.dayNames[d.getDay()];
    }.bind(undefined),
    w: function w(d) {
      _newArrowCheck(this, _this$b);
      return d.getDay();
    }.bind(undefined),
    z: function z(d) {
      _newArrowCheck(this, _this$b);
      return d.getDayInYear();
    }.bind(undefined),
    F: function F(d, l) {
      _newArrowCheck(this, _this$b);
      return l.monthNames[d.getMonth() - 1];
    }.bind(undefined),
    m: function m(d) {
      _newArrowCheck(this, _this$b);
      return leftPad(d.getMonth(), 2, '0');
    }.bind(undefined),
    M: function M(d, l) {
      _newArrowCheck(this, _this$b);
      return l.monthNames[d.getMonth() - 1].substring(0, 3);
    }.bind(undefined),
    n: function n(d) {
      _newArrowCheck(this, _this$b);
      return d.getMonth();
    }.bind(undefined),
    t: function t(d) {
      _newArrowCheck(this, _this$b);
      return d.getDaysInMonth();
    }.bind(undefined),
    Y: function Y(d) {
      _newArrowCheck(this, _this$b);
      return d.getFullYear();
    }.bind(undefined),
    y: function y(d) {
      _newArrowCheck(this, _this$b);
      return ('' + d.getFullYear()).substring(2, 4);
    }.bind(undefined),
    a: function a(d) {
      _newArrowCheck(this, _this$b);
      return d.getHours() < 12 ? 'am' : 'pm';
    }.bind(undefined),
    A: function A(d) {
      _newArrowCheck(this, _this$b);
      return d.getHours() < 12 ? 'AM' : 'PM';
    }.bind(undefined),
    g: function g(d) {
      _newArrowCheck(this, _this$b);
      return d.getHours() % 12 ? d.getHours() % 12 : 12;
    }.bind(undefined),
    G: function G(d) {
      _newArrowCheck(this, _this$b);
      return d.getHours();
    }.bind(undefined),
    h: function h(d) {
      _newArrowCheck(this, _this$b);
      return leftPad(d.getHours() % 12 ? d.getHours() % 12 : 12, 2, '0');
    }.bind(undefined),
    H: function H(d) {
      _newArrowCheck(this, _this$b);
      return leftPad(d.getHours(), 2, '0');
    }.bind(undefined),
    i: function i(d) {
      _newArrowCheck(this, _this$b);
      return leftPad(d.getMinutes(), 2, '0');
    }.bind(undefined),
    s: function s(d) {
      _newArrowCheck(this, _this$b);
      return leftPad(d.getSeconds(), 2, '0');
    }.bind(undefined),
    Z: function Z(d) {
      _newArrowCheck(this, _this$b);
      return d.date.getTimezoneOffset() / -60;
    }.bind(undefined)
  };

  /**
   * Formatting patterns listed above
   * @param {Date} d [01-31]
   * @param {Short_Day_Name} D [Su, Mo, Tu, We, Th, Fr, Sa]
   * @param {Date} j [1-31]
   * @param {Full_day_name} l  [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
   * @param {Week_day_number} w 0=Sunday, 1=Monday, 2=Tuesday etc...
   * @param {Nth_day_of_year} z [1-365] except leap years
   * @param {Full_month_name} F [January, February, ...]
   * @param {Month_number} m [01-12]
   * @param {Month_name_stripped_to_three_letters} M [Jan, Feb, ...]
   * @param {Month_number} n [1-12]
   * @param {Days_in_current_month} t [28-31]
   * @param {Full_year} Y [1900, ...]
   * @param {Last_two_digits_of_a_year} y [01-99]
   * @param {Time_postfix} a [am|pm]
   * @param {Time_postfix} A [AM|PM]
   * @param {Hours_in_12h_format} g [1-12]
   * @param {Hours_in_24h_format} G [0-23]
   * @param {Hour_in_12h_format_with_padding} h [01-12]
   * @param {Hours_in_24h_format_with_padding} H [00-23]
   * @param {Minutes_with_padding} i [00-59]
   * @param {Seconds_with_padding} s [00-59]
   * @param {Timezone} Z 2 for GMT+2
   */
  var DateFormat = /*#__PURE__*/function () {
    function DateFormat() {
      _classCallCheck(this, DateFormat);
    }
    return _createClass(DateFormat, null, [{
      key: "hoursAndMinutes",
      value: /** Returns hours and minutes as hours in decimal. For example <code>DateFormat.hoursAndMinutes(22,30)</code> returns <code>22.5</code> */
      function hoursAndMinutes(hours, minutes) {
        return (Math.round((hours + minutes / 60) * 100) / 100).toString();
      }

      /** Formats dateTime. For example <code>DateFormat.format(DateTime.fromDateTime(2014, 2, 25, 14, 30), 'Y-m-d H:i:s', DateLocale.EN)</code> returns <code>2014-02-25 14:30:00</code>
       * @param {DateTime} dateTime DateTime object to be formatted
       * @param {String} format  Pattern to be used for formatting
       * @param {DateLocale} locale  Locale to be used for formatting
       * @see DateFormat.patterns
       * @returns {String} Formatted date
       * */
    }, {
      key: "format",
      value: function format(dateTime, _format, locale) {
        var result = '';
        var special = false;
        var ch = '';
        for (var i = 0; i < _format.length; ++i) {
          ch = _format.charAt(i);
          if (!special && ch === '\\') {
            special = true;
          } else {
            if (special) {
              special = false;
              result += ch;
            } else {
              result += codeToValue(dateTime, ch, locale);
            }
          }
        }
        return result;
      }

      /**
       * Shorthand for formatting in short date format. For example <code>DateFormat.shortDateFormat(DateTime.fromDateTime(2014, 2, 25, 14, 30), DateLocale.EN)</code> returns <code>2/25/2014</code>
       * @param {DateTime} dateTime DateTime to be formattend
       * @param {DateLocale} locale locale to be used for formatting
       * @returns {String} Returns Date in short date format depending on locale
       */
    }, {
      key: "shortDateFormat",
      value: function shortDateFormat(dateTime, locale) {
        return DateFormat.format(dateTime, locale ? locale.shortDateFormat : 'n/j/Y', locale);
      }
      /**
       * Formats DateRange. TODO
       * @param {DateRange} dateRange DateRange to be formatted
       * @param {DateLocale} locale to be used for formatting
       * @returns {string} returns date range in formatted form, for example <code>2/25/2014-2/15/2015</code>
       */
    }, {
      key: "formatRange",
      value: function formatRange(dateRange, locale) {
        if (dateRange._hasTimes) {
          return locale.daysLabel(dateRange.days()) + ' ' + locale.hoursLabel(dateRange.hours(), dateRange.minutes());
        } else {
          return DateFormat.shortDateFormat(dateRange.start, locale) + ' - ' + DateFormat.shortDateFormat(dateRange.end, locale);
        }
      }

      /**
       * Need's documentation
       * @param dateRange
       * @param locale
       * @returns {*}
       */
    }, {
      key: "formatDefiningRangeDuration",
      value: function formatDefiningRangeDuration(dateRange, locale) {
        var years = parseInt(dateRange.days() / 360, 10);
        if (years > 0) return locale.yearsLabel(years);
        var months = parseInt(dateRange.days() / 30, 10);
        if (months > 0) return locale.monthsLabel(months);
        return locale.daysLabel(dateRange.days());
      }
    }]);
  }();
  /**
   * List of commonly used date format patterns
   * Above are listed results for following command: <code>DateFormat.format(DateTime.fromDateTime(2014, 2, 5, 14, 30), PATTERN,  DateLocale.EN)</code>
   *
   * @param {DateLocale.EN} ISO8601LongPattern 2014-02-05 14:30:00
   * @param {DateLocale.EN} ISO8601ShortPattern 2014-02-05
   * @param {DateLocale.EN} ShortDatePattern 2/5/2014
   * @param {DateLocale.EN} ShortDatePattern 2/5/2014
   * @param {DateLocale.EN} FiShortDatePattern  5.2.2014
   * @param {DateLocale.EN} FiWeekdayDatePattern We 5.2.2014
   * @param {DateLocale.FI} FiWeekdayDateTimePattern ke 5.2.2014 klo 14:30 (for DateLocale.FI)
   * @param {DateLocale.EN} LongDatePattern Wednesday, February 05, 2014
   * @param {DateLocale.EN} FullDateTimePattern Wednesday, February 05, 2014 2:30:00 PM
   * @param {DateLocale.EN} MontdDayPattern February 05
   * @param {DateLocale.EN} ShortTimePattern 2:30 PM
   * @param {DateLocale.EN} LongTimePattern 2:30:00 PM
   * @param {DateLocale.EN} SortableDateTimePattern 2014-02-05T14:30:00
   * @param {DateLocale.EN} UniversalSortableDateTimePattern 2014-02-05 14:30:00+-200
   * @param {DateLocale.EN} YearMontdPattern February, 2014
   */
  DateFormat.patterns = {
    ISO8601LongPattern: 'Y-m-d H:i:s',
    ISO8601ShortPattern: 'Y-m-d',
    ShortDatePattern: 'n/j/Y',
    FiShortDatePattern: 'j.n.Y',
    FiWeekdayDatePattern: 'D j.n.Y',
    FiWeekdayDateTimePattern: 'D j.n.Y k\\lo G:i',
    LongDatePattern: 'l, F d, Y',
    FullDateTimePattern: 'l, F d, Y g:i:s A',
    MonthDayPattern: 'F d',
    ShortTimePattern: 'g:i A',
    LongTimePattern: 'g:i:s A',
    SortableDateTimePattern: 'Y-m-d\\TH:i:s',
    UniversalSortableDateTimePattern: 'Y-m-d H:i:sO',
    YearMonthPattern: 'F, Y'
  };

  /** @private */
  function codeToValue(dateTime, ch, locale) {
    return ch in codes ? codes[ch](dateTime, locale) : ch;
  }

  /** @private */
  function leftPad(val, size, ch) {
    var result = String(val);
    if (ch === null) {
      ch = ' ';
    }
    while (result.length < size) {
      result = ch + result;
    }
    return result;
  }

  var holidays = {
    "2015-01-01": "Uudenvuodenpäivä",
    "2015-01-06": "Loppiainen",
    "2015-04-03": "Pitkäperjantai",
    "2015-04-04": "Pääsiäislauantai",
    "2015-04-05": "Pääsiäispäivä",
    "2015-04-06": "2. pääsiäispäivä",
    "2015-05-01": "Vappu",
    "2015-05-14": "Helatorstai",
    "2015-06-19": "Juhannusaatto",
    "2015-06-20": "Juhannuspäivä",
    "2015-10-31": "Pyhäinpäivä",
    "2015-12-06": "Itsenäisyyspäivä",
    "2015-12-24": "Jouluaatto",
    "2015-12-25": "Joulupäivä",
    "2015-12-26": "Tapaninpäivä",
    "2016-01-01": "Uudenvuodenpäivä",
    "2016-01-06": "Loppiainen",
    "2016-03-25": "Pitkäperjantai",
    "2016-03-26": "Pääsiäislauantai",
    "2016-03-27": "Pääsiäispäivä",
    "2016-03-28": "2. pääsiäispäivä",
    "2016-05-01": "Vappu",
    "2016-05-05": "Helatorstai",
    "2016-06-24": "Juhannusaatto",
    "2016-06-25": "Juhannuspäivä",
    "2016-11-05": "Pyhäinpäivä",
    "2016-12-06": "Itsenäisyyspäivä",
    "2016-12-24": "Jouluaatto",
    "2016-12-25": "Joulupäivä",
    "2016-12-26": "Tapaninpäivä",
    "2017-01-01": "Uudenvuodenpäivä",
    "2017-01-06": "Loppiainen",
    "2017-04-14": "Pitkäperjantai",
    "2017-04-15": "Pääsiäislauantai",
    "2017-04-16": "Pääsiäispäivä",
    "2017-04-17": "2. pääsiäispäivä",
    "2017-05-01": "Vappu",
    "2017-05-25": "Helatorstai",
    "2017-06-23": "Juhannusaatto",
    "2017-06-24": "Juhannuspäivä",
    "2017-11-04": "Pyhäinpäivä",
    "2017-12-06": "Itsenäisyyspäivä",
    "2017-12-24": "Jouluaatto",
    "2017-12-25": "Joulupäivä",
    "2017-12-26": "Tapaninpäivä",
    "2018-01-01": "Uudenvuodenpäivä",
    "2018-01-06": "Loppiainen",
    "2018-03-30": "Pitkäperjantai",
    "2018-03-31": "Pääsiäislauantai",
    "2018-04-01": "Pääsiäispäivä",
    "2018-04-02": "2. pääsiäispäivä",
    "2018-05-01": "Vappu",
    "2018-05-10": "Helatorstai",
    "2018-06-22": "Juhannusaatto",
    "2018-06-23": "Juhannuspäivä",
    "2018-11-03": "Pyhäinpäivä",
    "2018-12-06": "Itsenäisyyspäivä",
    "2018-12-24": "Jouluaatto",
    "2018-12-25": "Joulupäivä",
    "2018-12-26": "Tapaninpäivä",
    "2019-01-01": "Uudenvuodenpäivä",
    "2019-01-06": "Loppiainen",
    "2019-04-19": "Pitkäperjantai",
    "2019-04-20": "Pääsiäislauantai",
    "2019-04-21": "Pääsiäispäivä",
    "2019-04-22": "2. pääsiäispäivä",
    "2019-05-01": "Vappu",
    "2019-05-30": "Helatorstai",
    "2019-06-21": "Juhannusaatto",
    "2019-06-22": "Juhannuspäivä",
    "2019-11-02": "Pyhäinpäivä",
    "2019-12-06": "Itsenäisyyspäivä",
    "2019-12-24": "Jouluaatto",
    "2019-12-25": "Joulupäivä",
    "2019-12-26": "Tapaninpäivä",
    "2020-01-01": "Uudenvuodenpäivä",
    "2020-01-06": "Loppiainen",
    "2020-04-10": "Pitkäperjantai",
    "2020-04-11": "Pääsiäislauantai",
    "2020-04-12": "Pääsiäispäivä",
    "2020-04-13": "2. pääsiäispäivä",
    "2020-05-01": "Vappu",
    "2020-05-21": "Helatorstai",
    "2020-06-19": "Juhannusaatto",
    "2020-06-20": "Juhannuspäivä",
    "2020-10-31": "Pyhäinpäivä",
    "2020-12-06": "Itsenäisyyspäivä",
    "2020-12-24": "Jouluaatto",
    "2020-12-25": "Joulupäivä",
    "2020-12-26": "Tapaninpäivä",
    "2021-01-01": "Uudenvuodenpäivä",
    "2021-01-06": "Loppiainen",
    "2021-04-02": "Pitkäperjantai",
    "2021-04-03": "Pääsiäislauantai",
    "2021-04-04": "Pääsiäispäivä",
    "2021-04-05": "2. pääsiäispäivä",
    "2021-05-01": "Vappu",
    "2021-05-13": "Helatorstai",
    "2021-06-25": "Juhannusaatto",
    "2021-06-26": "Juhannuspäivä",
    "2021-11-06": "Pyhäinpäivä",
    "2021-12-06": "Itsenäisyyspäivä",
    "2021-12-24": "Jouluaatto",
    "2021-12-25": "Joulupäivä",
    "2021-12-26": "Tapaninpäivä"
  };

  var _this$a = undefined;
  var FI = {
    id: 'FI',
    monthNames: ['tammikuu', 'helmikuu', 'maaliskuu', 'huhtikuu', 'toukokuu', 'kesäkuu', 'heinäkuu', 'elokuu', 'syyskuu', 'lokakuu', 'marraskuu', 'joulukuu'],
    dayNames: ['sunnuntai', 'maanantai', 'tiistai', 'keskiviikko', 'torstai', 'perjantai', 'lauantai'],
    shortDayNames: ['su', 'ma', 'ti', 'ke', 'to', 'pe', 'la'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$a);
      return years + ' ' + (years === 1 ? 'vuosi' : 'vuotta');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$a);
      return months + ' ' + (months === 1 ? 'kuukausi' : 'kuukautta');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$a);
      return days + ' ' + (days === 1 ? 'päivä' : 'päivää');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$a);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes).replace('.', ',');
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'tunti' : 'tuntia');
    }.bind(undefined),
    clearRangeLabel: 'Poista valinta',
    clearDateLabel: 'Poista valinta',
    shortDateFormat: 'j.n.Y',
    weekDateFormat: 'D j.n.Y',
    dateTimeFormat: 'D j.n.Y k\\lo G:i',
    firstWeekday: DateTime.MONDAY,
    holidays: holidays
  };

  var _this$9 = undefined;
  var EN = {
    id: 'EN',
    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    shortDayNames: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$9);
      return years + ' ' + (years === 1 ? 'Year' : 'Years');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$9);
      return months + ' ' + (months === 1 ? 'Months' : 'Months');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$9);
      return days + ' ' + (days === 1 ? 'Day' : 'Days');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$9);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes);
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'Hour' : 'Hours');
    }.bind(undefined),
    clearRangeLabel: 'Clear Range',
    clearDateLabel: 'Clear Date',
    shortDateFormat: 'n/j/Y',
    weekDateFormat: 'D n/j/Y',
    dateTimeFormat: 'D n/j/Y G:i',
    firstWeekday: DateTime.SUNDAY,
    holidays: {}
  };

  var _this$8 = undefined;
  var AU = {
    id: 'AU',
    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    shortDayNames: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$8);
      return years + ' ' + (years === 1 ? 'Year' : 'Years');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$8);
      return months + ' ' + (months === 1 ? 'Months' : 'Months');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$8);
      return days + ' ' + (days === 1 ? 'Day' : 'Days');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$8);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes);
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'Hour' : 'Hours');
    }.bind(undefined),
    clearRangeLabel: 'Clear Range',
    clearDateLabel: 'Clear Date',
    shortDateFormat: 'j/n/Y',
    weekDateFormat: 'D j/n/Y',
    dateTimeFormat: 'D j/n/Y G:i',
    firstWeekday: DateTime.SUNDAY,
    holidays: {}
  };

  var _this$7 = undefined;
  var ET = {
    id: 'ET',
    monthNames: ['Jaanuar', 'Veebruar', 'Märts', 'Aprill', 'Mai', 'Juuni', 'Juuli', 'August', 'September', 'Oktoober', 'November', 'Detsember'],
    dayNames: ['Pühapäev', 'Esmaspäev', 'Teisipäev', 'Kolmapäev', 'Neljapäev', 'Reede', 'Laupäev'],
    shortDayNames: ['P', 'E', 'T', 'K', 'N', 'R', 'L'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$7);
      return years + ' ' + (years === 1 ? 'Aasta' : 'Aastat');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$7);
      return months + ' ' + (months === 1 ? 'Kuu' : 'Kuud');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$7);
      return days + ' ' + (days === 1 ? 'Päev' : 'Päeva');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$7);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes).replace('.', ',');
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'Tund' : 'Tundi');
    }.bind(undefined),
    clearRangeLabel: 'TODO',
    clearDateLabel: 'TODO',
    shortDateFormat: 'j.n.Y',
    weekDateFormat: 'D j.n.Y',
    dateTimeFormat: 'D j.n.Y G:i',
    firstWeekday: DateTime.MONDAY,
    holidays: {}
  };

  var _this$6 = undefined;

  /**
   * For example:
   *   1 год
   *   2 года, 3 года, 4 года
   *   5 лет, 6 лет … 11 лет, 12 лет … 20 лет
   *   21 год, 31 год
   *   22 года, 32 года
   * @param {number} number
   * @param {Array} words
   * @return {string}
   */
  function pluralize(number, words) {
    var magnitude = number % 100;
    var pluralWord = '';
    if (magnitude > 10 && magnitude < 20 || number === 0) {
      pluralWord = words[2];
    } else {
      switch (Math.abs(number % 10)) {
        case 1:
          pluralWord = words[0];
          break;
        case 2:
        case 3:
        case 4:
          pluralWord = words[1];
          break;
        default:
          pluralWord = words[2];
          break;
      }
    }
    return [number, pluralWord].join(' ');
  }
  var RU$1 = {
    id: 'RU',
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    shortDayNames: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$6);
      return pluralize(years, ['Год', 'Года', 'Лет']);
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$6);
      return pluralize(months, ['Месяц', 'Месяца', 'Месяцев']);
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$6);
      return pluralize(days, ['День', 'Дня', 'Дней']);
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$6);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes).replace('.', ',');
      /*
       * It's weird to say like this but correct pronounce is:
       * 1,2  = '1 целая две десятых часа'
       * 0,1  = '1 десятая часа'
       * 0,06 = '6 сотых часа'
       * 2,05 = '2 целых пять сотых часа'
       * 3,12 = '3 целых двенадцать сотых часа'
       * 4,29 = '4 целых 29 сотых часа'
       */
      return hoursAndMinutes + ' Часа';
    }.bind(undefined),
    learRangeLabel: 'Очистить диапазон',
    clearDateLabel: 'Очистить дату',
    shortDateFormat: 'j.n.Y',
    weekDateFormat: 'D j.n.Y',
    dateTimeFormat: 'D j.n.Y G:i',
    firstWeekday: DateTime.MONDAY,
    holidays: {}
  };

  var _this$5 = undefined;
  var SV = {
    id: 'SV',
    monthNames: ['Januari', 'Februari', 'Mars', 'April', 'Maj', 'Juni', 'Juli', 'Augusti', 'September', 'Oktober', 'November', 'December'],
    dayNames: ['Söndag', 'Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag'],
    shortDayNames: ['Sö', 'Må', 'Ti', 'On', 'To', 'Fr', 'Lö'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$5);
      return years + ' ' + (years === 1 ? 'År' : 'År');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$5);
      return months + ' ' + (months === 1 ? 'Månad' : 'Månader');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$5);
      return days + ' ' + (days === 1 ? 'Dag' : 'Dagar');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$5);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes).replace('.', ',');
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'Minut' : 'Minuter');
    }.bind(undefined),
    clearRangeLabel: 'TODO',
    clearDateLabel: 'TODO',
    shortDateFormat: 'j.n.Y',
    weekDateFormat: 'D j.n.Y',
    dateTimeFormat: 'D j.n.Y G:i',
    firstWeekday: DateTime.MONDAY,
    holidays: {}
  };

  var _this$4 = undefined;
  var LV = {
    id: 'LV',
    monthNames: ['Janvāris', 'Februāris', 'Marts', 'Aprīlis', 'Maijs', 'Jūnijs', ' Jūlijs', 'Augusts', 'Septembris', 'Oktobris', 'Novembris', 'Decembris'],
    dayNames: ['Svētdiena', 'Pirmdiena', 'Otrdiena', 'Trešdiena', 'Ceturtdiena', 'Piektdiena', 'Sestdiena'],
    shortDayNames: ['Sv', 'P', 'O', 'T', 'C', 'Pk', 'S'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$4);
      return years + ' ' + (years === 1 ? 'G' : 'G');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$4);
      return months + ' ' + (months === 1 ? 'Mēnesī' : 'Mēnešiem');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$4);
      return days + ' ' + (days === 1 ? 'Diena' : 'Dienas');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$4);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes);
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'Stundas' : 'Minūtes');
    }.bind(undefined),
    clearRangeLabel: 'TODO',
    clearDateLabel: 'TODO',
    shortDateFormat: 'j.n.Y',
    weekDateFormat: 'D j.n.Y',
    dateTimeFormat: 'D j.n.Y G:i',
    firstWeekday: DateTime.MONDAY,
    holidays: {}
  };

  var _this$3 = undefined;
  var FR = {
    id: 'FR',
    monthNames: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
    dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
    shortDayNames: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$3);
      return years + ' ' + (years === 1 ? 'Année' : 'Années');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$3);
      return months + ' ' + (months === 1 ? 'Mois' : 'Moiss');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$3);
      return days + ' ' + (days === 1 ? 'Jour' : 'Jours');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$3);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes);
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'Heure' : 'Heures');
    }.bind(undefined),
    clearRangeLabel: 'Effacer la sélection',
    clearDateLabel: 'Effacer la date',
    shortDateFormat: 'j/n/Y',
    weekDateFormat: 'D j/n/Y',
    dateTimeFormat: 'D j/n/Y G:i',
    firstWeekday: DateTime.MONDAY,
    holidays: {}
  };

  var _this$2 = undefined;
  var DE = {
    id: 'DE',
    monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
    dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
    shortDayNames: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$2);
      return years + ' ' + 'Jahr';
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$2);
      return months + ' ' + (months === 1 ? 'Monat' : 'Months');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$2);
      return days + ' ' + (days === 1 ? 'Tag' : 'Tage');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$2);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes);
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? 'Stunde' : 'Stunden');
    }.bind(undefined),
    clearRangeLabel: 'Auswahl löschen',
    clearDateLabel: 'Auswahl löschen',
    shortDateFormat: 'j.n.Y',
    weekDateFormat: 'D j.n.Y',
    dateTimeFormat: 'D j.n.Y G:i',
    firstWeekday: DateTime.MONDAY,
    holidays: {}
  };

  var _this$1 = undefined;
  var CN = {
    id: 'AU',
    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
    dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
    shortDayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
    yearsLabel: function yearsLabel(years) {
      _newArrowCheck(this, _this$1);
      return years + ' ' + (years === 1 ? '年' : '年');
    }.bind(undefined),
    monthsLabel: function monthsLabel(months) {
      _newArrowCheck(this, _this$1);
      return months + ' ' + (months === 1 ? '月' : '月');
    }.bind(undefined),
    daysLabel: function daysLabel(days) {
      _newArrowCheck(this, _this$1);
      return days + ' ' + (days === 1 ? '日' : '日');
    }.bind(undefined),
    hoursLabel: function hoursLabel(hours, minutes) {
      _newArrowCheck(this, _this$1);
      var hoursAndMinutes = DateFormat.hoursAndMinutes(hours, minutes);
      return hoursAndMinutes + ' ' + (+hoursAndMinutes === 1 ? '小时' : '小时');
    }.bind(undefined),
    clearRangeLabel: '范围',
    clearDateLabel: '日期',
    shortDateFormat: 'Y年m月d日',
    weekDateFormat: 'Y年m月d日 D',
    dateTimeFormat: 'Y年m月d日 H时i分s秒',
    firstWeekday: DateTime.MONDAY,
    holidays: {}
  };

  var DateLocale$1 = {
    FI: FI,
    EN: EN,
    AU: AU,
    ET: ET,
    RU: RU$1,
    SV: SV,
    LV: LV,
    FR: FR,
    DE: DE,
    CN: CN
  };

  var DateParse = /*#__PURE__*/function () {
    function DateParse() {
      _classCallCheck(this, DateParse);
    }
    return _createClass(DateParse, null, [{
      key: "parseDate",
      value: function parseDate(input, format) {
        var values = input.match(getOrCreateParseRegexp());
        return values ? matchesToDateTime(values) : null;
        function matchesToDateTime(values) {
          var day = matchesToObject(values);
          return DateTime.fromDate(day.Y, day.m ? day.m : day.n, day.d ? day.d : day.j);
        }
        function matchesToObject(matchValues) {
          var day = {};
          var keys = format.replace(/[^djmnY]/g, '').split('');
          for (var i = 0; i < keys.length; i++) day[keys[i]] = +matchValues[i + 1];
          return day;
        }
        function getOrCreateParseRegexp() {
          if (DateParse.parseRegexes[format] === undefined) {
            DateParse.parseRegexes[format] = new RegExp(format.replace(/[djmnY]/g, '(\\d+)').replace(/\./g, '\\.'));
          }
          return DateParse.parseRegexes[format];
        }
      }
    }, {
      key: "parseTime",
      value: function parseTime(timeStr) {
        var splittedTime = splitTime(timeStr.replace(/:|,/i, '.'));
        var time = [+splittedTime[0], +splittedTime[1]];
        return isHour(time[0]) && isMinute(time[1]) ? time : null;
        function splitTime(timeStr) {
          if (timeStr.indexOf('.') !== -1) {
            return timeStr.split('.');
          }
          var splitTimes = {
            4: [timeStr.slice(0, 2), timeStr.slice(2, 4)],
            3: [timeStr.slice(0, 1), timeStr.slice(1, 3)],
            2: [timeStr, 0]
          };
          return splitTimes[timeStr.length] || [-1, -1];
        }
        function isMinute(minutes) {
          return !isNaN(minutes) && minutes >= 0 && minutes <= 59;
        }
        function isHour(hours) {
          return !isNaN(hours) && hours >= 0 && hours <= 23;
        }
      }
    }, {
      key: "parse",
      value: function parse(input, locale) {
        if (typeof input === 'string') {
          if (input === 'today') {
            return DateTime.today();
          }
          var format = locale ? locale.shortDateFormat : DateParse.defaultFormat;
          var date = DateParse.parseDate(input, format);
          return date ? date : new DateTime(new Date(input));
        }
        throw new Error("DateParse only accepts strings");
      }
    }]);
  }();
  DateParse.parseRegexes = [];
  DateParse.defaultFormat = 'n/j/Y';

  var _this = undefined;
  var Duration = function Duration(durationMs) {
    this.durationMs = durationMs;
  };
  Duration.MS = 1;
  Duration.SECOND = 1000;
  Duration.MIN = 60 * Duration.SECOND;
  Duration.HOUR = 60 * Duration.MIN;
  Duration.DAY = 24 * Duration.HOUR;
  Duration.fromMS = function (milliSeconds) {
    _newArrowCheck(this, _this);
    return new Duration(milliSeconds);
  }.bind(undefined);
  Duration.fromSeconds = function (seconds) {
    _newArrowCheck(this, _this);
    return Duration.fromMS(seconds * Duration.SECOND);
  }.bind(undefined);
  Duration.fromMinutes = function (minutes) {
    _newArrowCheck(this, _this);
    return Duration.fromMS(minutes * Duration.MIN);
  }.bind(undefined);
  Duration.fromHours = function (hours) {
    _newArrowCheck(this, _this);
    return Duration.fromMS(hours * Duration.HOUR);
  }.bind(undefined);
  Duration.fromDays = function (days) {
    _newArrowCheck(this, _this);
    return Duration.fromMS(days * Duration.DAY);
  }.bind(undefined);
  Duration.fromIsoTime = function (isoTime) {
    _newArrowCheck(this, _this);
    var parts = isoTime.split(':').map(Number);
    var hour = parts[0];
    var minutes = parts[1];
    var seconds = parts[2];
    var milliseconds = parts[3] || 0;
    return Duration.fromMS(hour * Duration.HOUR + minutes * Duration.MIN + seconds * Duration.SECOND + milliseconds);
  }.bind(undefined);
  Duration.prototype.toMS = function () {
    return this.durationMs;
  };
  Duration.prototype.asUnit = function (unit) {
    return Number(this.durationMs / unit);
  };

  RU$1.shortDateFormat = 'd.m.Y';
  RU$1.weekDateFormat = 'D d.m.Y';
  RU$1.dateTimeFormat = 'D d.m.Y G:i';
  var RU = RU$1;

  DateLocale$1.RU = RU;
  var DateLocale = Object.assign({}, DateLocale$1);

  function _callSuper$B(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$B() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$B() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$B = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$l(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var CALENDAR_NO_ICON_AN = "no-icon";
  var SkDatePicker = /*#__PURE__*/function (_SkElement) {
    function SkDatePicker() {
      _classCallCheck(this, SkDatePicker);
      return _callSuper$B(this, SkDatePicker, arguments);
    }
    _inherits(SkDatePicker, _SkElement);
    return _createClass(SkDatePicker, [{
      key: "cnSuffix",
      get: function get() {
        return 'datepicker';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "fmt",
      get: function get() {
        return this.getAttribute('fmt') || this.dateLocale ? this.dateLocale.shortDateFormat : 'm/d/Y';
      }
    }, {
      key: "dateLocale",
      get: function get() {
        if (!this._lang) {
          this._lang = this.lang ? this.localeByCode(this.lang) : DateLocale.EN;
        }
        return this._lang;
      }
    }, {
      key: "value",
      get: function get() {
        return this.getAttribute('value');
      },
      set: function set(value) {
        if (value === null) {
          this.removeAttribute('value');
        } else {
          this.setAttribute('value', value);
        }
      }
    }, {
      key: "localeByCode",
      value: function localeByCode(langCode) {
        return DateLocale[LANGS_BY_CODES[langCode]];
      }
    }, {
      key: "validity",
      get: function get() {
        return this.inputEl ? this.inputEl.validity : false;
      }
    }, {
      key: "validationMessage",
      get: function get() {
        return this.inputEl && this.inputEl.validationMessage && this.inputEl.validationMessage !== 'undefined' ? this.inputEl.validationMessage : this.locale.tr('Field value is invalid');
      },
      set: function set(validationMessage) {
        this._validationMessage = validationMessage;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['inputEl'];
      }
    }, {
      key: "inputEl",
      get: function get() {
        if (!this._inputEl && this.el) {
          this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
      },
      set: function set(el) {
        this._inputEl = el;
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (!this.inputEl) {
          return false;
        }
        if (DATEPICKER_FORWARDED_ATTRS[name] && oldValue !== newValue) {
          if (newValue !== null && newValue !== undefined) {
            this.inputEl.setAttribute(name, newValue);
          } else {
            this.inputEl.removeAttribute(name);
          }
        }
        if (name === 'form-invalid') {
          if (newValue !== null) {
            this.impl.showInvalid();
          } else {
            this.impl.showValid();
          }
        }
        if (name === 'value') {
          this.dispatchEvent(new CustomEvent('skchange', {
            target: this,
            detail: {
              value: this.getAttribute('value'),
              checked: this.getAttribute('checked')
            },
            bubbles: true,
            composed: true
          }));
          this.dispatchEvent(new CustomEvent('change', {
            target: this,
            detail: {
              value: this.getAttribute('value'),
              checked: this.getAttribute('checked')
            },
            bubbles: true,
            composed: true
          }));
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }, {
      key: "setCustomValidity",
      value: function setCustomValidity(validity) {
        _superPropGet$l(SkDatePicker, "setCustomValidity", this, 3)([validity]);
        this.inputEl.setCustomValidity(validity);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['form-invalid'].concat(_toConsumableArray(Object.keys(DATEPICKER_FORWARDED_ATTRS)));
      }
    }]);
  }(SkElement);

  function _callSuper$A(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$A() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$A() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$A = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$k(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkDatepicker = /*#__PURE__*/function (_SkDatepickerImpl) {
    function JquerySkDatepicker() {
      _classCallCheck(this, JquerySkDatepicker);
      return _callSuper$A(this, JquerySkDatepicker, arguments);
    }
    _inherits(JquerySkDatepicker, _SkDatepickerImpl);
    return _createClass(JquerySkDatepicker, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'datepicker';
      }
    }, {
      key: "input",
      get: function get() {
        if (!this._input) {
          this._input = this.comp.el.querySelector('input');
        }
        return this._input;
      },
      set: function set(el) {
        this._input = el;
      }
    }, {
      key: "picker",
      get: function get() {
        if (!this._picker) {
          this._picker = this.comp.el.querySelector('input');
        }
        return this._picker;
      },
      set: function set(el) {
        this._picker = el;
      }
    }, {
      key: "pickerContainer",
      get: function get() {
        if (!this._pickerContainer) {
          this._pickerContainer = this.comp.el.querySelector('#ui-datepicker-div');
        }
        return this._pickerContainer;
      },
      set: function set(el) {
        this._pickerContainer = el;
      }
    }, {
      key: "calendarTBody",
      get: function get() {
        if (!this._calendarTBody) {
          this._calendarTBody = this.comp.el.querySelector('table.ui-datepicker-calendar tbody');
        }
        return this._calendarTBody;
      },
      set: function set(el) {
        this._calendarTBody = el;
      }
    }, {
      key: "calendarTHead",
      get: function get() {
        if (!this._calendarTHead) {
          this._calendarTHead = this.comp.el.querySelector('table.ui-datepicker-calendar thead');
        }
        return this._calendarTHead;
      },
      set: function set(el) {
        this._calendarTHead = el;
      }
    }, {
      key: "curMonthLabel",
      get: function get() {
        if (!this._curMonthLabel) {
          this._curMonthLabel = this.comp.el.querySelector('.ui-datepicker-month');
        }
        return this._curMonthLabel;
      },
      set: function set(el) {
        this._curMonthLabel = el;
      }
    }, {
      key: "curYearLabel",
      get: function get() {
        if (!this._curYearLabel) {
          this._curYearLabel = this.comp.el.querySelector('.ui-datepicker-year');
        }
        return this._curYearLabel;
      },
      set: function set(el) {
        this._curYearLabel = el;
      }
    }, {
      key: "prevMonthBtn",
      get: function get() {
        if (!this._prevMonthBtn) {
          this._prevMonthBtn = this.comp.el.querySelector('.ui-datepicker-prev');
        }
        return this._prevMonthBtn;
      },
      set: function set(el) {
        this._prevMonthBtn = el;
      }
    }, {
      key: "selectMonthBtn",
      get: function get() {
        if (!this._selectMonthBtn) {
          this._selectMonthBtn = this.comp.el.querySelector('.ui-datepicker-month');
        }
        return this._selectMonthBtn;
      },
      set: function set(el) {
        this._selectMonthBtn = el;
      }
    }, {
      key: "selectYearBtn",
      get: function get() {
        if (!this._selectYearBtn) {
          this._selectYearBtn = this.comp.el.querySelector('.ui-datepicker-year');
        }
        return this._selectYearBtn;
      },
      set: function set(el) {
        this._selectYearBtn = el;
      }
    }, {
      key: "nextMonthBtn",
      get: function get() {
        if (!this._nextMonthBtn) {
          this._nextMonthBtn = this.comp.el.querySelector('.ui-datepicker-next');
        }
        return this._nextMonthBtn;
      }
    }, {
      key: "calendarInput",
      get: function get() {
        if (!this._calendarInput) {
          this._calendarInput = this.comp.el.querySelector('input');
        }
        return this._calendarInput;
      },
      set: function set(el) {
        this._calendarInput = el;
      }
    }, {
      key: "currentYear",
      get: function get() {
        return this._currentYear || DateTime.today().getFullYear();
      },
      set: function set(el) {
        this._currentYear = el;
      }
    }, {
      key: "currentMonth",
      get: function get() {
        return this._currentMonth || DateTime.today().getMonth();
      },
      set: function set(el) {
        this._currentMonth = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['input', 'picker', 'pickerContainer', 'calendarTBody', 'calendarTHead', 'curMonthLabel', 'curYearLabel', 'prevMonthBtn', 'selectMonthBtn', 'selectYearBtn', 'calendarInput', 'currentYear', 'currentMonth'];
      }
    }, {
      key: "renderImpl",
      value: function renderImpl() {
        var _this = this;
        var themeLoaded = this.initTheme();
        themeLoaded["finally"](function () {
          var _this2 = this;
          _newArrowCheck(this, _this);
          // we just need to try loading styles
          this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
          if (!this.comp.tpl) {
            var loadTpl = /*#__PURE__*/function () {
              var _ref = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
                return regenerator.wrap(function _callee$(_context) {
                  while (1) switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return _this2.comp.renderer.mountTemplate(_this2.tplPath, _this2.comp.constructor.name + 'Tpl', _this2.comp, {
                        themePath: _this2.themePath
                      });
                    case 2:
                      _this2.comp.tpl = _context.sent;
                    case 3:
                    case "end":
                      return _context.stop();
                  }
                }, _callee);
              }));
              return function loadTpl() {
                return _ref.apply(this, arguments);
              };
            }();
            loadTpl().then(function () {
              _newArrowCheck(this, _this2);
              this.doRender();
            }.bind(this));
          } else {
            this.doRender();
          }
        }.bind(this));
      }
    }, {
      key: "updateInputValue",
      value: function updateInputValue() {
        this.calendarInput.value = this.comp.value;
        this.comp.dispatchEvent(new CustomEvent('input', {
          target: this,
          bubbles: true,
          composite: true,
          detail: {}
        }));
        this.comp.dispatchEvent(new CustomEvent('skinput', {
          target: this,
          bubbles: true,
          composite: true,
          detail: {}
        }));
      }
    }, {
      key: "renderPickerLabel",
      value: function renderPickerLabel() {
        this.curMonthLabel.innerHTML = DateFormat.format(DateTime.fromDate(this.currentYear, this.currentMonth, 1), 'F', this.comp.dateLocale);
        this.curMonthLabel.setAttribute('ref', this.currentMonth);
        this.curYearLabel.innerHTML = this.currentYear;
      }
    }, {
      key: "renderDays",
      value: function renderDays() {
        this.calendarTBody.innerHTML = '';
        var firstMonthDay = DateTime.fromDate(this.currentYear, this.currentMonth, 1);
        var firstWeekDay = firstMonthDay.getFirstDateOfWeek(this.comp.dateLocale);
        for (var w = 0; w <= 5; w++) {
          // weeks as displayed in picker
          var weekRow = this.comp.renderer.createEl('tr');
          weekRow.setAttribute('role', 'row');
          for (var i = 1; i <= 7; i++) {
            var dayOffset = w * 7 + i - 1;
            var day = firstWeekDay.plusDays(dayOffset);
            var dayCell = day.getMonth() === firstMonthDay.getMonth() ? "\n                        <td class=\"calendar-date \" data-handler=\"selectDay\" data-event=\"click\" data-month=\"".concat(day.getMonth(), "\" data-year=\"").concat(day.getFullYear(), "\" data-day=\"").concat(day.getDate(), "\" >\n                            <a class=\"ui-state-default\">").concat(DateFormat.format(day, 'j', this.comp.dateLocale), "</a>\n                        </td>\n                    ") : "<td class=\" ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled\">&nbsp;</td>";
            weekRow.insertAdjacentHTML('beforeend', dayCell);
          }
          this.calendarTBody.appendChild(weekRow);
        }
      }
    }, {
      key: "renderWeekDaysHeading",
      value: function renderWeekDaysHeading() {
        var wdRow = this.comp.renderer.createEl('tr');
        wdRow.setAttribute('role', 'row');
        var firstWdDay = DateTime.today().getFirstDateOfWeek(this.comp.dateLocale);
        for (var wd = 0; wd <= 6; wd++) {
          // weekday code heading
          var weekDay = firstWdDay.plusDays(wd);
          var wdCell = "\n                    <th scope=\"col\"><span title=\"".concat(DateFormat.format(weekDay, 'D', this.comp.dateLocale), "\"</span></th>\n                ");
          wdRow.insertAdjacentHTML('beforeend', wdCell);
        }
        this.calendarTHead.appendChild(wdRow);
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        var _this3 = this;
        _superPropGet$k(JquerySkDatepicker, "afterRendered", this, 3)([]);
        this.restoreState();
        this.pickerContainer.style.display = 'none';
        this.picker.addEventListener('focusin', function (event) {
          _newArrowCheck(this, _this3);
          this.showPicker();
        }.bind(this));
        this.pickerContainer.addEventListener('focusin', function (event) {
          _newArrowCheck(this, _this3);
          this.showPicker();
        }.bind(this));
        this.pickerContainer.addEventListener('focusout', function (event) {
          _newArrowCheck(this, _this3);
          this.closePicker();
        }.bind(this));
        this.onScrollHandler = this.fitPicker.bind(this);
        window.addEventListener('scroll', this.onScrollHandler);
        this.renderPickerIcon();
        this.mountStyles();
      }
    }, {
      key: "renderPickerIcon",
      value: function renderPickerIcon() {
        if (!this.comp.hasAttribute(CALENDAR_NO_ICON_AN)) {
          this.picker.insertAdjacentHTML('afterend', "<img class=\"ui-datepicker-trigger\" src=\"".concat(this.themePath, "/images/calendar.gif\" alt=\"Select date\" title=\"Select date\" />"));
        }
      }
    }, {
      key: "closePicker",
      value: function closePicker() {
        this.pickerContainer.style.display = 'none';
        this.comp.removeAttribute('open');
      }
    }, {
      key: "showPicker",
      value: function showPicker() {
        this.fitPicker(true);
        this.pickerContainer.style.display = 'block';
        this.comp.setAttribute('open', '');
      }
    }, {
      key: "fitPicker",
      value: function fitPicker(force) {
        if (force || this.comp.hasAttribute('open')) {
          var box = this.picker.getBoundingClientRect();
          this.pickerContainer.style.position = 'fixed';
          this.pickerContainer.style.top = "".concat(window.pageYOffset + box.bottom - this.getScrollTop(), "px");
          this.pickerContainer.style.left = "".concat(box.left, "px");
        }
      }
    }, {
      key: "selectDay",
      value: function selectDay(dayValue) {
        var day = DateTime.fromDate(this.currentYear, this.currentMonth, dayValue);
        this.comp.value = DateFormat.format(day, this.comp.fmt, this.dateLocale);
        this.updateInputValue();
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        var _this4 = this;
        _superPropGet$k(JquerySkDatepicker, "bindEvents", this, 3)([]);
        this.comp.el.addEventListener('click', function (event) {
          _newArrowCheck(this, _this4);
          var targetEl = event.target;
          var currentDate = DateTime.fromDate(this.currentYear, this.currentMonth, 1);
          if (targetEl) {
            if (targetEl.parentElement && targetEl.parentElement.classList.contains('calendar-date')) {
              var dayValue = parseInt(event.target.innerText);
              this.selectDay(dayValue);
              this.closePicker();
            } else if (targetEl === this.selectMonthBtn) {
              this.currentMonth = parseInt(event.target.getAttribute('ref'));
              var date = DateParse.parseDate(this.input.value, this.comp.fmt);
              this.selectDay(date.getDay());
              this.closePicker();
            } else if (targetEl === this.selectYearBtn) {
              this.currentYear = parseInt(event.target.innerText);
              var _date = DateParse.parseDate(this.input.value, this.comp.fmt);
              this.selectDay(_date.getDay());
              this.closePicker();
            } else if (targetEl.parentElement && targetEl.parentElement === this.prevMonthBtn) {
              if (this.currentMonth > 1 && this.currentMonth <= 12) {
                currentDate = DateTime.fromDate(this.currentYear, this.currentMonth - 1, 1);
              } else {
                if (this.currentMonth <= 1) {
                  currentDate = DateTime.fromDate(this.currentYear - 1, 12, 1);
                }
              }
            } else if (targetEl.parentElement && targetEl.parentElement === this.nextMonthBtn) {
              if (this.currentMonth >= 1 && this.currentMonth < 12) {
                currentDate = DateTime.fromDate(this.currentYear, this.currentMonth + 1, 1);
              } else {
                if (this.currentMonth >= 12) {
                  currentDate = DateTime.fromDate(this.currentYear + 1, 1, 1);
                }
              }
            } else if (targetEl.parentElement && targetEl.parentElement === this.prevYearBtn) {
              currentDate = DateTime.fromDate(this.currentYear - 1, this.currentMonth, 1);
            } else if (targetEl.parentElement && targetEl.parentElement === this.nextYearBtn) {
              currentDate = DateTime.fromDate(this.currentYear + 1, this.currentMonth, 1);
            } else if (targetEl.parentElement && targetEl.parentElement === this.todayBtn) {
              var now = DateTime.today();
              this.currentYear = now.getFullYear();
              this.currentMonth = now.getMonth();
              this.selectDay(now.getDate());
              this.closePicker();
            } else if (targetEl.classList.contains('ui-datepicker-trigger')) {
              if (this.comp.hasAttribute('open')) {
                this.closePicker();
              } else {
                this.showPicker();
              }
            }
            this.currentYear = currentDate.getFullYear();
            this.currentMonth = currentDate.getMonth();
            this.renderPickerLabel();
            this.renderDays();
          }
        }.bind(this));
      }
    }]);
  }(SkDatepickerImpl);

  function _createForOfIteratorHelper$c(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$c(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$c(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$c(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$c(r, a) : void 0; } }
  function _arrayLikeToArray$c(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$z(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$z() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$z() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$z = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$j(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkDialogImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkDialogImpl() {
      _classCallCheck(this, SkDialogImpl);
      return _callSuper$z(this, SkDialogImpl, arguments);
    }
    _inherits(SkDialogImpl, _SkComponentImpl);
    return _createClass(SkDialogImpl, [{
      key: "suffix",
      get: function get() {
        return 'dialog';
      }
    }, {
      key: "renderTitle",
      value: function renderTitle() {
        if (this.comp.title) {
          this.headerEl.style.display = 'block';
          this.titleEl.innerHTML = this.comp.title;
        } else {
          this.titleEl.style.display = 'none';
        }
      }
    }, {
      key: "dumpState",
      value: function dumpState() {
        return {
          'contents': this.comp.contentsState
        };
      }
    }, {
      key: "unmountStyles",
      value: function unmountStyles() {
        _superPropGet$j(SkDialogImpl, "unmountStyles", this, 3)([]);
        if (this.styles && this.styles.length > 0) {
          var _iterator = _createForOfIteratorHelper$c(this.styles),
            _step;
          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var style = _step.value;
              var path = this.comp.configEl.styles[style];
              var link = document.body.querySelector("link[href=\"".concat(path, "\"]"));
              if (link !== null) {
                document.body.removeChild(link);
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
      }
    }, {
      key: "cleanFooterTplCache",
      value: function cleanFooterTplCache() {
        var oldTpl = document.getElementById(this.comp.constructor.name + 'FooterTpl');
        if (oldTpl !== null) {
          if (typeof oldTpl.remove === 'function') {
            oldTpl.remove();
          } else {
            if (oldTpl.parentElement) {
              oldTpl.parentElement.removeChild(oldTpl);
            }
          }
        }
      }
    }, {
      key: "renderFooter",
      value: function () {
        var _renderFooter = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
          var _this = this;
          var renderFooterContents, tpl, tplLoaded;
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                this.cleanFooterTplCache();
                renderFooterContents = function (tpl) {
                  var el;
                  if (tpl.content) {
                    el = this.comp.renderer.prepareTemplate(tpl);
                  } else {
                    el = tpl.firstElementChild;
                  }
                  this.footerEl.innerHTML = '';
                  if (this.isInIE()) {
                    this.footerEl.appendChild(el);
                  } else {
                    this.footerEl.append(el);
                  }
                  this.bindActions(this.footerEl);
                }.bind(this);
                tpl = this.comp.renderer.findTemplateEl(this.comp.constructor.name + 'FooterTpl', this.dialog); // try to find overriden template
                if (!tpl) {
                  tplLoaded = this.comp.renderer.mountTemplate(this.tplFooterPath, this.comp.constructor.name + 'FooterTpl', this.comp, {
                    Ok: this.comp.locale.tr('Ok'),
                    Cancel: this.comp.locale.tr('Cancel')
                  });
                  tplLoaded.then(function (tpl) {
                    _newArrowCheck(this, _this);
                    renderFooterContents(tpl);
                  }.bind(this));
                } else {
                  renderFooterContents(tpl);
                }
              case 4:
              case "end":
                return _context.stop();
            }
          }, _callee, this);
        }));
        function renderFooter() {
          return _renderFooter.apply(this, arguments);
        }
        return renderFooter;
      }()
    }, {
      key: "reattachStylesToBody",
      value: function reattachStylesToBody() {
        var styles = Object.keys(this.skTheme.styles);
        if (styles) {
          var _iterator2 = _createForOfIteratorHelper$c(styles),
            _step2;
          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var style = _step2.value;
              this.attachStyleByPath(this.skTheme.styles[style], document.body);
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }
        }
      }
    }, {
      key: "remountToBody",
      value: function remountToBody(dialog) {
        this.reattachStylesToBody();
        if (this.isInIE()) {
          this.dialog = document.body.appendChild(dialog);
          this.comp.bindAutoRender(this.dialog);
          this.comp.setupConnections();
          this.bindActions(this.dialog);
        } else {
          document.body.append(dialog);
          this.dialog = dialog;
        }
      }
    }, {
      key: "polyfillNativeDialog",
      value: function polyfillNativeDialog(dialog) {
        if (this.comp.dialogTn === 'dialog') {
          if (typeof this.dialog.showModal !== "function") {
            if (!window.dialogPolyfill) {
              this.logger.warn('you trying to polyfill native dialog element, but polyfill was not loaded');
              return false;
            }
            this.remountToBody(dialog);
            window.dialogPolyfill.registerDialog(this.dialog);
            this.dialogPolyfilled = true;
          }
        } else {
          this.comp.style.position = 'fixed';
          this.comp.style.zIndex = 500;
        }
        if (this.comp.hasAttribute('to-body')) {
          this.remountToBody(this.dialog);
        }
      }
    }, {
      key: "getScrollTop",
      value: function getScrollTop() {
        if (typeof pageYOffset !== 'undefined') {
          return pageYOffset;
        } else {
          var body = _document.body;
          var _document = _document.documentElement;
          _document = _document.clientHeight ? _document : body;
          return _document.scrollTop;
        }
      }
    }, {
      key: "fixDialogPosition",
      value: function fixDialogPosition() {
        var box = this.dialog.getBoundingClientRect();
        var winWidth = document.body.clientWidth || document.clientWidth;
        var winHeight = document.body.clientHeight || document.clientHeight;
        this.dialog.style.position = 'fixed';
        var dialogLeft = this.dialog.style.right = (winWidth - box.width) / 2;
        dialogLeft = dialogLeft > 0 ? dialogLeft : 0;
        this.dialog.style.left = dialogLeft.toString() + 'px';
        var dialogTop = (winHeight - box.height) / 2;
        dialogTop = dialogTop > 0 ? dialogTop : 0;
        this.dialog.style.top = dialogTop.toString() + 'px';
        this.dialog.style.margin = 0;
        this.dialog.style.padding = 0;
      }
    }, {
      key: "open",
      value: function open() {
        this.dialog.style.setProperty('display', 'block', 'important');
        if (typeof this.dialog.showModal === "function") {
          if (!this.dialog.hasAttribute('open')) {
            this.dialog.showModal();
          } else {
            this.logger.warn('trying to open dialog with open attr');
          }
        } else {
          this.logger.warn('native dialog not supported by this browser');
        }
        if (this.isInIE() && !this.dialog.hasAttribute('open')) {
          // assume dialog can be not only native like
          this.comp.setAttribute('open', 'true');
        } else {
          this.comp.setAttribute('open', '');
        }
        if (this.comp.fixPosHandler) {
          this.dialog.removeEventListener(SK_RENDER_EVT, this.fixPosHandler);
        }
        this.comp.fixPosHandler = function (event) {
          this.fixDialogPosition();
        }.bind(this);
        this.dialog.addEventListener(SK_RENDER_EVT, this.comp.fixPosHandler);
        if (this.comp.resizeHandler) {
          window.removeEventListener(SK_RENDER_EVT, this.comp.fixPosHandler);
        }
        this.comp.resizeHandler = function (event) {
          this.fixDialogPosition();
        }.bind(this);
        window.addEventListener('resize', this.comp.resizeHandler);
        if (this.comp.dialogTn !== 'dialog') {
          this.fixDialogPosition();
        }
      }
    }, {
      key: "close",
      value: function close() {
        this.dialog.style.setProperty('display', 'none', 'important');
        if (typeof this.dialog.close === "function" && this.dialog.hasAttribute('open')) {
          this.dialog.close();
        } else {
          this.logger.warn('native dialog not supported by this browser');
        }
        if (this.comp.hasAttribute('open')) {
          this.comp.removeAttribute('open');
        }
      }
    }, {
      key: "doRender",
      value: function doRender() {
        var id = this.getOrGenId();
        this.beforeRendered();
        var el = this.comp.renderer.prepareTemplate(this.comp.tpl);
        var dialog = this.comp.renderer.createEl(this.comp.dialogTn);
        dialog.setAttribute('id', id + 'Dialog');
        var rendered;
        if (this.comp.renderer.variableRender && this.comp.renderer.variableRender !== 'false') {
          rendered = this.comp.renderer.renderMustacheVars(el, {
            id: id,
            themePath: this.themePath
          });
          dialog.insertAdjacentHTML('beforeend', rendered);
        } else {
          dialog.innerHTML = '';
          dialog.appendChild(el);
        }
        var _iterator3 = _createForOfIteratorHelper$c(this.tplClones),
          _step3;
        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var template = _step3.value;
            dialog.appendChild(template);
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }
        if (this.comp.hasAttribute('to-body')) {
          this.dialog = document.body.appendChild(dialog);
        } else {
          this.comp.el.innerHTML = '';
          this.dialog = this.comp.el.appendChild(dialog);
          this.indexMountedStyles();
        }
        this.afterRendered();
        if (this.comp.hasAttribute('open') && !this.comp.implRenderTimest) {
          this.open();
        }
        this.comp.dispatchEvent(new CustomEvent('skafterrendered'));
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', {
          bubbles: true,
          composed: true
        }));
        this.comp.dispatchEvent(new SkRenderEvent({
          bubbles: true,
          composed: true
        }));
        if (this.comp.renderDeferred) {
          this.comp.renderDeferred.resolve(this.comp);
        }
      }
    }, {
      key: "renderImpl",
      value: function renderImpl() {
        var _this2 = this;
        var ownTemplates = this.comp.querySelectorAll('template');
        this.tplClones = [];
        var _iterator4 = _createForOfIteratorHelper$c(ownTemplates),
          _step4;
        try {
          for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
            var template = _step4.value;
            this.tplClones.push(template.cloneNode(true));
          }
        } catch (err) {
          _iterator4.e(err);
        } finally {
          _iterator4.f();
        }
        var themeLoaded = this.initTheme();
        themeLoaded["finally"](function () {
          var _this3 = this;
          _newArrowCheck(this, _this2);
          // we just need to try loading styles
          this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
          if (!this.comp.tpl) {
            var loadTpl = /*#__PURE__*/function () {
              var _ref = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2() {
                return regenerator.wrap(function _callee2$(_context2) {
                  while (1) switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return _this3.comp.renderer.mountTemplate(_this3.tplPath, _this3.cachedTplId, _this3.comp, {
                        themePath: _this3.themePath
                      });
                    case 2:
                      _this3.comp.tpl = _context2.sent;
                    case 3:
                    case "end":
                      return _context2.stop();
                  }
                }, _callee2);
              }));
              return function loadTpl() {
                return _ref.apply(this, arguments);
              };
            }();
            loadTpl().then(function () {
              _newArrowCheck(this, _this3);
              this.doRender();
            }.bind(this));
          } else {
            this.doRender();
          }
        }.bind(this));
      }
    }, {
      key: "setTitle",
      value: function setTitle(title) {
        this.titleEl.innerHTML = title;
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$y(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$y() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$y() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$y = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$i(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkDialog = /*#__PURE__*/function (_SkDialogImpl) {
    function JquerySkDialog() {
      _classCallCheck(this, JquerySkDialog);
      return _callSuper$y(this, JquerySkDialog, arguments);
    }
    _inherits(JquerySkDialog, _SkDialogImpl);
    return _createClass(JquerySkDialog, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'dialog';
      }
    }, {
      key: "tplFooterPath",
      get: function get() {
        if (!this._tplFooterPath) {
          this._tplFooterPath = this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path') ? "".concat(this.comp.configEl.getAttribute('tpl-path'), "/").concat(this.prefix, "-sk-").concat(this.suffix, "-footer.tpl.html") : "/node_modules/sk-".concat(this.comp.cnSuffix, "-").concat(this.prefix, "/src/").concat(this.prefix, "-sk-").concat(this.suffix, "-footer.tpl.html");
        }
        return this._tplFooterPath;
      }
    }, {
      key: "dialog",
      get: function get() {
        if (!this._dialog) {
          this._dialog = this.comp.el.querySelector(this.comp.dialogTn);
        }
        return this._dialog;
      },
      set: function set(dialog) {
        this._dialog = dialog;
      }
    }, {
      key: "body",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-content');
      }
    }, {
      key: "closeBtn",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-titlebar-close');
      }
    }, {
      key: "footerEl",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-footer');
      }
    }, {
      key: "headerEl",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-titlebar');
      }
    }, {
      key: "titleEl",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-title');
      }
    }, {
      key: "btnCancelEl",
      get: function get() {
        return this.dialog.querySelector('.btn-cancel');
      }
    }, {
      key: "btnConfirmEl",
      get: function get() {
        return this.dialog.querySelector('.btn-confirm');
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$i(JquerySkDialog, "beforeRendered", this, 3)([]);
        this.saveState();
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        //super.afterRendered();
        this.restoreState({
          contentsState: this.contentsState || this.comp.contentsState
        });
        if (this.dialog) {
          this.polyfillNativeDialog(this.dialog);
        }
        this.bindEvents();
        this.close();
        this.renderTitle();
        if (this.comp.type === 'confirm') {
          this.renderFooter();
        }
        this.bindActions(this.dialog);
        this.mountStyles();
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$i(JquerySkDialog, "bindEvents", this, 3)([]);
        this.closeBtn.onclick = function (event) {
          this.close(event);
        }.bind(this);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$i(JquerySkDialog, "unbindEvents", this, 3)([]);
        this.closeBtn.removeEventListener('click', this.close);
      }
    }]);
  }(SkDialogImpl);

  function _callSuper$x(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$x() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$x() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$x = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkDialog = /*#__PURE__*/function (_SkElement) {
    function SkDialog() {
      _classCallCheck(this, SkDialog);
      return _callSuper$x(this, SkDialog, arguments);
    }
    _inherits(SkDialog, _SkElement);
    return _createClass(SkDialog, [{
      key: "cnSuffix",
      get: function get() {
        return 'dialog';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "open",
      value: function open() {
        this.impl.open();
      }
    }, {
      key: "close",
      value: function close() {
        this.impl.close();
      }
    }, {
      key: "toggle",
      value: function toggle() {
        if (this.open) {
          this.impl.close();
        } else {
          this.impl.open();
        }
      }
    }, {
      key: "dialogTn",
      get: function get() {
        return this.confValOrDefault('dialog-tn', 'dialog');
      },
      set: function set(dialogTn) {
        return this.setAttribute('dialog-tn', dialogTn);
      }
    }, {
      key: "type",
      get: function get() {
        return this.getAttribute('type');
      }
    }, {
      key: "oncancel",
      value: function oncancel() {
        this.close();
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'open' && this.implRenderTimest) {
          if (newValue !== null && !oldValue) {
            if (this.impl && !this.impl.open) {
              this.open();
            }
          } else {
            if (newValue === null) {
              this.close();
            }
          }
        }
        if (name === 'title' && this.implRenderTimest) {
          this.impl.setTitle(newValue);
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['open', 'title'];
      }
    }]);
  }(SkElement);

  function _createForOfIteratorHelper$b(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$b(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$b(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$b(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$b(r, a) : void 0; } }
  function _arrayLikeToArray$b(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$w(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$w() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$w() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$w = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$h(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkNavbarImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkNavbarImpl() {
      _classCallCheck(this, SkNavbarImpl);
      return _callSuper$w(this, SkNavbarImpl, arguments);
    }
    _inherits(SkNavbarImpl, _SkComponentImpl);
    return _createClass(SkNavbarImpl, [{
      key: "suffix",
      get: function get() {
        return 'navbar';
      }
    }, {
      key: "panelEl",
      get: function get() {
        if (!this._panelEl) {
          this._panelEl = this.comp.el.querySelector('.sk-navbar-panel');
        }
        return this._panelEl;
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$h(SkNavbarImpl, "beforeRendered", this, 3)([]);
        this.templates = this.comp.querySelectorAll('template');
        this.saveState();
      }
    }, {
      key: "checkForAutoOpen",
      value: function checkForAutoOpen(event) {
        if (this.comp.getAttribute('align') === 'right') {
          if (event.offsetX + event.offsetX * 0.2 > window.innerWidth) {
            this.open();
          } else {
            this.close();
          }
        } else if (this.comp.getAttribute('align') === 'top') {
          if (event.offsetY + event.offsetY * 0.2 - this.getScrollTop() < window.innerHeight / 7) {
            this.open();
          } else {
            this.close();
          }
        } else if (this.comp.getAttribute('align') === 'bottom') {
          if (event.offsetY + event.offsetY * 0.4 - this.getScrollTop() > window.innerHeight) {
            this.open();
          } else {
            this.close();
          }
        } else {
          if (event.offsetX + event.offsetX * 0.2 < window.innerWidth / 7) {
            this.open();
          } else {
            this.close();
          }
        }
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$h(SkNavbarImpl, "afterRendered", this, 3)([]);
        if (this.templates) {
          var _iterator = _createForOfIteratorHelper$b(this.templates),
            _step;
          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var tpl = _step.value;
              this.comp.appendChild(tpl);
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
        if (this.comp.hasAttribute('auto-open')) {
          if (this.autoOpenHandler) {
            document.removeEventListener('click', this.autoOpenHandler);
          }
          this.autoOpenHandler = document.addEventListener('mousemove', function (event) {
            this.checkForAutoOpen(event);
          }.bind(this));
        }
      }
    }, {
      key: "renderPanelTpl",
      value: function () {
        var _renderPanelTpl = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                if (!this.comp.panelTplPath) {
                  _context.next = 5;
                  break;
                }
                _context.next = 3;
                return this.comp.renderer.mountTemplate(this.comp.panelTplPath, this.comp.constructor.name + 'PanelTpl',
                // :TODO make cachedTplId better
                this.comp, {
                  themePath: "".concat(this.comp.basePath, "/theme/").concat(this.comp.theme)
                });
              case 3:
                this.comp.panelTpl = _context.sent;
                this.renderWithVars(this.getOrGenId(), this.panelEl, this.comp.panelTpl);
              case 5:
              case "end":
                return _context.stop();
            }
          }, _callee, this);
        }));
        function renderPanelTpl() {
          return _renderPanelTpl.apply(this, arguments);
        }
        return renderPanelTpl;
      }()
    }, {
      key: "bindSticky",
      value: function bindSticky(targetEl, index) {
        if (!targetEl) {
          return false;
        }
        this.stickyHandler = this.stickyHandler ? this.stickyHandler : [];
        if (this.stickyHandler && this.stickyHandler[index]) {
          window.removeEventListener('scroll', this.stickyHandler[index]);
        }
        this.stickyHandler[index] = function (event) {
          var scrollTop = this.getScrollTop();
          if (scrollTop > 0) {
            targetEl.style.position = 'fixed';
          } else {
            targetEl.style.removeProperty('position');
          }
        }.bind(this);
        window.addEventListener('scroll', this.stickyHandler[index]);
      }
    }, {
      key: "onDisconnected",
      value: function onDisconnected() {
        if (this.stickyHandler) {
          for (var _i = 0, _Object$keys = Object.keys(this.stickyHandler); _i < _Object$keys.length; _i++) {
            var handlerKey = _Object$keys[_i];
            var handler = this.stickyHandler[handlerKey];
            window.removeEventListener('scroll', handler);
          }
        }
      }
    }, {
      key: "setAlignStyling",
      value: function setAlignStyling() {
        var align = this.comp.getAttribute('align');
        if (align) {
          this.bodyEl.classList.add('sk-navbar-' + align);
          if (this.comp.hasAttribute('panel')) {
            this.panelEl.classList.add('sk-navbar-panel-' + align);
          }
          this.setContentLength(align);
        } else {
          this.bodyEl.classList.add('sk-navbar-left');
          if (this.comp.hasAttribute('panel')) {
            this.panelEl.classList.add('sk-navbar-panel-left');
          }
        }
      }
    }, {
      key: "setContentLength",
      value: function setContentLength(align) {
        if (this.comp.hasAttribute('clength')) {
          if (align === 'left' || align === 'right' || !align) {
            this.bodyEl.style.width = this.comp.getAttribute('clength');
          } else {
            this.bodyEl.style.height = this.comp.getAttribute('clength');
          }
        }
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$v(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$v() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$v() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$v = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$g(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkNavbar = /*#__PURE__*/function (_SkNavbarImpl) {
    function JquerySkNavbar() {
      _classCallCheck(this, JquerySkNavbar);
      return _callSuper$v(this, JquerySkNavbar, arguments);
    }
    _inherits(JquerySkNavbar, _SkNavbarImpl);
    return _createClass(JquerySkNavbar, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'navbar';
      }
    }, {
      key: "bodyEl",
      get: function get() {
        if (!this._bodyEl) {
          this._bodyEl = this.comp.el.querySelector('.sk-navbar');
        }
        return this._bodyEl;
      }
    }, {
      key: "open",
      value: function open() {
        this.bodyEl.classList.add('sk-navbar-open');
        this.panelEl.classList.add('sk-navbar-panel-open');
      }
    }, {
      key: "close",
      value: function close() {
        this.bodyEl.classList.remove('sk-navbar-open');
        this.panelEl.classList.remove('sk-navbar-panel-open');
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$g(JquerySkNavbar, "afterRendered", this, 3)([]);
        this.bodyEl.innerHTML = this.contentsState || this.comp.contentsState;
        this.setAlignStyling();
        if (this.comp.hasAttribute('panel')) {
          this.bodyEl.classList.add('has-panel');
        }
        this.renderPanelTpl();
        if (this.comp.getAttribute('sticky') !== 'false') {
          this.bindSticky(this.panelEl, 'panel');
          this.bindSticky(this.bodyEl, 'body');
        }
      }
    }]);
  }(SkNavbarImpl);

  function _callSuper$u(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$u() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$u() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$u = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkNavbar = /*#__PURE__*/function (_SkElement) {
    function SkNavbar() {
      _classCallCheck(this, SkNavbar);
      return _callSuper$u(this, SkNavbar, arguments);
    }
    _inherits(SkNavbar, _SkElement);
    return _createClass(SkNavbar, [{
      key: "cnSuffix",
      get: function get() {
        return 'navbar';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "panelTplPath",
      get: function get() {
        return this.getAttribute('panel-tpl-path');
      },
      set: function set(tplPath) {
        return this.setAttribute('panel-tpl-path', tplPath);
      }
    }, {
      key: "open",
      value: function open() {
        this.impl.open();
      }
    }, {
      key: "close",
      value: function close() {
        this.impl.close();
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'open') {
          if (newValue !== null && !oldValue) {
            this.open();
          } else {
            if (newValue === null) {
              this.close();
            }
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['open'];
      }
    }]);
  }(SkElement);

  var REQ_TYPE_GET = "GET";
  var REQ_TYPE_POST = "POST";
  var REQ_TYPE_PUT = "PUT";
  var REQ_TYPE_DELETE = "DELETE";
  var REQ_TYPE_OPTIONS = "OPTIONS";
  var CONTENT_TYPE_JSON = "application/json";
  var HEADER_CONTENT_TYPE = "Content-Type";
  var HEADER_AUTHORIZATION = "Authorization";
  var ARG_CONTENT_TYPE_AUTO = "auto";
  var AUTH_TYPE_BASIC = "basic";
  var HttpClientBase = /*#__PURE__*/function () {
    function HttpClientBase() {
      _classCallCheck(this, HttpClientBase);
    }
    return _createClass(HttpClientBase, [{
      key: "get",
      value: function get(url, contentType) {
        return this.request(url, REQ_TYPE_GET, contentType ? contentType : null, null);
      }
    }, {
      key: "post",
      value: function post(url, contentType, body) {
        return this.request(url, REQ_TYPE_POST, contentType ? contentType : null, body);
      }
    }, {
      key: "put",
      value: function put(url, contentType, body) {
        return this.request(url, REQ_TYPE_PUT, contentType ? contentType : null, body);
      }
    }, {
      key: "delete",
      value: function _delete(url, contentType, body) {
        return this.request(url, REQ_TYPE_DELETE, contentType ? contentType : null, body);
      }
    }, {
      key: "options",
      value: function options(url, contentType, body) {
        return this.request(url, REQ_TYPE_OPTIONS, contentType ? contentType : null, body);
      }
    }]);
  }();

  function _callSuper$t(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$t() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$t() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$t = function _isNativeReflectConstruct() { return !!t; })(); }
  var XmlHttpClient = /*#__PURE__*/function (_HttpClientBase) {
    function XmlHttpClient() {
      _classCallCheck(this, XmlHttpClient);
      return _callSuper$t(this, XmlHttpClient, arguments);
    }
    _inherits(XmlHttpClient, _HttpClientBase);
    return _createClass(XmlHttpClient, [{
      key: "request",
      value:
      //auth;
      //headers;

      function request(url) {
        var _this = this;
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'GET';
        var contentType = arguments.length > 2 ? arguments[2] : undefined;
        var body = arguments.length > 3 ? arguments[3] : undefined;
        var headers = arguments.length > 4 ? arguments[4] : undefined;
        return new Promise(function (resolve, reject) {
          var _this2 = this;
          _newArrowCheck(this, _this);
          var xhr = new XMLHttpRequest();
          xhr.open(type, url);
          if (contentType !== ARG_CONTENT_TYPE_AUTO) {
            xhr.setRequestHeader(HEADER_CONTENT_TYPE, contentType ? contentType : CONTENT_TYPE_JSON);
          }
          if (this.auth && this.auth.type === AUTH_TYPE_BASIC) {
            xhr.setRequestHeader(HEADER_AUTHORIZATION, "Basic " + btoa(this.auth.username + ':' + this.auth.password));
          }
          if (this.headers && _typeof(this.headers) === 'object') {
            for (var _i = 0, _Object$keys = Object.keys(this.headers); _i < _Object$keys.length; _i++) {
              var headerName = _Object$keys[_i];
              xhr.setRequestHeader(headerName, this.headers[headerName]);
            }
          }
          if (headers && _typeof(headers) === 'object') {
            for (var _i2 = 0, _Object$keys2 = Object.keys(headers); _i2 < _Object$keys2.length; _i2++) {
              var _headerName = _Object$keys2[_i2];
              xhr.setRequestHeader(_headerName, headers[_headerName]);
            }
          }
          if (!ResLoader.isInIE()) {
            xhr.responseType = 'json';
          }
          xhr.onreadystatechange = function (event) {
            _newArrowCheck(this, _this2);
            var request = event.target;
            if (request.readyState === XMLHttpRequest.DONE) {
              if (request.status === 200) {
                resolve(request);
              } else {
                reject(request);
              }
            }
          }.bind(this);
          if (body) {
            xhr.send(body);
          } else {
            xhr.send();
          }
        }.bind(this));
      }
    }]);
  }(HttpClientBase);

  function _createForOfIteratorHelper$a(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$a(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$a(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$a(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$a(r, a) : void 0; } }
  function _arrayLikeToArray$a(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$s(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$s() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$s() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$s = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$f(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var FORM_SUBELEMENTS_SL = 'sk-input,sk-checkbox,sk-select,sk-datepicker,sk-switch,input,textarea,select';
  var SkFormImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkFormImpl() {
      _classCallCheck(this, SkFormImpl);
      return _callSuper$s(this, SkFormImpl, arguments);
    }
    _inherits(SkFormImpl, _SkComponentImpl);
    return _createClass(SkFormImpl, [{
      key: "formEl",
      get: function get() {
        if (!this._formEl) {
          this._formEl = this.comp.el.querySelector('form');
        }
        return this._formEl;
      }
    }, {
      key: "formSubElementsSl",
      get: function get() {
        if (!this._formSubElementsSl) {
          this._formSubElementsSl = FORM_SUBELEMENTS_SL;
        }
        return this._formSubElementsSl;
      },
      set: function set(sl) {
        this._formSubElementsSl = sl;
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$f(SkFormImpl, "bindEvents", this, 3)([]);
        if (this.submitHandler) {
          this.comp.removeEventListener('formsubmit', this.submitHandler);
        }
        this.submitHandler = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onSubmit(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.comp.addEventListener('formsubmit', this.submitHandler);
        this.onSuccessHandler = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onSubmitSuccess(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.onErrorHandler = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onSubmitError(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.onValid = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onFormValid(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.onInvalid = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onFormInvalid(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.validators = {};
        if (!this.comp.hasAttribute('no-live-validation')) {
          this.logger.debug('binding form custom validators');
          this.bindFieldValidators();
        }
        this.bindActions(this.comp);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$f(SkFormImpl, "unbindEvents", this, 3)([]);
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$f(SkFormImpl, "afterRendered", this, 3)([]);
        this.restoreState();
        this.applyValidationLabel();
      }
    }, {
      key: "applyValidationLabel",
      value: function applyValidationLabel() {
        if (this.comp.hasAttribute('validation-label')) {
          var validationLabel = this.comp.getAttribute('validation-label');
          var fields = this.comp.el.host.querySelectorAll(this.formSubElementsSl);
          var _iterator = _createForOfIteratorHelper$a(fields),
            _step;
          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var field = _step.value;
              if (!field.hasAttribute('validation-label')) {
                field.setAttribute('validation-label', validationLabel);
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
      }
    }, {
      key: "httpClient",
      get: function get() {
        if (!this._http) {
          this._http = new XmlHttpClient();
        }
        return this._http;
      }
    }, {
      key: "onSubmitSuccess",
      value: function onSubmitSuccess(event) {
        var validationResult = false;
        if (this.comp.hasAttribute('response-validation')) {
          var validator = this.comp.getAttribute('response-validation');
          if (validator === 'false' || validator === 'no' || validator === 'disabled') {
            validationResult = true;
          } else {
            if (typeof this.comp[validator] === 'function') {
              validationResult = this.comp[validator].call(this.comp, event);
            } else if (typeof this[validator] === 'function') {
              validationResult = this[validator].call(this, event);
            } else if (typeof window[validator] === 'function') {
              validationResult = window[validator].call(this.comp, event);
            } else if (typeof this.comp['validateResponse'] === 'function') {
              validationResult = this.comp['validateResponse'].call(this.comp, event);
            } else if (typeof this['validateResponse'] === 'function') {
              validationResult = this['validateResponse'].call(this.comp, event);
            } else {
              this.logger.warn("Specified validator ".concat(validator, " not found"));
              validationResult = true;
            }
          }
        } else {
          validationResult = true;
        }
        if (validationResult) {
          this.comp.dispatchEvent(new CustomEvent('formsubmitsuccess', {
            bubbles: true,
            composite: true,
            detail: {
              request: event
            }
          }));
          this.comp.dispatchEvent(new CustomEvent('skformsubmitsuccess', {
            bubbles: true,
            composite: true,
            detail: {
              request: event
            }
          }));
        } else {
          this.onSubmitError(event);
        }
      }
    }, {
      key: "onFormValid",
      value: function onFormValid() {
        this.comp.dispatchEvent(new CustomEvent('formvalid', {
          bubbles: true,
          composite: true,
          detail: {}
        }));
        this.comp.dispatchEvent(new CustomEvent('skformvalid', {
          bubbles: true,
          composite: true,
          detail: {}
        }));
      }
    }, {
      key: "onFormInvalid",
      value: function onFormInvalid(errors) {
        this.comp.dispatchEvent(new CustomEvent('forminvalid', {
          bubbles: true,
          composite: true,
          detail: {
            errors: errors
          }
        }));
        this.comp.dispatchEvent(new CustomEvent('skforminvalid', {
          bubbles: true,
          composite: true,
          detail: {
            errors: errors
          }
        }));
      }
    }, {
      key: "onSubmitError",
      value: function onSubmitError(event) {
        this.comp.dispatchEvent(new CustomEvent('formsubmiterror', {
          bubbles: true,
          composite: true,
          detail: {
            request: event
          }
        }));
        this.comp.dispatchEvent(new CustomEvent('skformsubmiterror', {
          bubbles: true,
          composite: true,
          detail: {
            request: event
          }
        }));
      }
    }, {
      key: "onFieldChange",
      value: function onFieldChange(event) {
        this.validateCustom(event.target);
      }
    }, {
      key: "bindFieldValidators",
      value: function bindFieldValidators() {
        var fields = this.queryFields();
        var _iterator2 = _createForOfIteratorHelper$a(fields),
          _step2;
        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var field = _step2.value;
            var name = field.getAttribute('name'); // :TODO generate identifier if no name provided
            this.bindFieldValidation(name, field);
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
      }
    }, {
      key: "onDisconnected",
      value: function onDisconnected() {
        this.unbindCustomValidators();
      }
    }, {
      key: "unbindCustomValidators",
      value: function unbindCustomValidators() {
        var fields = this.queryFields();
        var _iterator3 = _createForOfIteratorHelper$a(fields),
          _step3;
        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var field = _step3.value;
            var name = field.getAttribute('name'); // :TODO generate identifier if no name provided
            field.oninput = null;
            field.onchange = null;
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }
      }
    }, {
      key: "onSubmit",
      value: function onSubmit(event) {
        var fields = this.queryFields();
        var formData = new FormData();
        if (!this.validationDisabled()) {
          var errors = {};
          var _iterator4 = _createForOfIteratorHelper$a(fields),
            _step4;
          try {
            for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
              var field = _step4.value;
              var name = field.getAttribute('name'); // :TODO generate identifier if no name provided
              if (field.validity.customError) {
                field.setCustomValidity('');
              }
              var validatedField = this.validateBuildIns(field);
              validatedField = this.validateCustom(field);
              if (!this.comp.hasAttribute('no-live-validation')) {
                this.bindFieldValidation(name, field);
              }
              if (validatedField.validity.valid) {
                this.addFieldValue(validatedField, name, formData);
                if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
                  this.flushFieldValidity(validatedField);
                }
                this.renderFieldValid(validatedField);
              } else {
                if (!validatedField.hasAttribute('form-validation')) {
                  this.renderFieldInvalid(validatedField);
                }
                if (this.comp.getAttribute('validation-label') !== 'disabled' && field.getAttribute('validation-label') !== 'disabled') {
                  if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
                    this.flushFieldValidity(field);
                    this.renderFieldValidation(field);
                  }
                }
                errors[name] = validatedField.validity;
              }
            }
          } catch (err) {
            _iterator4.e(err);
          } finally {
            _iterator4.f();
          }
          if (Object.keys(errors).length === 0) {
            this.onFormValid();
            this.sendFormData(formData);
          } else {
            this.onFormInvalid(errors);
          }
        } else {
          var _iterator5 = _createForOfIteratorHelper$a(fields),
            _step5;
          try {
            for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
              var _field = _step5.value;
              var _name = _field.getAttribute('name');
              this.addFieldValue(_field, _name, formData);
            }
          } catch (err) {
            _iterator5.e(err);
          } finally {
            _iterator5.f();
          }
          this.sendFormData(formData);
        }
      }
    }, {
      key: "queryFields",
      value: function queryFields() {
        var fields = this.comp.el.host.querySelectorAll(this.formSubElementsSl);
        return fields;
      }
    }, {
      key: "bindFieldValidation",
      value: function bindFieldValidation(name, field) {
        if (!this.validators[name]) {
          this.validators[name] = this.onFieldChange.bind(this);
          field.oninput = field.onchange = function (event) {
            this.validators[name](event);
          }.bind(this);
          field.addEventListener('formchange', this.validators[name]);
        }
      }
    }, {
      key: "validationDisabled",
      value: function validationDisabled() {
        return this.comp.getAttribute('novalidate') && this.comp.getAttribute('novalidate') !== '';
      }
    }, {
      key: "skValidators",
      get: function get() {
        if (!this._skValidators) {
          this._skValidators = {
            'sk-required': new SkRequired(),
            'sk-min': new SkMin(),
            'sk-max': new SkMax(),
            'sk-email': new SkEmail(),
            'sk-pattern': new SkPattern()
          };
        }
        return this._skValidators;
      }
    }, {
      key: "validateBuildIns",
      value: function validateBuildIns(field) {
        var result = false;
        for (var _i = 0, _Object$keys = Object.keys(this.skValidators); _i < _Object$keys.length; _i++) {
          var validatorAttr = _Object$keys[_i];
          if (field.hasAttribute(validatorAttr)) {
            result = this.skValidators[validatorAttr].validate(field, this);
            if (typeof result === 'string') {
              field.setCustomValidity(this.comp.locale.tr(result));
            }
          }
        }
      }
    }, {
      key: "validateCustom",
      value: function validateCustom(field) {
        var formValidation = field.getAttribute('form-validation');
        var validationMessages = {};
        try {
          if (field.hasAttribute('form-validation-msg')) {
            validationMessages = JSON.parse(field.getAttribute('form-validation-msg'));
          }
        } catch (_unused) {
          this.logger.debug('failed to load validation texts from attribute for field', field);
        }
        if (formValidation) {
          var validationMsgKey = null;
          if (typeof field[formValidation] === 'function') {
            validationMsgKey = field[formValidation].call(this, field, this);
          } else if (typeof window[formValidation] === 'function') {
            validationMsgKey = window[formValidation].call(window, field, this);
          }
          if (typeof field['setCustomValidity'] === 'function') {
            if (validationMsgKey !== true) {
              if (validationMessages[validationMsgKey]) {
                validationMsgKey = validationMessages[validationMsgKey];
              }
              field.setCustomValidity(typeof validationMsgKey === 'string' ? validationMsgKey : this.comp.locale.tr('Field value is invalid'));
              if (this.comp.getAttribute('validation-label') !== 'disabled' && field.getAttribute('validation-label') !== 'disabled') {
                if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
                  this.flushFieldValidity(field);
                  this.renderFieldValidation(field);
                }
              }
              this.renderFieldInvalid(field);
            } else {
              if (typeof field['flushValidity'] === 'function') {
                field.flushValidity();
              } else {
                // possibly native element
                field.setCustomValidity('');
                if (this.comp.getAttribute('validation-label') !== 'disabled' && field.getAttribute('validation-label') !== 'disabled') {
                  this.flushFieldValidity(field);
                }
              }
              this.renderFieldValid(field);
            }
          }
        }
        return field;
      }
    }, {
      key: "renderFieldValidation",
      value: function renderFieldValidation(field) {
        var msgEl = field.nextElementSibling;
        var msgExists = false;
        if (!msgEl) {
          msgEl = this.comp.renderer.createEl('span');
          msgEl.classList.add('form-validation-message');
        } else {
          msgExists = true;
          msgEl.innerHTML = '';
        }
        msgEl.insertAdjacentHTML('afterbegin', field.validationMessage);
        if (!msgExists) {
          field.insertAdjacentHTML('afterend', msgEl.outerHTML);
        }
      }
    }, {
      key: "flushFieldValidity",
      value: function flushFieldValidity(field) {
        var possibleLabel = field.nextElementSibling;
        if (possibleLabel && possibleLabel.classList.contains('form-validation-message')) {
          possibleLabel.parentElement.removeChild(possibleLabel);
        }
      }
    }, {
      key: "renderFieldInvalid",
      value: function renderFieldInvalid(field) {
        if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
          field.classList.add('form-invalid');
        } else {
          field.setAttribute('form-invalid', '');
        }
      }
    }, {
      key: "renderFieldValid",
      value: function renderFieldValid(field) {
        if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
          field.classList.remove('form-invalid');
        } else {
          field.removeAttribute('form-invalid');
        }
      }
    }, {
      key: "addFieldValue",
      value: function addFieldValue(field, name, formData) {
        var value = null;
        if (field.type === 'checkbox' || field.type === 'radio') {
          if (field.getAttribute('checked')) {
            value = field.value ? field.value : '1';
          }
        } else if ((field.type === 'select' || field.tagName === 'SELECT') && !field.value) {
          value = field.selectedOptions[0].value;
        } else {
          value = field.value || field.getAttribute('value');
        }
        if (name && value) {
          formData.append(name, value);
        }
      }
    }, {
      key: "sendFormData",
      value: function sendFormData(formData) {
        var action = this.comp.getAttribute('action');
        var method = this.comp.getAttribute('method');
        var enctype = this.comp.getAttribute('enctype') || 'application/x-www-form-urlencoded';
        if (enctype === 'application/json') {
          if (Object.fromEntries) {
            formData = JSON.stringify(Object.fromEntries(formData));
          } else {
            formData = JSON.stringify(this.comp.fromEntries(formData));
          }
        }
        var req = this.httpClient.request(action, method, enctype, formData);
        req.then(this.onSuccessHandler, this.onErrorHandler);
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$r(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$r() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$r() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$r = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$e(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkForm = /*#__PURE__*/function (_SkFormImpl) {
    function JquerySkForm() {
      _classCallCheck(this, JquerySkForm);
      return _callSuper$r(this, JquerySkForm, arguments);
    }
    _inherits(JquerySkForm, _SkFormImpl);
    return _createClass(JquerySkForm, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'form';
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$e(JquerySkForm, "beforeRendered", this, 3)([]);
        this.attachStyleByPath(this.skTheme.basePath + '/jquery-theme.css', this.comp.querySelector('span[slot=fields]'));
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$e(JquerySkForm, "afterRendered", this, 3)([]);
        this.mountStyles();
      }
    }]);
  }(SkFormImpl);

  function _createForOfIteratorHelper$9(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$9(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$9(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$9(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$9(r, a) : void 0; } }
  function _arrayLikeToArray$9(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$q(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$q() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$q() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$q = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkForm = /*#__PURE__*/function (_SkElement) {
    function SkForm() {
      _classCallCheck(this, SkForm);
      return _callSuper$q(this, SkForm, arguments);
    }
    _inherits(SkForm, _SkElement);
    return _createClass(SkForm, [{
      key: "cnSuffix",
      get: function get() {
        return 'form';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "reset",
      value: function reset() {
        var fields = this.impl.queryFields();
        var _iterator = _createForOfIteratorHelper$9(fields),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var field = _step.value;
            if (typeof field.reset === 'function') {
              field.reset();
            } else if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
              field.value = '';
            } else if (field.tagName === 'SK-CHECKBOX' || field.tagName === 'SK-SWITCH' || field.tagName === 'CHECKBOX') {
              field.removeAttribute('checked');
            } else if (field.hasAttribute('value') || field.tagName === 'SK-INPUT' || field.tagName === 'SK-DATEPICKER' || field.tagName === 'SK-SELECT') {
              field.setAttribute('value', '');
            } else {
              field.value = '';
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }, {
      key: "focus",
      value: function focus() {
        var _this = this;
        setTimeout(function () {
          _newArrowCheck(this, _this);
          this.input.formEl.focus();
        }.bind(this), 0);
      }
    }]);
  }(SkElement);

  function _createForOfIteratorHelper$8(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$8(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$8(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$8(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$8(r, a) : void 0; } }
  function _arrayLikeToArray$8(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$p(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$p() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$p() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$p = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$d(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkSelectImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkSelectImpl() {
      _classCallCheck(this, SkSelectImpl);
      return _callSuper$p(this, SkSelectImpl, arguments);
    }
    _inherits(SkSelectImpl, _SkComponentImpl);
    return _createClass(SkSelectImpl, [{
      key: "suffix",
      get: function get() {
        return 'select';
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$d(SkSelectImpl, "beforeRendered", this, 3)([]);
        this.saveState();
      }
    }, {
      key: "saveState",
      value: function saveState() {
        if (!this.comp.contentsState) {
          if (this.comp.useShadowRoot) {
            this.comp.contentsState = this.comp.el.host.innerHTML;
            //this.comp.el.host.innerHTML = '';
          } else {
            this.comp.contentsState = this.comp.el.innerHTML;
            //this.comp.el.innerHTML = '';
          }
        }
      }
    }, {
      key: "body",
      get: function get() {
        if (!this._body) {
          this._body = this.comp.el.querySelector('select');
        }
        return this._body;
      },
      set: function set(el) {
        this._body = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['body'];
      }
    }, {
      key: "dumpState",
      value: function dumpState() {
        return {
          'contents': this.comp.contentsState
        };
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        //this.body.innerHTML = '';
        this.comp.el.insertAdjacentHTML('beforeend', state.contentsState);
        this.body.value = this.comp.value;
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        this.body.onchange = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          if (this.comp.useShadowRoot) {
            this.comp.el.host.setAttribute('value', event.target.value);
            this.comp.el.host.dispatchEvent(new CustomEvent('change', {
              target: this.comp,
              bubbles: true,
              composed: true
            }));
            this.comp.el.host.dispatchEvent(new CustomEvent('skchange', {
              target: this.comp,
              bubbles: true,
              composed: true
            }));
          } else {
            this.comp.el.setAttribute('value', event.target.value);
            this.comp.el.dispatchEvent(new CustomEvent('change', {
              target: this.comp,
              bubbles: true,
              composed: true
            }));
            this.comp.el.dispatchEvent(new CustomEvent('skchange', {
              target: this.comp,
              bubbles: true,
              composed: true
            }));
          }
          this.comp.setAttribute('value', event.target.value);
          var result = this.validateWithAttrs();
          if (typeof result === 'string') {
            this.showInvalid();
          }
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
      }
    }, {
      key: "fitPopup",
      value: function fitPopup() {
        if (this.comp.hasAttribute('open')) {
          var box = this.comp.el.host.getBoundingClientRect();
          this.popupEl.style.position = 'fixed';
          this.popupEl.style.top = "".concat(window.pageYOffset + box.bottom - this.getScrollTop(), "px");
          this.popupEl.style.left = "".concat(box.left, "px");
        }
      }
    }, {
      key: "bindFitPopup",
      value: function bindFitPopup() {
        if (this.onScrollHandler) {
          window.removeEventListener('scroll', this.onScrollHandler);
        }
        this.onScrollHandler = this.fitPopup.bind(this);
        window.addEventListener('scroll', this.onScrollHandler);
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$d(SkSelectImpl, "afterRendered", this, 3)([]);
        this.restoreState({
          contentsState: this.contentsState || this.comp.contentsState
        });
        if (this.comp.useShadowRoot) {
          if (this.comp.value || this.comp.hasAttribute('value')) {
            this.body.value = this.comp.value || this.comp.getAttribute('value');
          }
        } else {
          if (this.comp.el.value) {
            this.body.value = this.comp.el.value;
          }
        }
      }
    }, {
      key: "enable",
      value: function enable() {
        _superPropGet$d(SkSelectImpl, "enable", this, 3)([]);
      }
    }, {
      key: "disable",
      value: function disable() {
        _superPropGet$d(SkSelectImpl, "disable", this, 3)([]);
      }
    }, {
      key: "optionEls",
      get: function get() {
        return this.body && typeof this.body['querySelectorAll'] === 'function' ? this.body.querySelectorAll('option') : [];
      }
    }, {
      key: "selectOptionByValue",
      value: function selectOptionByValue(value) {
        if (!value) {
          this.selectOption(value);
        } else {
          if (this.options && Object.keys(this.options).length > 0) {
            for (var _i = 0, _Object$keys = Object.keys(this.options); _i < _Object$keys.length; _i++) {
              var optionKey = _Object$keys[_i];
              var option = this.options[optionKey];
              if (value === option.value || value === option.innerHTML) {
                this.selectOption(option);
              }
            }
          } else {
            var options = this.optionEls;
            if (options) {
              var _iterator = _createForOfIteratorHelper$8(options),
                _step;
              try {
                for (_iterator.s(); !(_step = _iterator.n()).done;) {
                  var _option = _step.value;
                  if (value === _option.value || value === _option.innerHTML) {
                    this.selectOption(_option);
                  }
                }
              } catch (err) {
                _iterator.e(err);
              } finally {
                _iterator.f();
              }
            }
          }
        }
      }
    }, {
      key: "isClickedOnDropdown",
      value: function isClickedOnDropdown(event) {
        var _this$btnMultiselectE, _this$dropdownEl;
        var xy = this.comp.hasAttribute('multiple') ? (_this$btnMultiselectE = this.btnMultiselectEl) === null || _this$btnMultiselectE === void 0 ? void 0 : _this$btnMultiselectE.getBoundingClientRect() : (_this$dropdownEl = this.dropdownEl) === null || _this$dropdownEl === void 0 ? void 0 : _this$dropdownEl.getBoundingClientRect();
        if (xy) {
          return event.pageX > xy.left && event.pageX < window.pageXOffset + xy.left + xy.width && event.pageY > xy.top && event.pageY < window.pageYOffset + xy.top + xy.height;
        } else {
          return false;
        }
      }
    }]);
  }(SkComponentImpl);

  function _createForOfIteratorHelper$7(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$7(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$7(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$7(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$7(r, a) : void 0; } }
  function _arrayLikeToArray$7(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$o(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$o() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$o() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$o = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$c(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkSelect = /*#__PURE__*/function (_SkSelectImpl) {
    function JquerySkSelect() {
      _classCallCheck(this, JquerySkSelect);
      return _callSuper$o(this, JquerySkSelect, arguments);
    }
    _inherits(JquerySkSelect, _SkSelectImpl);
    return _createClass(JquerySkSelect, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'select';
      }
    }, {
      key: "optionsEl",
      get: function get() {
        if (!this._optionsEl) {
          this._optionsEl = this.comp.el.querySelector('.ui-selectmenu-menu');
        }
        return this._optionsEl;
      }
    }, {
      key: "optionsListEl",
      get: function get() {
        return this.optionsEl.querySelector('ul');
      }
    }, {
      key: "dropdownEl",
      get: function get() {
        if (!this._dropdownEl) {
          this._dropdownEl = this.comp.el.querySelector('.ui-selectmenu-button');
        }
        return this._dropdownEl;
      },
      set: function set(el) {
        this._dropdownEl = el;
      }
    }, {
      key: "selectedLabelEl",
      get: function get() {
        if (!this._selectedLabelEl) {
          this._selectedLabelEl = this.comp.el.querySelector('.ui-selectmenu-text');
        }
        return this._selectedLabelEl;
      },
      set: function set(el) {
        this._selectedLabelEl = el;
      }
    }, {
      key: "popupEl",
      get: function get() {
        return this.optionsEl;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['optionsEl', 'optionsListEl', 'dropdownEl', 'selectedLabelEl'];
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$c(JquerySkSelect, "afterRendered", this, 3)([]);
        this.mountStyles();
      }
    }, {
      key: "btnMultiselectEl",
      get: function get() {
        return this.comp.el.querySelector('button.ui-multiselect');
      }
    }, {
      key: "fmtMultiLabel",
      value: function fmtMultiLabel() {
        this.comp.selectedValues.length;
        Object.keys(this.options).length;
        if (this.comp.selectedValues.length > 0) {
          return "".concat(this.comp.selectedValues.slice(0, 3).join(', ')).concat(this.comp.selectedValues.length > 3 ? '..' : '');
        } else {
          return '--';
        }
      }
    }, {
      key: "onMouseLeave",
      value: function onMouseLeave(event) {
        this.toggleSelection();
      }
    }, {
      key: "renderMultiMode",
      value: function renderMultiMode(isMultiselect) {
        if (isMultiselect) {
          this.btnMultiselectEl.style.display = 'block';
          this.dropdownEl.style.display = 'none';
          var buttonLabel = this.btnMultiselectEl.querySelector('.sk-select-filter-label');
          buttonLabel.innerHTML = this.fmtMultiLabel();
          this.renderOptions();
          if (!this.leaveHandler) {
            this.optionsEl.onmouseleave = function (event) {
              this.onMouseLeave(event);
            }.bind(this);
          }
        } else {
          this.btnMultiselectEl.style.display = 'none';
          this.dropdownEl.style.display = 'inline-block';
        }
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        //this.body.innerHTML = '';
        //this.body.insertAdjacentHTML('beforeend', state.contentsState);
        this.body.style.display = 'none'; // hide original select element
        this.optionsListEl.innerHTML = '';
        if (this.comp.hasAttribute('multiple')) {
          this.renderOptions();
          this.renderMultiMode(true);
          this.optionsEl.style.display = 'none';
        } else {
          this.renderOptions();
          this.renderMultiMode(false);
          if (this.comp.el.host.value) {
            var optionFound = false;
            for (var _i = 0, _Object$keys = Object.keys(this.options); _i < _Object$keys.length; _i++) {
              var key = _Object$keys[_i];
              var option = this.options[key];
              var value = option.dataset.value || option.value || option.innerHTML;
              if (value === this.comp.el.host.value) {
                this.selectOption(this.options[key]);
                optionFound = true;
              }
            }
            if (!optionFound) {
              this.selectNoOption();
            }
          } else {
            this.selectNoOption();
            //this.selectOption();
          }
          this.toggleSelection(true);
        }
        this.bindEvents();
      }
    }, {
      key: "onClick",
      value: function onClick(event) {
        this.comp.callPluginHook('onEventStart', event);
        var menuItemEl = this.findSelectionItem(event.path);
        if (menuItemEl && menuItemEl.tagName === 'LI' && menuItemEl.classList.contains('ui-selectmenu-optgroup')) {
          // optgroup click
          return false;
        }
        var target = event.originalTarget || event.srcElement || event.target; // firefox and ie
        if (menuItemEl || target.classList.contains('ui-menu-item') || target.classList.contains('ui-menu-item-wrapper')) {
          var selectedOption = menuItemEl || target;
          this.selectOption(selectedOption, event);
          if (!this.comp.hasAttribute('multiple')) {
            this.toggleSelection();
          }
        } else {
          if (this.isClickedOnDropdown(event)) {
            this.toggleSelection();
          }
        }
        this.comp.callPluginHook('onEventEnd', event);
      }
    }, {
      key: "toggleSelection",
      value: function toggleSelection(forceOpen) {
        var open = this.dropdownEl.classList.contains('ui-selectmenu-button-open');
        if (open || forceOpen) {
          this.optionsEl.style.display = 'none';
          this.dropdownEl.classList.remove('ui-selectmenu-button-open');
          this.comp.removeAttribute('open');
        } else {
          var box = this.dropdownEl.getBoundingClientRect();
          if (this.comp.hasAttribute('multiple')) {
            box = this.btnMultiselectEl.getBoundingClientRect();
          }
          this.optionsEl.style.display = 'block';
          this.optionsEl.style.width = box.width - 2 + 'px';
          this.optionsEl.style.left = box.left + 'px';
          this.optionsEl.style.top = box.bottom + 'px';
          this.optionsEl.style.position = 'fixed';
          this.dropdownEl.classList.add('ui-selectmenu-button-open');
          this.comp.setAttribute('open', '');
        }
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$c(JquerySkSelect, "bindEvents", this, 3)([]);
        if (this.clickHandle) {
          this.comp.removeEventListener('click', this.clickHandle);
        }
        this.clickHandle = this.onClick.bind(this);
        this.comp.addEventListener('click', this.clickHandle);
        this.bindFitPopup();
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$c(JquerySkSelect, "unbindEvents", this, 3)([]);
        if (this.clickHandle) {
          this.comp.removeEventListener('click', this.clickHandle);
        }
        if (this.onScrollHandler) {
          window.removeEventListener('scroll', this.onScrollHandler);
          delete this.onScrollHandler;
        }
      }
    }, {
      key: "renderOptions",
      value: function renderOptions() {
        var num = 1;
        this.options = {};
        var optgroups = this.comp.querySelectorAll('optgroup') || this.optgroups;
        if (optgroups && optgroups.length > 0) {
          if (!this.optgroups) {
            this.optgroups = optgroups;
            var _iterator = _createForOfIteratorHelper$7(this.optgroups),
              _step;
            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var optgroup = _step.value;
                var label = optgroup.getAttribute('label') ? optgroup.getAttribute('label') : '';
                this.optionsListEl.insertAdjacentHTML('beforeend', "\n                    <li class=\"ui-selectmenu-optgroup ui-menu-divider\">".concat(label, "</li>\n                "));
                var groupOptions = optgroup.querySelectorAll('option');
                var _iterator2 = _createForOfIteratorHelper$7(groupOptions),
                  _step2;
                try {
                  for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                    var option = _step2.value;
                    this.renderOption(option, num);
                    this.options['option-' + num] = option;
                    num++;
                  }
                } catch (err) {
                  _iterator2.e(err);
                } finally {
                  _iterator2.f();
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          }
        } else {
          var options = this.comp.querySelectorAll('option');
          var _iterator3 = _createForOfIteratorHelper$7(options),
            _step3;
          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var _option = _step3.value;
              this.renderOption(_option, num);
              this.options['option-' + num] = _option;
              num++;
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
        }
      }
    }, {
      key: "renderOption",
      value: function renderOption(option, num) {
        var value = option.dataset.value || option.value || option.innerHTML;
        var title = option.innerHTML || value;
        if (this.comp.hasAttribute('multiple')) {
          this.optionsListEl.insertAdjacentHTML('beforeend', "\n                    <li class=\"ui-menu-item sk-select-multi-item\" data-value=\"".concat(value, "\">\n                        <input type=\"checkbox\" ").concat(this.comp.isSelected(value) ? 'checked' : '', " />\n                        <span id=\"ui-id-").concat(num, "\" tabindex=\"-1\" role=\"option\" class=\"ui-menu-item-wrapper\">").concat(title, "</span>\n                    </li>\n                "));
        } else {
          this.optionsListEl.insertAdjacentHTML('beforeend', "\n                    <li class=\"ui-menu-item\" data-value=\"".concat(value, "\">\n                        <span id=\"ui-id-").concat(num, "\" tabindex=\"-1\" role=\"option\" class=\"ui-menu-item-wrapper\">").concat(title, "</span>\n                    </li>\n                "));
        }
        //option.parentElement.removeChild(option);
      }
    }, {
      key: "findSelectionItem",
      value: function findSelectionItem(path) {
        if (!path) return null;
        var _iterator4 = _createForOfIteratorHelper$7(path),
          _step4;
        try {
          for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
            var item = _step4.value;
            if (item.tagName === 'LI') {
              return item;
            }
          }
        } catch (err) {
          _iterator4.e(err);
        } finally {
          _iterator4.f();
        }
      }
    }, {
      key: "selectOption",
      value: function selectOption(option, event) {
        if (this.comp.hasAttribute('multiple')) {
          var item = option;
          if (event && event.target.path) {
            this.findSelectionItem(event.target.path);
          }
          var value = item.dataset.value !== undefined ? item.dataset.value : option.value || option.innerHTML;
          var checkbox = option.parentElement.querySelector("li[data-value=".concat(value, "] input[type=checkbox]"));
          if (!checkbox.hasAttribute('checked')) {
            this.comp.selectValue(value);
            checkbox.setAttribute('checked', '');
          } else {
            this.comp.deSelectValue(value);
            checkbox.removeAttribute('checked');
          }
          this.renderMultiMode(true);
        } else {
          if (!option) {
            //option = this.options['option-1'];
            this.selectedLabelEl.innerHTML = '--';
            //this.comp.removeAttribute('value');
          } else {
            this.comp.setAttribute('value', option.tagName === 'OPTION' && option.value !== undefined ? option.value : option.dataset.value || option.innerText);
            this.comp.dispatchEvent(new CustomEvent('change', {
              target: this.comp,
              bubbles: true,
              composed: true
            }));
            this.comp.dispatchEvent(new CustomEvent('skchange', {
              target: this.comp,
              bubbles: true,
              composed: true
            }));
            this.selectedLabelEl.innerHTML = '';
            this.selectedLabelEl.insertAdjacentHTML('beforeend', option.innerText);
            this.comp.setAttribute('value', option.tagName === 'OPTION' && option.value !== undefined ? option.value : option.dataset.value || option.innerText);
          }
        }
      }
    }, {
      key: "selectNoOption",
      value: function selectNoOption() {
        this.selectedLabelEl.innerHTML = '';
        this.selectedLabelEl.insertAdjacentHTML('beforeend', '--');
      }
    }, {
      key: "disable",
      value: function disable() {
        this.dropdownEl.classList.add('ui-state-disabled');
      }
    }, {
      key: "enable",
      value: function enable() {
        this.dropdownEl.classList.remove('ui-state-disabled');
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        _superPropGet$c(JquerySkSelect, "showInvalid", this, 3)([]);
        this.dropdownEl.classList.add('form-invalid');
      }
    }, {
      key: "showValid",
      value: function showValid() {
        _superPropGet$c(JquerySkSelect, "showValid", this, 3)([]);
        this.dropdownEl.classList.remove('form-invalid');
      }
    }, {
      key: "focus",
      value: function focus() {
        this.btnMultiselectEl.focus();
      }
    }]);
  }(SkSelectImpl);

  function _callSuper$n(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$n() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$n() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$n = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkSelect = /*#__PURE__*/function (_SkElement) {
    function SkSelect() {
      _classCallCheck(this, SkSelect);
      return _callSuper$n(this, SkSelect, arguments);
    }
    _inherits(SkSelect, _SkElement);
    return _createClass(SkSelect, [{
      key: "cnSuffix",
      get: function get() {
        return 'select';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "valueType",
      get: function get() {
        return this.getAttribute('value-type');
      },
      set: function set(valueType) {
        this.setAttribute('value-type', valueType);
      }
    }, {
      key: "value",
      get: function get() {
        return this.getAttribute('value');
      },
      set: function set(value) {
        if (this.valueType === 'native') {
          if (Array.isArray(value)) {
            if (value[0]) {
              this.setAttribute('value', value[0]);
            } else {
              this.removeAttribute('value');
            }
          } else if (_typeof(value) === 'object') {
            var keys = Object.keys(value);
            if (value[keys[0]]) {
              this.setAttribute('value', value[keys[0]]);
            } else {
              this.removeAttribute('value');
            }
          }
        } else {
          if (value === null) {
            this.removeAttribute('value');
          } else {
            this.setAttribute('value', value);
          }
        }
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['body'];
      }
    }, {
      key: "body",
      get: function get() {
        if (!this._body) {
          this._body = this.el.querySelector('select');
        }
        return this._body;
      },
      set: function set(el) {
        this._body = el;
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'disabled' && oldValue !== newValue) {
          if (newValue) {
            this.impl.disable();
          } else {
            this.impl.enable();
          }
        }
        if (name === 'form-invalid') {
          if (newValue !== null) {
            this.impl.showInvalid();
          } else {
            this.impl.showValid();
          }
        }
        if (name === 'value' && oldValue !== newValue) {
          if (this.implRenderTimest) {
            this.impl.selectOptionByValue.call(this.impl, newValue);
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }, {
      key: "validity",
      get: function get() {
        if (this.hasAttribute('required')) {
          var value = this.getAttribute('value');
          if (!value || value === '') {
            return {
              valid: false,
              valueMissing: true
            };
          }
        }
        return {
          valid: true
        };
      }
    }, {
      key: "flushValidity",
      value: function flushValidity() {
        this.impl.flushValidation();
      }
    }, {
      key: "selectedValues",
      get: function get() {
        if (!this._selectedValues) {
          var valuesRaw = this.getAttribute('value');
          try {
            var values = JSON.parse(valuesRaw);
            this._selectedValues = values !== null ? values : [];
          } catch (_unused) {
            this._selectedValues = valuesRaw !== null && valuesRaw !== '' ? [valuesRaw] : [];
          }
        }
        this.value = this._selectedValues;
        return this._selectedValues;
      },
      set: function set(values) {
        this._selectedValues = values;
        this.value = values;
      }
    }, {
      key: "selectValue",
      value: function selectValue(value) {
        var _this = this;
        var values = this.selectedValues;
        if (Array.isArray(values) && !values.find(function (item) {
          _newArrowCheck(this, _this);
          return item === value;
        }.bind(this))) {
          values.push(value);
        } else {
          values[value] = value;
        }
        this.selectedValues = values;
      }
    }, {
      key: "selectedOptions",
      value: function selectedOptions() {
        return this.body ? this.body.selectedOptions : [];
      }
    }, {
      key: "isSelected",
      value: function isSelected(value) {
        var _this2 = this;
        var values = this.selectedValues;
        if (Array.isArray(values)) {
          return values.find(function (item) {
            _newArrowCheck(this, _this2);
            return item === value;
          }.bind(this));
        } else if (values != null && value !== undefined && value !== '') {
          return values[value];
        } else {
          return false;
        }
      }
    }, {
      key: "deSelectValue",
      value: function deSelectValue(value) {
        var _this3 = this;
        var values = this.selectedValues;
        if (Array.isArray(values)) {
          values = values.filter(function (item) {
            _newArrowCheck(this, _this3);
            return item !== value;
          }.bind(this));
        } else {
          delete values[value];
        }
        this.selectedValues = values;
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['disabled', 'required', 'form-invalid', 'value'];
      }
    }]);
  }(SkElement);

  var SPINNER_SHOW_EVT = 'spinner-show';

  var SPINNER_HIDE_EVT = 'spinner-hide';

  var SPINNER_TOGGLE_EVT = 'spinner-toggle';

  function _callSuper$m(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$m() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$m() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$m = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkSpinnerImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkSpinnerImpl() {
      _classCallCheck(this, SkSpinnerImpl);
      return _callSuper$m(this, SkSpinnerImpl, arguments);
    }
    _inherits(SkSpinnerImpl, _SkComponentImpl);
    return _createClass(SkSpinnerImpl, [{
      key: "suffix",
      get: function get() {
        return 'spinner';
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        if (this.showHandle) {
          this.comp.removeEventListener('show', this.showHandle);
          this.comp.removeEventListener(SPINNER_SHOW_EVT, this.showHandle);
        }
        this.showHandle = this.comp.show.bind(this.comp);
        this.comp.addEventListener('show', this.showHandle);
        this.comp.addEventListener(SPINNER_SHOW_EVT, this.showHandle);
        if (this.hideHandle) {
          this.comp.removeEventListener('hide', this.hideHandle);
          this.comp.removeEventListener(SPINNER_HIDE_EVT, this.hideHandle);
        }
        this.hideHandle = this.comp.hide.bind(this.comp);
        this.comp.addEventListener('hide', this.hideHandle);
        this.comp.addEventListener(SPINNER_HIDE_EVT, this.showHandle);
        if (this.toggleHandle) {
          this.comp.removeEventListener('toggle', this.toggleHandle);
          this.comp.removeEventListener(SPINNER_TOGGLE_EVT, this.toggleHandle);
        }
        this.toggleHandle = this.comp.toggle.bind(this.comp);
        this.comp.addEventListener('toggle', this.toggleHandle);
        this.comp.addEventListener(SPINNER_TOGGLE_EVT, this.toggleHandle);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        this.comp.removeEventListener('show', this.showHandle);
        this.comp.removeEventListener(SPINNER_SHOW_EVT, this.showHandle);
        this.comp.removeEventListener('hide', this.hideHandle);
        this.comp.removeEventListener(SPINNER_HIDE_EVT, this.hideHandle);
        this.comp.removeEventListener('toggle', this.toggleHandle);
        this.comp.removeEventListener(SPINNER_TOGGLE_EVT, this.toggleHandle);
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$l(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$l() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$l() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$l = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$b(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkSpinner = /*#__PURE__*/function (_SkSpinnerImpl) {
    function JquerySkSpinner() {
      _classCallCheck(this, JquerySkSpinner);
      return _callSuper$l(this, JquerySkSpinner, arguments);
    }
    _inherits(JquerySkSpinner, _SkSpinnerImpl);
    return _createClass(JquerySkSpinner, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'spinner';
      }
    }, {
      key: "spinnerImagePath",
      get: function get() {
        if (!this._spinnerImagePath) {
          this._spinnerImagePath = this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path') ? "/".concat(this.comp.configEl.getAttribute('tpl-path'), "/loading_spinner.gif") : "/node_modules/sk-theme-jquery/loading_spinner.gif";
        }
        return this._spinnerImagePath;
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$b(JquerySkSpinner, "afterRendered", this, 3)([]);
        this.mountStyles();
        this.comp.el.querySelector('.spinner').style.backgroundImage = "url(".concat(this.spinnerImagePath, ")");
      }
    }]);
  }(SkSpinnerImpl);

  function _callSuper$k(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$k() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$k() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$k = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkSpinner = /*#__PURE__*/function (_SkElement) {
    function SkSpinner() {
      _classCallCheck(this, SkSpinner);
      return _callSuper$k(this, SkSpinner, arguments);
    }
    _inherits(SkSpinner, _SkElement);
    return _createClass(SkSpinner, [{
      key: "cnSuffix",
      get: function get() {
        return 'spinner';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "display",
      get: function get() {
        return this.getAttribute('display') || 'none';
      },
      set: function set(display) {
        this.setAttribute('display', display);
        if (display === 'none') {
          this.hide();
        } else {
          this.show();
        }
      }
    }, {
      key: "show",
      value: function show() {
        this.style.display = 'inline-block';
      }
    }, {
      key: "hide",
      value: function hide() {
        this.style.display = 'none';
      }
    }, {
      key: "toggle",
      value: function toggle() {
        if (this.display === 'none') {
          this.display = 'inline-block';
          this.show();
        } else {
          this.display = 'none';
          this.hide();
        }
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['display'];
      }
    }]);
  }(SkElement);

  function _callSuper$j(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$j() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$j() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$j = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$a(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SW_FORWARDED_ATTRS = {
    'disabled': 'disabled',
    'checked': 'checked',
    'required': 'required'
  };
  var SkSwitchImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkSwitchImpl() {
      _classCallCheck(this, SkSwitchImpl);
      return _callSuper$j(this, SkSwitchImpl, arguments);
    }
    _inherits(SkSwitchImpl, _SkComponentImpl);
    return _createClass(SkSwitchImpl, [{
      key: "suffix",
      get: function get() {
        return 'switch';
      }
    }, {
      key: "input",
      get: function get() {
        return this.comp.el.querySelector('input');
      }
    }, {
      key: "dumpState",
      value: function dumpState() {
        return {
          'contents': this.comp.contentsState
        };
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        _superPropGet$a(SkSwitchImpl, "restoreState", this, 3)([state]);
        this.comp._inputEl = null;
        this.forwardAttributes();
        this.bindEvents();
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {}
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$a(SkSwitchImpl, "afterRendered", this, 3)([]);
        this.restoreState({
          contentsState: this.contentsState || this.comp.contentsState
        });
      }
    }, {
      key: "forwardAttributes",
      value: function forwardAttributes() {
        if (this.input) {
          for (var _i = 0, _Object$keys = Object.keys(SW_FORWARDED_ATTRS); _i < _Object$keys.length; _i++) {
            var attrName = _Object$keys[_i];
            var value = this.comp.getAttribute(attrName);
            if (value) {
              this.input.setAttribute(attrName, value);
            } else {
              if (this.comp.hasAttribute(attrName)) {
                this.input.setAttribute(attrName, '');
              }
            }
          }
        }
      }
    }, {
      key: "enable",
      value: function enable() {
        _superPropGet$a(SkSwitchImpl, "enable", this, 3)([]);
      }
    }, {
      key: "disable",
      value: function disable() {
        _superPropGet$a(SkSwitchImpl, "disable", this, 3)([]);
      }
    }, {
      key: "check",
      value: function check() {}
    }, {
      key: "uncheck",
      value: function uncheck() {}
    }, {
      key: "onClick",
      value: function onClick(event) {
        if (this.comp.getAttribute('disabled')) {
          return false;
        }
        if (!this.comp.getAttribute('checked')) {
          this.comp.setAttribute('checked', 'checked');
        } else {
          this.comp.removeAttribute('checked');
        }
        var result = this.validateWithAttrs();
        if (typeof result === 'string') {
          this.showInvalid();
        } else {
          this.showValid();
        }
      }
    }, {
      key: "skValidators",
      get: function get() {
        if (!this._skValidators) {
          this._skValidators = {
            'sk-required': new SkRequired()
          };
        }
        return this._skValidators;
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        _superPropGet$a(SkSwitchImpl, "showInvalid", this, 3)([]);
        this.inputEl.classList.add('form-invalid');
      }
    }, {
      key: "showValid",
      value: function showValid() {
        _superPropGet$a(SkSwitchImpl, "showValid", this, 3)([]);
        this.inputEl.classList.remove('form-invalid');
      }
    }, {
      key: "focus",
      value: function focus() {
        var _this = this;
        setTimeout(function () {
          _newArrowCheck(this, _this);
          this.comp.inputEl.focus();
        }.bind(this), 0);
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$i(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$i() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$i() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$i = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$9(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkSwitch = /*#__PURE__*/function (_SkSwitchImpl) {
    function JquerySkSwitch() {
      _classCallCheck(this, JquerySkSwitch);
      return _callSuper$i(this, JquerySkSwitch, arguments);
    }
    _inherits(JquerySkSwitch, _SkSwitchImpl);
    return _createClass(JquerySkSwitch, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'switch';
      }
    }, {
      key: "inputEl",
      get: function get() {
        if (!this._inputEl) {
          this._inputEl = this.comp.el.querySelector('input');
        }
        return this._inputEl;
      },
      set: function set(el) {
        this._inputEl = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['inputEl'];
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$9(JquerySkSwitch, "bindEvents", this, 3)([]);
        this.comp.onclick = function (event) {
          this.comp.callPluginHook('onEventStart', event);
          this.onClick(event);
          this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$9(JquerySkSwitch, "unbindEvents", this, 3)([]);
        this.comp.onclick = null;
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$9(JquerySkSwitch, "afterRendered", this, 3)([]);
        this.mountStyles();
      }
    }, {
      key: "enable",
      value: function enable() {
        _superPropGet$9(JquerySkSwitch, "enable", this, 3)([]);
        this.inputEl.removeAttribute('disabled');
      }
    }, {
      key: "disable",
      value: function disable() {
        _superPropGet$9(JquerySkSwitch, "disable", this, 3)([]);
        this.inputEl.setAttribute('disabled', 'disabled');
      }
    }, {
      key: "check",
      value: function check() {
        _superPropGet$9(JquerySkSwitch, "check", this, 3)([]);
        this.inputEl.setAttribute('checked', 'checked');
      }
    }, {
      key: "uncheck",
      value: function uncheck() {
        _superPropGet$9(JquerySkSwitch, "check", this, 3)([]);
        this.inputEl.removeAttribute('checked');
      }
    }]);
  }(SkSwitchImpl);

  function _callSuper$h(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$h() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$h() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$h = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkSwitch = /*#__PURE__*/function (_SkElement) {
    function SkSwitch() {
      _classCallCheck(this, SkSwitch);
      return _callSuper$h(this, SkSwitch, arguments);
    }
    _inherits(SkSwitch, _SkElement);
    return _createClass(SkSwitch, [{
      key: "cnSuffix",
      get: function get() {
        return 'switch';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "inputEl",
      get: function get() {
        if (!this._inputEl && this.el) {
          this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
      },
      set: function set(el) {
        this._inputEl = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['inputEl'];
      }
    }, {
      key: "checked",
      get: function get() {
        return this.getAttribute('checked');
      },
      set: function set(checked) {
        this.setAttribute('checked', checked);
      }
    }, {
      key: "value",
      get: function get() {
        if (this.getAttribute('checked')) {
          return this.getAttribute('value');
        } else {
          return null;
        }
      },
      set: function set(value) {
        if (value === null) {
          this.removeAttribute('value');
        } else {
          this.setAttribute('value', value);
        }
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (!this.inputEl) {
          return false;
        }
        if (name === 'disabled' && oldValue !== newValue) {
          if (newValue) {
            this.impl.disable();
          } else {
            this.impl.enable();
          }
        }
        if (name === 'checked') {
          if (newValue) {
            this.impl.check();
          } else {
            this.impl.uncheck();
          }
          this.dispatchEvent(new CustomEvent('change', {
            target: this,
            detail: {
              value: this.getAttribute('value'),
              checked: newValue
            },
            bubbles: true,
            composed: true
          }));
          this.dispatchEvent(new CustomEvent('skchange', {
            target: this,
            detail: {
              value: this.getAttribute('value'),
              checked: newValue
            },
            bubbles: true,
            composed: true
          }));
        }
        if (name === 'form-invalid') {
          if (newValue !== null) {
            this.impl.showInvalid();
          } else {
            this.impl.showValid();
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }, {
      key: "validity",
      get: function get() {
        if (this.hasAttribute('required')) {
          if (this.hasAttribute('checked')) {
            return {
              valid: true,
              valueMissing: true
            };
          }
        }
        return {
          valid: false
        };
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['disabled', 'checked', 'form-invalid', 'required'];
      }
    }]);
  }(SkElement);

  function _callSuper$g(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$g() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$g() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$g = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$8(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  function _createForOfIteratorHelper$6(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$6(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$6(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$6(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$6(r, a) : void 0; } }
  function _arrayLikeToArray$6(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  var SkSltabsUtil = /*#__PURE__*/function () {
    function SkSltabsUtil() {
      _classCallCheck(this, SkSltabsUtil);
    }
    return _createClass(SkSltabsUtil, [{
      key: "setInitialAttrs",
      value: function setInitialAttrs() {
        this.setAttribute('role', 'tablist');
        var _iterator = _createForOfIteratorHelper$6(this.panels.entries()),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var _step$value = _slicedToArray(_step.value, 2),
              i = _step$value[0],
              panel = _step$value[1];
            panel.setAttribute('role', 'tabpanel');
            panel.setAttribute('tabindex', 0);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        this.selected = this.findFirstSelectedTab() || 0;
      }
    }, {
      key: "bindTabEvents",
      value: function bindTabEvents() {
        this.titleClickHandle = this.onTitleClick.bind(this);
        this.keyDownHandle = this.onKeyDown.bind(this);
        this.tabsSlot.addEventListener('click', this.titleClickHandle);
        this.tabsSlot.addEventListener('keydown', this.keyDownHandle);
      }
    }, {
      key: "unbindTabEvents",
      value: function unbindTabEvents() {
        if (this.titleClickHandle) {
          this.tabsSlot.removeEventListener('click', this.titleClickHandle);
        }
        if (this.keyDownHandle) {
          this.tabsSlot.removeEventListener('keydown', this.keyDownHandle);
        }
      }
    }, {
      key: "onTitleClick",
      value: function onTitleClick(e) {
        if (e.target.slot === 'title' && !this.disabled && !e.target.hasAttribute(DISABLED_AN)) {
          this.selected = this.tabs.indexOf(e.target);
          e.target.focus();
        }
      }
    }, {
      key: "onKeyDown",
      value: function onKeyDown(e) {
        switch (e.code) {
          case 'ArrowUp':
          case 'ArrowLeft':
            e.preventDefault();
            var idx = this.selected - 1;
            idx = idx < 0 ? this.tabs.length - 1 : idx;
            this.tabs[idx].click();
            break;
          case 'ArrowDown':
          case 'ArrowRight':
            e.preventDefault();
            var idx = this.selected + 1;
            this.tabs[idx % this.tabs.length].click();
            break;
        }
      }
    }, {
      key: "findFirstSelectedTab",
      value: function findFirstSelectedTab() {
        var selectedIdx;
        var _iterator2 = _createForOfIteratorHelper$6(this.tabs.entries()),
          _step2;
        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var _step2$value = _slicedToArray(_step2.value, 2),
              i = _step2$value[0],
              tab = _step2$value[1];
            tab.setAttribute('role', 'tab');
            if (tab.hasAttribute(SELECTED_AN)) {
              selectedIdx = i;
            }
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
        return selectedIdx;
      }
    }, {
      key: "selectTab",
      value: function selectTab() {
        var idx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        for (var i = 0, tab; tab = this.tabs[i]; ++i) {
          var select = i === idx;
          tab.setAttribute('tabindex', select ? 0 : -1);
          tab.setAttribute('aria-selected', select);
          this.panels[i].setAttribute('aria-hidden', !select);
        }
      }
    }]);
  }();
  var SkSltabs = /*#__PURE__*/function (_SkElement) {
    function SkSltabs() {
      _classCallCheck(this, SkSltabs);
      return _callSuper$g(this, SkSltabs, arguments);
    }
    _inherits(SkSltabs, _SkElement);
    return _createClass(SkSltabs, [{
      key: "cnSuffix",
      get: function get() {
        return 'sltabs';
      }
    }, {
      key: "implPath",
      get: function get() {
        return this.getAttribute(IMPL_PATH_AN) || "/node_modules/sk-tabs-".concat(this.theme, "/src/").concat(this.theme, "-sk-").concat(this.cnSuffix, ".js");
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "utilClassName",
      get: function get() {
        return 'SkSltabsUtil';
      }
    }, {
      key: "utilPath",
      get: function get() {
        return '/node_modules/sk-tabs/src/sk-sltabs.js';
      }
    }, {
      key: "slUtil",
      get: function get() {
        if (!this._slUtil) {
          window.SkSltabsUtil = SkSltabsUtil;
          this._slUtil = ResLoader.dynLoad(this.utilClassName, this.utilPath, function (def) {
            return new def();
          }.bind(this), false, false, false, false, false, false, true, true);
        }
        return this._slUtil;
      }
    }, {
      key: "selected",
      get: function get() {
        return this._selected;
      },
      set: function set(idx) {
        this._selected = idx;
        this.selectTab(idx);
        this.setAttribute(SELECTED_AN, idx);
      }
    }, {
      key: "tabsSlot",
      get: function get() {
        return this.el.querySelector('#tabsSlot');
      }
    }, {
      key: "panelsSlot",
      get: function get() {
        return this.el.querySelector('#panelsSlot');
      }
    }, {
      key: "tabs",
      get: function get() {
        return this.tabsSlot.assignedNodes({
          flatten: true
        });
      }
    }, {
      key: "panels",
      get: function get() {
        var _this = this;
        return this.panelsSlot.assignedNodes({
          flatten: true
        }).filter(function (el) {
          _newArrowCheck(this, _this);
          return el.nodeType === Node.ELEMENT_NODE;
        }.bind(this));
      }
    }, {
      key: "tabTn",
      get: function get() {
        return this.getAttribute(TAB_TN_AN) || TAB_TN;
      },
      set: function set(tabTn) {
        return this.setAttribute(TAB_TN_AN, tabTn);
      }
    }, {
      key: "tabSl",
      get: function get() {
        return this.tabTn;
      }
    }, {
      key: "disabled",
      get: function get() {
        return this.hasAttribute(DISABLED_AN);
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;
        _superPropGet$8(SkSltabs, "render", this, 3)([]);
        this.whenRendered(function () {
          var _this3 = this;
          _newArrowCheck(this, _this2);
          this.setInitialAttrs();
          setTimeout(function () {
            _newArrowCheck(this, _this3);
            this.unbindTabEvents();
            this.bindTabEvents();
          }.bind(this), 0);
        }.bind(this));
      }
    }, {
      key: "disconnectedCallback",
      value: function disconnectedCallback() {
        this.unbindTabEvents();
      }
    }, {
      key: "setInitialAttrs",
      value: function setInitialAttrs() {
        return this.slUtil.setInitialAttrs.call(this);
      }
    }, {
      key: "bindTabEvents",
      value: function bindTabEvents() {
        return this.slUtil.bindTabEvents.call(this);
      }
    }, {
      key: "unbindTabEvents",
      value: function unbindTabEvents() {
        return this.slUtil.unbindTabEvents.call(this);
      }
    }, {
      key: "onTitleClick",
      value: function onTitleClick(e) {
        return this.slUtil.onTitleClick.call(this, e);
      }
    }, {
      key: "onKeyDown",
      value: function onKeyDown(e) {
        return this.slUtil.onKeyDown.call(this, e);
      }
    }, {
      key: "findFirstSelectedTab",
      value: function findFirstSelectedTab() {
        return this.slUtil.findFirstSelectedTab.call(this);
      }
    }, {
      key: "selectTab",
      value: function selectTab() {
        var idx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        return this.slUtil.selectTab.call(this, idx);
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === DISABLED_AN && oldValue !== newValue) {
          if (newValue) {
            if (this.impl && typeof this.impl.disable === 'function') {
              this.impl.disable();
            }
          } else {
            if (this.impl && typeof this.impl.enable === 'function') {
              this.impl.enable();
            }
          }
        }
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return [DISABLED_AN];
      }
    }]);
  }(SkElement);

  function _callSuper$f(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$f() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$f() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$f = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$7(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var TAB_TN = "sk-tab";
  var TITLE_AN = "title";
  var OPEN_AN = "open";
  var TAB_TN_AN = "tab-tn";
  var SLOTTED_TPLS_AN = "slotted-tpls";
  var SkTabs = /*#__PURE__*/function (_SkElement) {
    function SkTabs() {
      _classCallCheck(this, SkTabs);
      return _callSuper$f(this, SkTabs, arguments);
    }
    _inherits(SkTabs, _SkElement);
    return _createClass(SkTabs, [{
      key: "cnSuffix",
      get: function get() {
        return 'tabs';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "selected",
      get: function get() {
        return this._selected;
      },
      set: function set(idx) {
        this._selected = idx;
        this.selectTab(idx);
        this.setAttribute(SELECTED_AN, idx);
      }
    }, {
      key: "tabsSlot",
      get: function get() {
        return this.el.querySelector('#tabsSlot');
      }
    }, {
      key: "panelsSlot",
      get: function get() {
        return this.el.querySelector('#panelsSlot');
      }
    }, {
      key: "tabs",
      get: function get() {
        return this.tabsSlot.assignedNodes({
          flatten: true
        });
      }
    }, {
      key: "panels",
      get: function get() {
        var _this = this;
        return this.panelsSlot.assignedNodes({
          flatten: true
        }).filter(function (el) {
          _newArrowCheck(this, _this);
          return el.nodeType === Node.ELEMENT_NODE;
        }.bind(this));
      }
    }, {
      key: "tabTn",
      get: function get() {
        return this.getAttribute(TAB_TN_AN) || TAB_TN;
      },
      set: function set(tabTn) {
        return this.setAttribute(TAB_TN_AN, tabTn);
      }
    }, {
      key: "tabSl",
      get: function get() {
        return this.tabTn;
      }
    }, {
      key: "disabled",
      get: function get() {
        return this.hasAttribute(DISABLED_AN);
      }
    }, {
      key: "utilClassName",
      get: function get() {
        return 'SkSltabsUtil';
      }
    }, {
      key: "utilPath",
      get: function get() {
        return '/node_modules/sk-tabs/src/sk-sltabs.js';
      }
    }, {
      key: "slUtil",
      get: function get() {
        if (!this._slUtil) {
          if (!window.SkSltabsUtil) {
            window.SkSltabsUtil = SkSltabsUtil;
          }
          this._slUtil = ResLoader.dynLoad(this.utilClassName, this.utilPath, function (def) {
            return new def();
          }.bind(this), false, false, false, false, false, false, true, true);
        }
        return this._slUtil;
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === DISABLED_AN && oldValue !== newValue) {
          if (newValue) {
            if (this.impl && typeof this.impl.disable === 'function') {
              this.impl.disable();
            }
          } else {
            if (this.impl && typeof this.impl.enable === 'function') {
              this.impl.enable();
            }
          }
        }
      }
    }, {
      key: "isTplSlotted",
      get: function get() {
        var cfgSlotted = this.confValOrDefault(SLOTTED_TPLS_AN, "false"); // no slots by default
        if (this.isInIE() || cfgSlotted === "false") {
          return false;
        }
        return true;
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;
        if (!this.isTplSlotted) {
          _superPropGet$7(SkTabs, "render", this, 3)([]);
        } else {
          _superPropGet$7(SkTabs, "render", this, 3)([]);
          this.whenRendered(function () {
            var _this3 = this;
            _newArrowCheck(this, _this2);
            this.setInitialAttrs();
            setTimeout(function () {
              _newArrowCheck(this, _this3);
              this.unbindTabEvents();
              this.bindTabEvents();
            }.bind(this), 0);
          }.bind(this));
        }
      }
    }, {
      key: "disconnectedCallback",
      value: function disconnectedCallback() {
        this.unbindTabEvents();
      }
    }, {
      key: "setInitialAttrs",
      value: function setInitialAttrs() {
        return this.slUtil.setInitialAttrs.call(this);
      }
    }, {
      key: "bindTabEvents",
      value: function bindTabEvents() {
        return this.slUtil.bindTabEvents.call(this);
      }
    }, {
      key: "unbindTabEvents",
      value: function unbindTabEvents() {
        return this.slUtil.unbindTabEvents.call(this);
      }
    }, {
      key: "onTitleClick",
      value: function onTitleClick(e) {
        return this.slUtil.onTitleClick.call(this, e);
      }
    }, {
      key: "onKeyDown",
      value: function onKeyDown(e) {
        return this.slUtil.onKeyDown.call(this, e);
      }
    }, {
      key: "findFirstSelectedTab",
      value: function findFirstSelectedTab() {
        return this.slUtil.findFirstSelectedTab.call(this);
      }
    }, {
      key: "selectTab",
      value: function selectTab() {
        var idx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        return this.slUtil.selectTab.call(this, idx);
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return [DISABLED_AN];
      }
    }]);
  }(SkElement);

  function _createForOfIteratorHelper$5(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$5(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$5(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$5(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$5(r, a) : void 0; } }
  function _arrayLikeToArray$5(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$e(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$e() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$e() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$e = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$6(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkSltabsImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkSltabsImpl() {
      _classCallCheck(this, SkSltabsImpl);
      return _callSuper$e(this, SkSltabsImpl, arguments);
    }
    _inherits(SkSltabsImpl, _SkComponentImpl);
    return _createClass(SkSltabsImpl, [{
      key: "suffix",
      get: function get() {
        return 'sltabs';
      }
    }, {
      key: "tplPath",
      get: function get() {
        if (!this._tplPath) {
          this._tplPath = this.comp.tplPath || this.comp.configEl && this.comp.configEl.hasAttribute(TPL_PATH_AN) ? "".concat(this.comp.configEl.getAttribute(TPL_PATH_AN), "/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html") : "/node_modules/sk-tabs-".concat(this.prefix, "/src/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html");
        }
        return this._tplPath;
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$6(SkSltabsImpl, "beforeRendered", this, 3)([]);
        this.importTabEls();
      }
    }, {
      key: "importTabEls",
      value: function importTabEls() {
        var tabEls = this.comp.querySelectorAll(this.comp.tabSl);
        if (tabEls && tabEls.length > 0) {
          this.renderTabs(tabEls);
        }
      }
    }, {
      key: "renderTabs",
      value: function renderTabs(tabEls) {
        var tabs = tabEls || this.tabsEl.querySelectorAll(this.comp.tabSl);
        var num = 1;
        this.tabs = {};
        var _iterator = _createForOfIteratorHelper$5(tabs),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var tab = _step.value;
            var isOpen = tab.hasAttribute(OPEN_AN);
            var title = tab.getAttribute(TITLE_AN) ? tab.getAttribute(TITLE_AN) : '';
            var disabled = tab.hasAttribute(DISABLED_AN);
            var selected = tab.hasAttribute(SELECTED_AN) || tab.hasAttribute(OPEN_AN);
            this.comp.insertAdjacentHTML('beforeend', "\n                <h2 slot=\"title\" ".concat(disabled ? 'disabled' : '', " ").concat(selected ? 'selected' : '', ">").concat(title, "</h2>\n                <section id=\"tabs-").concat(num, "\">").concat(tab.innerHTML, "</section>\n            "));
            this.removeEl(tab);
            this.tabs['tabs-' + num] = this.comp.querySelector('#tabs-' + num);
            num++;
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$d(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$d() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$d() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$d = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$5(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkTabsImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkTabsImpl() {
      _classCallCheck(this, SkTabsImpl);
      return _callSuper$d(this, SkTabsImpl, arguments);
    }
    _inherits(SkTabsImpl, _SkComponentImpl);
    return _createClass(SkTabsImpl, [{
      key: "tplPath",
      get: function get() {
        if (!this._tplPath) {
          if (this.comp.isTplSlotted) {
            var suffix = 'sltabs';
            this._tplPath = this.comp.tplPath || this.comp.configEl && this.comp.configEl.hasAttribute(TPL_PATH_AN) ? "".concat(this.comp.configEl.getAttribute(TPL_PATH_AN), "/").concat(this.prefix, "-sk-").concat(suffix, ".tpl.html") : "/node_modules/sk-tabs-".concat(this.prefix, "/src/").concat(this.prefix, "-sk-").concat(suffix, ".tpl.html");
          } else {
            this._tplPath = this.comp.tplPath || this.comp.configEl && this.comp.configEl.hasAttribute(TPL_PATH_AN) ? "".concat(this.comp.configEl.getAttribute(TPL_PATH_AN), "/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html") : "/node_modules/sk-tabs-".concat(this.prefix, "/src/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html");
          }
        }
        return this._tplPath;
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'tabs';
      }
    }, {
      key: "slImplInst",
      get: function get() {
        if (!this._slImplInst) {
          this._slImplInst = new SkSltabsImpl();
        }
        return this._slImplInst;
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$5(SkTabsImpl, "beforeRendered", this, 3)([]);
        if (this.comp.isTplSlotted) {
          return this.slImplInst.importTabEls.call(this);
        } else {
          this.saveState();
        }
      }
    }, {
      key: "renderSlottedTabs",
      value: function renderSlottedTabs(tabEls) {
        return this.slImplInst.renderTabs.call(this, tabEls);
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this._tabsEl = null;
        this._tabsTitleBarEl = null;
        this._titleContentEl = null;
        this.tabsContentEl.innerHTML = '';
        this.titleBarEl.innerHTML = '';
        this.tabsContentEl.insertAdjacentHTML('beforeend', state.contentsState);
        this.renderTabs();
        this.bindTabSwitch();
        if (!this.titleBarEl.querySelector("[".concat(OPEN_AN, "]"))) {
          this.updateTabs('tabs-1');
        }
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$5(SkTabsImpl, "afterRendered", this, 3)([]);
        this.restoreState({
          contentsState: this.contentsState || this.comp.contentsState
        });
      }
    }, {
      key: "cachedTplId",
      get: function get() {
        if (this.comp.isTplSlotted) {
          return 'SkSltabsTpl';
        } else {
          return this.comp.constructor.name + 'Tpl';
        }
      }
    }]);
  }(SkComponentImpl);

  function _createForOfIteratorHelper$4(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$4(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$4(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$4(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$4(r, a) : void 0; } }
  function _arrayLikeToArray$4(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$c(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$c() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$c() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$c = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$4(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var JquerySkTabs = /*#__PURE__*/function (_SkTabsImpl) {
    function JquerySkTabs() {
      _classCallCheck(this, JquerySkTabs);
      return _callSuper$c(this, JquerySkTabs, arguments);
    }
    _inherits(JquerySkTabs, _SkTabsImpl);
    return _createClass(JquerySkTabs, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'tabs';
      }
    }, {
      key: "tabsEl",
      get: function get() {
        if (!this._tabsEl) {
          this._tabsEl = this.comp.el.querySelector('div#tabs');
        }
        return this._tabsEl;
      },
      set: function set(el) {
        this._tabsEl = el;
      }
    }, {
      key: "titleBarEl",
      get: function get() {
        if (!this._tabsTitleBarEl) {
          this._tabsTitleBarEl = this.tabsEl.querySelector('ul');
        }
        return this._tabsTitleBarEl;
      },
      set: function set(el) {
        this._tabsTitleBarEl = el;
      }
    }, {
      key: "tabsContentEl",
      get: function get() {
        if (!this._titleContentEl) {
          this._titleContentEl = this.tabsEl.querySelector('.tabs-contents');
        }
        return this._titleContentEl;
      },
      set: function set(el) {
        this._titleContentEl = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['tabsEl', 'titleBarEl', 'tabsContentEl'];
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        if (!this.comp.isTplSlotted) {
          this._tabsEl = null;
          this._tabsTitleBarEl = null;
          this._titleContentEl = null;
          this.tabsContentEl.innerHTML = '';
          this.titleBarEl.innerHTML = '';
          this.tabsContentEl.insertAdjacentHTML('beforeend', state.contentsState);
          this.renderTabs();
          this.bindTabSwitch();
          if (!this.titleBarEl.querySelector('[open]')) {
            this.updateTabs('tabs-1');
          }
        }
      }
    }, {
      key: "updateTabs",
      value: function updateTabs(curTabId) {
        for (var _i = 0, _Object$keys = Object.keys(this.tabs); _i < _Object$keys.length; _i++) {
          var tabId = _Object$keys[_i];
          var tab = this.tabs[tabId];
          var tabEl = this.titleBarEl.querySelector("#header-".concat(tab.dataset.tabId));
          if (tabId === curTabId) {
            tabEl.classList.add('ui-tabs-active', 'ui-state-active');
            this.tabs[tabId].style.display = 'block';
          } else {
            tabEl.classList.remove('ui-tabs-active', 'ui-state-active');
            this.tabs[tabId].style.display = 'none';
          }
        }
      }
    }, {
      key: "renderTabs",
      value: function renderTabs(tabEls) {
        if (this.comp.isTplSlotted) {
          this.renderSlottedTabs(tabEls);
        } else {
          //this.tabsEl.insertAdjacentHTML('beforeend', this.contentsState || this.comp.contentsState);
          var tabs = tabEls || this.tabsEl.querySelectorAll(this.comp.tabSl);
          var num = 1;
          this.tabs = {};
          var _iterator = _createForOfIteratorHelper$4(tabs),
            _step;
          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var tab = _step.value;
              var isOpen = tab.hasAttribute(OPEN_AN);
              var title = tab.getAttribute(TITLE_AN) ? tab.getAttribute(TITLE_AN) : '';
              this.titleBarEl.insertAdjacentHTML('beforeend', "\n                    <li role=\"tab\" tabindex=\"0\" class=\"ui-tabs-tab ui-corner-top ".concat(tab.hasAttribute(DISABLED_AN) ? 'ui-state-disabled' : 'ui-state-default', " ui-tab ").concat(isOpen ? 'ui-tabs-active ui-state-active' : '', "\" \n                        aria-controls=\"tabs-").concat(num, "\" aria-labelledby=\"ui-id-").concat(num, "\" aria-selected=\"true\" aria-expanded=\"true\" id=\"header-").concat(num, "\">\n                            <a href=\"#tabs-").concat(num, "\" role=\"presentation\" tabindex=\"-1\" class=\"ui-tabs-anchor\" id=\"ui-id-").concat(num, "\" ").concat(isOpen ? 'open' : '', ">\n                                ").concat(title, "\n                            </a>\n                    </li>"));
              this.tabsContentEl.insertAdjacentHTML('beforeend', "\n                    <div id=\"tabs-".concat(num, "\" aria-labelledby=\"ui-id-").concat(num, "\" role=\"tabpanel\" data-tab-id=\"").concat(num, "\"\n                        ").concat(!isOpen ? 'style="display: none;"' : '', " \n                        class=\"ui-tabs-panel ui-corner-bottom ui-widget-content\" aria-hidden=\"").concat(!isOpen ? "true" : "false", "\">\n                        ").concat(tab.outerHTML, "\n                    </div>\n                "));
              this.removeEl(tab);
              this.tabs['tabs-' + num] = this.tabsEl.querySelector('#tabs-' + num);
              num++;
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
      }
    }, {
      key: "bindTabSwitch",
      value: function bindTabSwitch() {
        this.titleBarEl.querySelectorAll('a').forEach(function (link) {
          link.onclick = function (event) {
            if (this.comp.hasAttribute(DISABLED_AN) || event.target.classList.contains('ui-state-disabled') || event.target.parentElement.classList.contains('ui-state-disabled')) {
              return false;
            }
            var tabId = event.target.getAttribute('href').substr(1);
            this.updateTabs(tabId);
          }.bind(this);
        }.bind(this));
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$4(JquerySkTabs, "afterRendered", this, 3)([]);
        this.mountStyles();
      }
    }, {
      key: "enable",
      value: function enable() {
        _superPropGet$4(JquerySkTabs, "enable", this, 3)([]);
        this.tabsEl.classList.remove('ui-state-disabled');
      }
    }, {
      key: "disable",
      value: function disable() {
        _superPropGet$4(JquerySkTabs, "disable", this, 3)([]);
        this.tabsEl.classList.add('ui-state-disabled');
      }
    }]);
  }(SkTabsImpl);

  function _callSuper$b(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$b() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$b() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$b = function _isNativeReflectConstruct() { return !!t; })(); }
  var JquerySkSltabs = /*#__PURE__*/function (_SkSltabsImpl) {
    function JquerySkSltabs() {
      _classCallCheck(this, JquerySkSltabs);
      return _callSuper$b(this, JquerySkSltabs, arguments);
    }
    _inherits(JquerySkSltabs, _SkSltabsImpl);
    return _createClass(JquerySkSltabs, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'sltabs';
      }
    }]);
  }(SkSltabsImpl);

  function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
  function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
  function _createForOfIteratorHelper$3(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$3(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$3(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$3(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$3(r, a) : void 0; } }
  function _arrayLikeToArray$3(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$a(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$a() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$a() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$a = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$3(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var COLLAPSED_CN = 'sk-tree-collapsed';
  var EXPANDED_CN = 'sk-tree-expanded';
  var SkTreeImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkTreeImpl() {
      _classCallCheck(this, SkTreeImpl);
      return _callSuper$a(this, SkTreeImpl, arguments);
    }
    _inherits(SkTreeImpl, _SkComponentImpl);
    return _createClass(SkTreeImpl, [{
      key: "suffix",
      get: function get() {
        return 'tree';
      }
    }, {
      key: "itemTplPath",
      get: function get() {
        if (!this._itemTplPath) {
          this._itemTplPath = this.comp.itemTplPath || this.comp.configEl && this.comp.configEl.hasAttribute('item-tpl-path') ? "/".concat(this.comp.configEl.getAttribute('item-tpl-path'), "/").concat(this.prefix, "-sk-").concat(this.suffix, "-item.tpl.html") : "/node_modules/sk-".concat(this.comp.cnSuffix, "-").concat(this.prefix, "/src/").concat(this.prefix, "-sk-").concat(this.suffix, "-item.tpl.html");
        }
        return this._itemTplPath;
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$3(SkTreeImpl, "beforeRendered", this, 3)([]);
        this.saveState();
      }
    }, {
      key: "slot",
      get: function get() {
        return this.comp.el.querySelector('slot');
      }
    }, {
      key: "rootTagName",
      get: function get() {
        if (this.comp.hasAttribute('root-tn')) {
          return this.comp.getAttribute('root-tn');
        } else {
          return 'ul';
        }
      }
    }, {
      key: "rootClassName",
      get: function get() {
        if (this.comp.hasAttribute('root-cn')) {
          return this.comp.getAttribute('root-cn');
        } else {
          return 'sk-tree-root';
        }
      }
    }, {
      key: "itemTagName",
      get: function get() {
        if (this.comp.hasAttribute('item-tn')) {
          return this.comp.getAttribute('item-tn');
        } else {
          return 'li';
        }
      }
    }, {
      key: "itemClassName",
      get: function get() {
        if (this.comp.hasAttribute('item-cn')) {
          return this.comp.getAttribute('item-cn');
        } else {
          return 'sk-tree-item';
        }
      }
    }, {
      key: "branchClassName",
      get: function get() {
        if (this.comp.hasAttribute('branch-cn')) {
          return this.comp.getAttribute('branch-cn');
        } else {
          return 'sk-tree-has-child';
        }
      }
    }, {
      key: "containerEl",
      get: function get() {
        if (!this._containerEl) {
          this._containerEl = this.comp.el.querySelector('.sk-tree');
        }
        return this._containerEl;
      },
      set: function set(el) {
        this._containerEl = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['containerEl'];
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this.slot ? this.slot.innerHTML = '' : this.containerEl.innerHTML = '';
        //this.slot.insertAdjacentHTML('beforeend', state.contentsState);
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _superPropGet$3(SkTreeImpl, "bindEvents", this, 3)([]);
        if (!this.onClickedHandler) {
          this.onClickedHandler = this.onClick.bind(this);
          this.comp.el.addEventListener('click', this.onClickedHandler);
        }
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _superPropGet$3(SkTreeImpl, "unbindEvents", this, 3)([]);
        if (this.onClickedHandler) {
          this.removeEventListener('click', this.onClickedHandler);
        }
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$3(SkTreeImpl, "afterRendered", this, 3)([]);
        this.unbindEvents();
        this.clearAllElCache();
        this.rendered = false;
        this.renderImpl();
        //this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
      }
    }, {
      key: "onClick",
      value: function onClick(event) {
        var el = event.target;
        if (el && el.tagName === 'LI') {
          if (el.classList.contains(COLLAPSED_CN)) {
            el.classList.remove(COLLAPSED_CN);
            el.classList.add(EXPANDED_CN);
          } else {
            if (this.comp._byParentId[el.dataset.id]) {
              el.classList.add(COLLAPSED_CN);
              el.classList.remove(EXPANDED_CN);
            }
          }
        }
      }
    }, {
      key: "doRender",
      value: function doRender() {
        var id = this.getOrGenId();
        this.renderWithVars(id);
        var rootUl = this.comp.renderer.createEl(this.rootTagName);
        rootUl.classList.add(this.rootClassName);
        var _iterator = _createForOfIteratorHelper$3(this.comp.rootItems),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var item = _step.value;
            var itemEl = this.renderItem(item);
            rootUl.appendChild(itemEl);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        if (this.selectedEl) {
          this.expandSubtree(this.selectedEl.parentElement);
        }
        if (this.slot) {
          this.slot.appendChild(rootUl);
        } else {
          if (this.containerEl) {
            this.containerEl.appendChild(rootUl);
          }
        }
        this.bindEvents();
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.rendered = true;
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', {
          bubbles: true,
          composed: true
        })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({
          bubbles: true,
          composed: true
        }));
        if (this.comp.renderDeferred) {
          this.comp.renderDeferred.resolve(this.comp);
        }
      }
    }, {
      key: "renderImpl",
      value: function renderImpl() {
        var _this = this;
        this.comp.treeData; // load and index data if not
        /*        if (this.rendered) {
                    return false;
                }*/
        var themeLoaded = this.initTheme();
        themeLoaded["finally"](function () {
          var _this2 = this;
          _newArrowCheck(this, _this);
          // we just need to try loading styles
          this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
          if (!this.comp.tpl) {
            var loadTpl = /*#__PURE__*/function () {
              var _ref = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
                return regenerator.wrap(function _callee$(_context) {
                  while (1) switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return _this2.comp.renderer.mountTemplate(_this2.tplPath, _this2.cachedTplId, _this2.comp, {
                        themePath: _this2.themePath
                      });
                    case 2:
                      _this2.comp.tpl = _context.sent;
                    case 3:
                    case "end":
                      return _context.stop();
                  }
                }, _callee);
              }));
              return function loadTpl() {
                return _ref.apply(this, arguments);
              };
            }();
            loadTpl().then(function () {
              _newArrowCheck(this, _this2);
              this.doRender();
            }.bind(this));
          } else {
            this.doRender();
          }
        }.bind(this));
      }
    }, {
      key: "expandSubtree",
      value: function expandSubtree(el) {
        if (el.tagName === this.itemTagName.toUpperCase()) {
          if (el.classList.contains(COLLAPSED_CN)) {
            el.classList.remove(COLLAPSED_CN);
            el.classList.add(EXPANDED_CN);
          }
        }
        if (el.tagName !== this.tagName && el.parentElement) {
          this.expandSubtree(el.parentElement);
        }
      }
    }, {
      key: "toggleSubtree",
      value: function toggleSubtree(el) {
        if (el.tagName === this.itemTagName.toUpperCase() && el.classList.contains('sk-tree-has-child')) {
          if (el.classList.contains(EXPANDED_CN)) {
            el.classList.remove(EXPANDED_CN);
            el.classList.add(COLLAPSED_CN);
          } else if (el.classList.contains(COLLAPSED_CN) && el.classList.contains('sk-tree-has-child')) {
            el.classList.add(EXPANDED_CN);
            el.classList.remove(COLLAPSED_CN);
          }
        }
        if (el.tagName !== this.tagName && el.parentElement) {
          this.toggleSubtree(el.parentElement);
        }
      }
    }, {
      key: "deSelectItem",
      value: function deSelectItem() {
        if (this.comp.selectedItemEl) {
          this.comp.selectedItemEl.classList.remove('sk-selected');
          this.comp.selectedItemEl = null;
        }
      }
    }, {
      key: "selectItem",
      value: function selectItem(itemEl) {
        this.comp.selectedItemEl = itemEl;
        var value = this.comp.selectedItemEl.value || this.comp.selectedItemEl.dataset.value;
        if (value) {
          this.comp.setAttribute('value', value);
        }
        if (!itemEl.classList.contains('sk-selected')) {
          itemEl.classList.add('sk-selected');
        } else {
          itemEl.classList.remove('sk-selected');
        }
      }
    }, {
      key: "bindItemSelect",
      value: function bindItemSelect(itemEl, item) {
        if (itemEl.clickHandler) {
          itemEl.removeEventListener('click', itemEl.clickHandler);
        }
        itemEl.clickHandler = function (event) {
          if (event.target.tagName === 'LI' && this.comp._byParentId[item.id]) {
            this.toggleSubtree(event.target || event.srcElement);
          } else {
            event.preventDefault();
            event.stopPropagation();
            this.deSelectItem();
            this.selectItem(itemEl);
            this.comp.dispatchEvent(new CustomEvent('skitemselected', {
              detail: {
                value: this.comp.selectedItemEl.value,
                itemEl: this.comp.selectedItemEl,
                item: item
              },
              bubbles: true,
              composed: true
            }));
          }
        }.bind(this);
        itemEl.addEventListener('click', itemEl.clickHandler);
      }
    }, {
      key: "renderItemWithTplVars",
      value: function renderItemWithTplVars(itemEl, item) {
        var el = this.comp.renderer.prepareTemplate(this.comp.itemTpl);
        itemEl.innerHTML = this.comp.renderer.renderMustacheVars(el, _objectSpread({
          themePath: this.themePath,
          name: item.title || item.name
        }, item));
        if (this.comp.hasAttribute('selectable')) {
          this.bindItemSelect(itemEl, item);
        }
        this.renderSubItems(itemEl, item);
      }
    }, {
      key: "renderItem",
      value: function renderItem(item) {
        var _this3 = this;
        var itemEl = this.comp.renderer.createEl(this.itemTagName);
        itemEl.classList.add(this.itemClassName);
        itemEl.setAttribute('data-id', item.id);
        itemEl.setAttribute('data-parentid', item.parentId);
        for (var _i = 0, _Object$keys = Object.keys(item); _i < _Object$keys.length; _i++) {
          var itemProp = _Object$keys[_i];
          if (itemProp !== 'id' && itemProp !== 'parentId') {
            itemEl.setAttribute('data-' + this.comp.camelCase2Css(itemProp), item[itemProp]);
          }
        }
        if (this.comp.getAttribute('expanded') === 'false') {
          if (this.comp._byParentId[item.id]) {
            itemEl.classList.add(COLLAPSED_CN);
          } else {
            itemEl.classList.add(EXPANDED_CN);
          }
        } else if (this.comp.getAttribute('expanded') === 'true') {
          itemEl.classList.add(EXPANDED_CN);
        }
        if (this.comp.hasAttribute('link-tpl-str')) {
          var linkTplStr = this.comp.getAttribute('link-tpl-str');
          var itemContentsEl = this.comp.renderer.createEl('span');
          itemContentsEl.insertAdjacentHTML('beforeend', linkTplStr);
          itemEl.innerHTML = this.comp.renderer.renderMustacheVars(itemContentsEl, item);
          this.renderSubItems(itemEl, item);
        } else {
          if (this.itemTplPath) {
            if (!this.comp.itemTpl) {
              var loadTpl = /*#__PURE__*/function () {
                var _ref2 = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2() {
                  return regenerator.wrap(function _callee2$(_context2) {
                    while (1) switch (_context2.prev = _context2.next) {
                      case 0:
                        _context2.next = 2;
                        return _this3.comp.renderer.mountTemplate(_this3.itemTplPath, _this3.comp.constructor.name + 'ItemTpl', _this3.comp, {
                          themePath: "".concat(_this3.comp.basePath, "/theme/").concat(_this3.comp.theme)
                        });
                      case 2:
                        _this3.comp.itemTpl = _context2.sent;
                      case 3:
                      case "end":
                        return _context2.stop();
                    }
                  }, _callee2);
                }));
                return function loadTpl() {
                  return _ref2.apply(this, arguments);
                };
              }();
              loadTpl().then(function () {
                _newArrowCheck(this, _this3);
                this.renderItemWithTplVars(itemEl, item);
              }.bind(this));
            } else {
              this.renderItemWithTplVars(itemEl, item);
            }
          } else {
            itemEl.innerHTML = item.title || item.name;
            this.renderSubItems(itemEl, item);
          }
        }
        return itemEl;
      }
    }, {
      key: "renderSubItems",
      value: function renderSubItems(itemEl, item) {
        // render subitems
        if (this.comp._byParentId[item.id]) {
          var itemSubEl = this.comp.renderer.createEl(this.rootTagName);
          var _iterator2 = _createForOfIteratorHelper$3(this.comp._byParentId[item.id]),
            _step2;
          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var subItem = _step2.value;
              var subItemEl = this.renderItem(subItem);
              if (this.comp.hasAttribute('selectable')) {
                this.bindItemSelect(subItemEl, subItem);
              }
              itemSubEl.appendChild(subItemEl);
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }
          itemEl.appendChild(itemSubEl);
          itemEl.classList.add(this.branchClassName);
        }
        var selectedId = this.comp.getAttribute('selected-id');
        if (selectedId !== null && selectedId === item.id) {
          this.selectedEl = itemEl;
        }
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$9(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$9() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$9() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$9 = function _isNativeReflectConstruct() { return !!t; })(); }
  var JquerySkTree = /*#__PURE__*/function (_SkTreeImpl) {
    function JquerySkTree() {
      _classCallCheck(this, JquerySkTree);
      return _callSuper$9(this, JquerySkTree, arguments);
    }
    _inherits(JquerySkTree, _SkTreeImpl);
    return _createClass(JquerySkTree, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'tree';
      }
    }]);
  }(SkTreeImpl);

  function _createForOfIteratorHelper$2(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$2(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$2(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$2(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$2(r, a) : void 0; } }
  function _arrayLikeToArray$2(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$8(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$8() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$8() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$8 = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$2(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkTree = /*#__PURE__*/function (_SkElement) {
    function SkTree() {
      _classCallCheck(this, SkTree);
      return _callSuper$8(this, SkTree, arguments);
    }
    _inherits(SkTree, _SkElement);
    return _createClass(SkTree, [{
      key: "cnSuffix",
      get: function get() {
        return 'tree';
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "buildIdCache",
      value: function buildIdCache() {
        this._byId = {};
        this._byParentId = {};
        var _iterator = _createForOfIteratorHelper$2(this._treeData),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var category = _step.value;
            this._byId[category.id] = category;
            if (!this._byParentId[category.parentId]) {
              this._byParentId[category.parentId] = [];
            }
            this._byParentId[category.parentId].push(category);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }, {
      key: "treeData",
      get: function get() {
        if (!this._treeData) {
          var value = this.getAttribute('tree-data');
          this._treeData = value ? JSON.parse(value) : [];
          this.buildIdCache();
        }
        return this._treeData;
      },
      set: function set(treeData) {
        this._treeData = treeData;
        this.setAttribute('tree-data', JSON.stringify(treeData));
        this.buildIdCache();
      }
    }, {
      key: "rootItems",
      get: function get() {
        return this._byParentId[0] || [];
      }
    }, {
      key: "render",
      value: function render() {
        if (this.implRenderTimest) {
          this.impl.rendered = false;
          this.impl.clearAllElCache();
        }
        _superPropGet$2(SkTree, "render", this, 3)([]);
      }
    }]);
  }(SkElement);

  function _callSuper$7(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$7() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$7() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$7 = function _isNativeReflectConstruct() { return !!t; })(); }
  var ITEMCLICK_EVT = 'itemclick';
  var ItemclickEvent = /*#__PURE__*/function (_CustomEvent) {
    function ItemclickEvent(options) {
      _classCallCheck(this, ItemclickEvent);
      return _callSuper$7(this, ItemclickEvent, [ITEMCLICK_EVT, options]);
    }
    _inherits(ItemclickEvent, _CustomEvent);
    return _createClass(ItemclickEvent);
  }(/*#__PURE__*/_wrapNativeSuper(CustomEvent));

  function _createForOfIteratorHelper$1(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$1(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$1(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$1(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$1(r, a) : void 0; } }
  function _arrayLikeToArray$1(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$6(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$6() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$6() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$6 = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet$1(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkMenuImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkMenuImpl() {
      _classCallCheck(this, SkMenuImpl);
      return _callSuper$6(this, SkMenuImpl, arguments);
    }
    _inherits(SkMenuImpl, _SkComponentImpl);
    return _createClass(SkMenuImpl, [{
      key: "suffix",
      get: function get() {
        return 'menu';
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        _superPropGet$1(SkMenuImpl, "beforeRendered", this, 3)([]);
        this.saveState();
      }
    }, {
      key: "body",
      get: function get() {
        if (!this._body) {
          this._body = this.comp.el.querySelector('ul');
        }
        return this._body;
      },
      set: function set(el) {
        this._body = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['body'];
      }
    }, {
      key: "dumpState",
      value: function dumpState() {
        return {
          'contents': this.comp.contentsState
        };
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
        this.renderItems();
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet$1(SkMenuImpl, "afterRendered", this, 3)([]);
        this.restoreState({
          contentsState: this.contentsState || this.comp.contentsState
        });
      }
    }, {
      key: "renderItems",
      value: function renderItems() {
        var _this = this;
        var items = this.comp.el.querySelectorAll(this.comp.itemTn);
        var _iterator = _createForOfIteratorHelper$1(items),
          _step;
        try {
          var _loop = function _loop() {
            var item = _step.value;
            var tpl = _this.comp.renderer.prepareTemplate(_this.comp.itemTpl);
            var html = _this.comp.renderer.renderMustacheVars(tpl, {
              contents: item.innerHTML
            });
            item.innerHTML = html;
            item.addEventListener('click', function (event) {
              this.comp.dispatchEvent(new ItemclickEvent({
                detail: {
                  origEvent: event,
                  item: item
                }
              }));
            }.bind(_this));
          };
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            _loop();
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$5(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$5() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$5() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$5 = function _isNativeReflectConstruct() { return !!t; })(); }
  var JquerySkMenu = /*#__PURE__*/function (_SkMenuImpl) {
    function JquerySkMenu() {
      _classCallCheck(this, JquerySkMenu);
      return _callSuper$5(this, JquerySkMenu, arguments);
    }
    _inherits(JquerySkMenu, _SkMenuImpl);
    return _createClass(JquerySkMenu, [{
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }]);
  }(SkMenuImpl);

  function _callSuper$4(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$4() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$4() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$4 = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkMenu = /*#__PURE__*/function (_SkElement) {
    function SkMenu() {
      _classCallCheck(this, SkMenu);
      return _callSuper$4(this, SkMenu, arguments);
    }
    _inherits(SkMenu, _SkElement);
    return _createClass(SkMenu, [{
      key: "cnSuffix",
      get: function get() {
        return 'menu';
      }
    }, {
      key: "itemTn",
      get: function get() {
        return this.getAttribute('item-tn') || 'sk-menu-item';
      },
      set: function set(itemTn) {
        return this.setAttribute('item-tn', itemTn);
      }
    }, {
      key: "itemTplPath",
      get: function get() {
        if (!this._itemTplPath) {
          this._itemTplPath = this.confValOrDefault('tpl-path', "/node_modules/sk-menu-".concat(this.theme, "/src/")) + "".concat(this.theme, "-sk-menu-item.tpl.html");
        }
        return this._itemTplPath;
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'disabled' && oldValue !== newValue) {
          if (newValue) {
            this.impl.disable();
          } else {
            this.impl.enable();
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }, {
      key: "hide",
      value: function hide() {
        this.style.display = 'none';
      }
    }, {
      key: "show",
      value: function show(event) {
        this.style.position = 'fixed';
        this.style.display = 'block';
        this.style.top = "".concat(event.clientY, "px");
        this.style.left = "".concat(event.clientX, "px");
        if (!this.mouseLeaveHandler) {
          this.mouseLeaveHandler = function () {
            this.hide();
          }.bind(this);
          this.onmouseleave = this.mouseLeaveHandler;
        }
      }
    }, {
      key: "preLoadedTpls",
      get: function get() {
        return ['itemTpl'];
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['disabled'];
      }
    }]);
  }(SkElement);

  function _callSuper$3(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$3() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$3() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$3 = function _isNativeReflectConstruct() { return !!t; })(); }
  var TOOLBAR_ITEMCLICK_EVT = 'toolbaritemclick';
  var ToolbarItemClickEvent = /*#__PURE__*/function (_CustomEvent) {
    function ToolbarItemClickEvent(options) {
      _classCallCheck(this, ToolbarItemClickEvent);
      return _callSuper$3(this, ToolbarItemClickEvent, [TOOLBAR_ITEMCLICK_EVT, options]);
    }
    _inherits(ToolbarItemClickEvent, _CustomEvent);
    return _createClass(ToolbarItemClickEvent);
  }(/*#__PURE__*/_wrapNativeSuper(CustomEvent));

  function _createForOfIteratorHelper(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
  function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$2(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$2() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$2() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$2 = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var SkToolbarImpl = /*#__PURE__*/function (_SkComponentImpl) {
    function SkToolbarImpl() {
      _classCallCheck(this, SkToolbarImpl);
      return _callSuper$2(this, SkToolbarImpl, arguments);
    }
    _inherits(SkToolbarImpl, _SkComponentImpl);
    return _createClass(SkToolbarImpl, [{
      key: "suffix",
      get: function get() {
        return 'toolbar';
      }
    }, {
      key: "body",
      get: function get() {
        if (!this._body) {
          this._body = this.comp.el.querySelector('.sk-toolbar');
        }
        return this._body;
      },
      set: function set(el) {
        this._body = el;
      }
    }, {
      key: "subEls",
      get: function get() {
        return ['body'];
      }
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _superPropGet(SkToolbarImpl, "afterRendered", this, 3)([]);
        this.renderItems();
      }
    }, {
      key: "renderItems",
      value: function renderItems() {
        var _this = this;
        var items = this.comp.querySelectorAll(this.comp.itemTn);
        var _iterator = _createForOfIteratorHelper(items),
          _step;
        try {
          var _loop = function _loop() {
            var item = _step.value;
            var tpl = _this.comp.renderer.prepareTemplate(_this.comp.itemTpl);
            var contents = _this.icons[item.innerText] ? _this.icons[item.innerText] : item.innerHTML;
            var html = _this.comp.renderer.renderMustacheVars(tpl, {
              contents: contents
            });
            item.innerHTML = html;
            item.addEventListener('click', function (event) {
              this.comp.dispatchEvent(new ToolbarItemClickEvent({
                detail: {
                  origEvent: event,
                  item: item
                },
                bubbles: true,
                composed: true
              }));
            }.bind(_this));
          };
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            _loop();
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }]);
  }(SkComponentImpl);

  function _callSuper$1(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$1() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$1() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$1 = function _isNativeReflectConstruct() { return !!t; })(); }
  var JquerySkToolbar = /*#__PURE__*/function (_SkToolbarImpl) {
    function JquerySkToolbar() {
      _classCallCheck(this, JquerySkToolbar);
      return _callSuper$1(this, JquerySkToolbar, arguments);
    }
    _inherits(JquerySkToolbar, _SkToolbarImpl);
    return _createClass(JquerySkToolbar, [{
      key: "icons",
      get: function get() {
        if (!this._icons) {
          this._icons = {
            'add': '<span class="ui-icon ui-icon-plusthick"></span>',
            'edit': '<span class="ui-icon ui-icon-pencil"></span>',
            'delete': '<span class="ui-icon ui-icon-trash"></span>',
            'save': '<span class="ui-icon ui-icon-disk"></span>',
            'settings': '<span class="ui-icon ui-icon-gear"></span>',
            'home': '<span class="ui-icon ui-icon-home"></span>',
            'folder': '<span class="ui-icon ui-icon-folder-collapsed"></span>',
            'file': '<span class="ui-icon ui-icon-document"></span>'
          };
        }
        return this._icons;
      },
      set: function set(icons) {
        this._icons = icons;
      }
    }, {
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }]);
  }(SkToolbarImpl);

  function _callSuper(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct = function _isNativeReflectConstruct() { return !!t; })(); }
  var SkToolbar = /*#__PURE__*/function (_SkElement) {
    function SkToolbar() {
      _classCallCheck(this, SkToolbar);
      return _callSuper(this, SkToolbar, arguments);
    }
    _inherits(SkToolbar, _SkElement);
    return _createClass(SkToolbar, [{
      key: "cnSuffix",
      get: function get() {
        return 'toolbar';
      }
    }, {
      key: "itemTn",
      get: function get() {
        return this.getAttribute('item-tn') || 'sk-toolbar-item';
      },
      set: function set(itemTn) {
        return this.setAttribute('item-tn', itemTn);
      }
    }, {
      key: "itemTplPath",
      get: function get() {
        if (!this._itemTplPath) {
          this._itemTplPath = this.confValOrDefault('tpl-path', "/node_modules/sk-toolbar-".concat(this.theme, "/src/")) + "".concat(this.theme, "-sk-toolbar-item.tpl.html");
        }
        return this._itemTplPath;
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          this.initImpl();
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'disabled' && oldValue !== newValue) {
          if (newValue) {
            this.impl.disable();
          } else {
            this.impl.enable();
          }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
      }
    }, {
      key: "hide",
      value: function hide() {
        this.style.display = 'none';
      }
    }, {
      key: "preLoadedTpls",
      get: function get() {
        return ['itemTpl'];
      }
    }], [{
      key: "observedAttributes",
      get: function get() {
        return ['disabled'];
      }
    }]);
  }(SkElement);

  exports.JquerySkAccordion = JquerySkAccordion;
  exports.JquerySkAlert = JquerySkAlert;
  exports.JquerySkApp = JquerySkApp;
  exports.JquerySkButton = JquerySkButton;
  exports.JquerySkCheckbox = JquerySkCheckbox;
  exports.JquerySkDatepicker = JquerySkDatepicker;
  exports.JquerySkDialog = JquerySkDialog;
  exports.JquerySkForm = JquerySkForm;
  exports.JquerySkInput = JquerySkInput;
  exports.JquerySkMenu = JquerySkMenu;
  exports.JquerySkNavbar = JquerySkNavbar;
  exports.JquerySkSelect = JquerySkSelect;
  exports.JquerySkSltabs = JquerySkSltabs;
  exports.JquerySkSpinner = JquerySkSpinner;
  exports.JquerySkSwitch = JquerySkSwitch;
  exports.JquerySkTabs = JquerySkTabs;
  exports.JquerySkToolbar = JquerySkToolbar;
  exports.JquerySkTree = JquerySkTree;
  exports.JqueryTheme = JqueryTheme;
  exports.SkAccordion = SkAccordion;
  exports.SkAccordionImpl = SkAccordionImpl;
  exports.SkAlert = SkAlert;
  exports.SkAlertImpl = SkAlertImpl;
  exports.SkApp = SkApp;
  exports.SkAppImpl = SkAppImpl;
  exports.SkButton = SkButton;
  exports.SkButtonImpl = SkButtonImpl;
  exports.SkCheckbox = SkCheckbox;
  exports.SkCheckboxImpl = SkCheckboxImpl;
  exports.SkComponentImpl = SkComponentImpl;
  exports.SkConfig = SkConfig;
  exports.SkDatePicker = SkDatePicker;
  exports.SkDatepickerImpl = SkDatepickerImpl;
  exports.SkDialog = SkDialog;
  exports.SkDialogImpl = SkDialogImpl;
  exports.SkElement = SkElement;
  exports.SkForm = SkForm;
  exports.SkFormImpl = SkFormImpl;
  exports.SkInput = SkInput;
  exports.SkInputImpl = SkInputImpl;
  exports.SkMenu = SkMenu;
  exports.SkMenuImpl = SkMenuImpl;
  exports.SkNavbar = SkNavbar;
  exports.SkNavbarImpl = SkNavbarImpl;
  exports.SkRegistry = SkRegistry;
  exports.SkSelect = SkSelect;
  exports.SkSelectImpl = SkSelectImpl;
  exports.SkSltabs = SkSltabs;
  exports.SkSltabsImpl = SkSltabsImpl;
  exports.SkSpinner = SkSpinner;
  exports.SkSpinnerImpl = SkSpinnerImpl;
  exports.SkSwitch = SkSwitch;
  exports.SkSwitchImpl = SkSwitchImpl;
  exports.SkTabs = SkTabs;
  exports.SkTabsImpl = SkTabsImpl;
  exports.SkToolbar = SkToolbar;
  exports.SkToolbarImpl = SkToolbarImpl;
  exports.SkTree = SkTree;
  exports.SkTreeImpl = SkTreeImpl;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=sk-jquery-bundle.js.map
