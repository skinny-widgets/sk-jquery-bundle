let gulp = require('gulp');
let run = require('gulp-run');

let paths = {
    packages: {
        src: [
            '../sk-core/src/**/*.js',
            '../sk-core/cli/**/*.js',
            '../sk-input/src/**/*.js',
            '../sk-input-jquery/**/*.js',
            '../sk-button/src/**/*.js',
            '../sk-button-jquery/**/*.js',
            '../sk-alert/src/**/*.js',
            '../sk-alert-jquery/**/*.js',
            '../sk-accordion/src/**/*.js',
            '../sk-accordion-jquery/**/*.js',
            '../sk-app/src/**/*.js',
            '../sk-app-jquery/**/*.js',
            '../sk-checkbox/src/**/*.js',
            '../sk-checkbox-jquery/**/*.js',
            '../sk-datepicker/src/**/*.js',
            '../sk-datepicker-jquery/**/*.js',
            '../sk-dialog/src/**/*.js',
            '../sk-dialog-jquery/**/*.js',
            '../sk-form/src/**/*.js',
            '../sk-form-jquery/**/*.js',
            '../sk-navbar/src/**/*.js',
            '../sk-navbar-jquery/**/*.js',
            '../sk-select/src/**/*.js',
            '../sk-select-jquery/**/*.js',
            '../sk-spinner/src/**/*.js',
            '../sk-spinner-jquery/**/*.js',
            '../sk-switch/src/**/*.js',
            '../sk-switch-jquery/**/*.js',
            '../sk-tabs/src/**/*.js',
            '../sk-tabs-jquery/**/*.js',
            '../sk-tree/src/**/*.js',
            '../sk-tree-jquery/**/*.js',
            '../sk-theme-jquery/**/*'
        ],
        dest: './node_modules'
    },
    stand: {
        dest: '../sk-stand/node_modules/sk-jquery-bundle/dist'
    }
};

function rebuild() {
    return run('npm run build').exec();
}

function scripts() {
    return gulp.src(paths.packages.src)
        .pipe(gulp.dest(paths.packages.dest));
}

function hotdeploy() {
    return gulp.src('./dist')
        .pipe(gulp.dest(paths.stand.dest));
}

/**
 * watches set of bundle source packages, rebuild bundle and hotdeploy (replace files) to stand
 * when changed
 */
function watch() {
    gulp.watch(paths.packages.src, gulp.series(scripts, rebuild, hotdeploy));
}

exports.watch = watch;
